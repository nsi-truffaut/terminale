def anagram(chaine_lettre):
    n = len(chaine_lettre)
    anagram_list = [chaine_lettre]
    if n ==1 :
        return anagram_list
    elif n == 2 :
        anagram_list.append(chaine_lettre[1] + chaine_lettre[0])
    else :
        for i in range(n) :
            list_temp= anagram(chaine_lettre[:i]+chaine_lettre[i+1:n])
            
            for j in range(len(list_temp)):
                new_mot = chaine_lettre[i] + list_temp[j]
                if new_mot not in anagram_list :
                    anagram_list.append(new_mot)

    return anagram_list

c = "abc"
d = "saintes"
print(anagram(c))
print(anagram(d))

import itertools as it

def anagram2(mot):
    return set("".join(e) for e in it.permutations(mot, len(mot)))

print(anagram2('saintes'))
