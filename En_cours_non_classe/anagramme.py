#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : anagramme.py
#  auteur : Gregory MAUPU
#    date : 2021/09/03

import itertools as it

def anagram(mot):
    """ Fonction qui renvoie la liste de tous les anagrammes possibles d'un mot
        existants ou pas.

        >>> set(anagram('abc')) == set(['bac', 'acb', 'cab', 'cba', 'abc', 'bca'])
        True
        >>> set(anagram('rate')) == set(['trae', 'tera', 'eart', 'aret', 'etar', 'taer', 'rtae', 'ater', 'aert', 'rtea', 'aetr', 'arte', 'erta', 'reat', 'atre', 'reta', 'tear', 'eatr', 'trea', 'raet', 'rate', 'etra', 'tare', 'erat'])
        True
        >>> set(anagram('moto')) == set(['omot', 'moto', 'toom', 'otmo', 'otom', 'omto', 'tmoo', 'mtoo', 'tomo', 'ootm', 'oomt', 'moot'])
        True
        
    """
    s = set("".join(e) for e in it.permutations(mot, len(mot)))
    return list(s)

if __name__=='__main__' :
    import doctest
    doctest.testmod(verbose=True)

# fin du fichier anagramme.py
