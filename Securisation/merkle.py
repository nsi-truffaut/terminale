#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : merkel.py
#  auteur : Gregory MAUPU
#    date : 2022/04/03

import random

ab = "abcedefghijklmnopqrstuvwxyz#)@,ABCDEFGHIHJLMNOPQRSTUVWXYZ"
def chaine_alea(n):
    s = ""
    for k in range(n):
        s = s + random.choice(ab)
    return s
    
def merkle():
    with open("merkel_file.txt","w",encoding="utf-8") as fichier :
        k = 0
        while k < 100000 :
            c = "identifiant :" + str(random.randint(0,100000)) +","+ " clé :" + chaine_alea(15) + "\n"
            fichier.write(c)
            k = k+1

        
