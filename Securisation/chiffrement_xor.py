#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : chiffrement_xor.py
#  auteur : Gregory MAUPU
#    date : 2022/03/30

from generation_cle import cle,enumerer
from module_chiffrement import *



def xor(a : int,b : int) -> int:
    """
    Fonction qui implémente l'opérateur logique xor

    >>> xor(0,1)
    1
    >>> xor(1,1)
    0
    """
    assert a in [0,1] and b in [0,1],"Les nombres données ne sont pas 0 ou 1"
    if a == b :
       return 0
    else :
       return 1

def chiffrement(chaine : str,cle : int)-> list:
    """
    Fonction qui prend en argument une chaîne et renvoie l'écriture binaire chiffrée
    """
    l = chaine_vers_bin(chaine) # Conversion binaire de la chaîne
    
    m =len(cle)
    n = len(l)
    return [xor(l[k],cle[k%m]) for k in range(n)]
        

def affichage_chiffrement(message : list) -> str:
    """
    Fonction qui permet d'afficher le message chiffré sous forme d'une chaîne
    """
    return bin_vers_chaine(message)

def dechiffrement(message : list ,cle : list) -> str:
    """
    Fonction qui prend en paramètre une liste de binaire qui
    correspond au message chiffré et la clé et  renvoie la chaîne correspondante
    """
    taille_cle = len(cle)
    
    u = []
    for k in range(len(message)):
        u.append(xor(message[k],cle[k%taille_cle]))
        
        
    return bin_vers_chaine(u)

def chiffrer_fichier(fichier,cle : list) :
    """
    Fonction qui prend en paramètre un fichier de type texte et une clé sous forme binaire
    Elle renvoie un fichier de 0 et de 1 qui correspond au texte chiffré.
    Le texte ne doit comporter que des caractères ascii
    """
    with open(fichier,'r',encoding='utf-8') as file :
        with open('fichier_code.txt', 'w',encoding='utf-8') as nfile :
            l = file.readline().rstrip('\n')
            
            
            
            while l !="":
                c = chiffrement(l,list(cle))
                nfile.write("".join(map(str,c))+"\n")
                l = file.readline().rstrip('\n')
                
def dechiffrer(fichier,cle):
    """
    Fonction qui prend en paramètre un fichier contenant de 0 et des 1 et une clé sous forme binaire
    Elle renvoie un fichier de 0 et de 1 qui correspond au texte chiffré.
   
    """
    with open(fichier,'r',encoding='utf-8') as file :
        with open('fichier_decode.txt', 'w',encoding='utf-8') as nfile :
            l = list(file.readline().rstrip('\n'))
            l = list(map(lambda a : int(a),l))           
            while l !=[]:
                c = dechiffrement(l,list(cle))
                                  
                nfile.write(c+"\n")
                l = list(file.readline().rstrip('\n'))
                l = list(map(lambda a : int(a),l))   
                
if __name__=="__main__" :

    # Test avec une ligne
    
    chaine = "identifiant :2361, clé :GtteJl#rlAlJheQ"
    cle = cle(4)
    mes = chiffrement(chaine,cle) # On chiffre la chîne
    print(mes)
    aff = affichage_chiffrement(mes) # On affiche le message codé
    c = chaine_vers_bin(aff) # On reconvertit en binaire
    print(dechiffrement(mes,cle))
    assert(dechiffrement(chiffrement(chaine,cle),cle)==chaine)
    #print(dechiffrement(mes,cle))
    a = chiffrement('Bonjour',cle)
    #print(a)
    #print(dechiffrement(a,cle))
    l = enumerer([1,1,1,1])
    a = list('110010110110011011110011010110010001110001011110010110110011001110010110110011110110010001110001011111011111111000101111001101111001100111001001111001110111010011111011111110011100110010011100010110111011111111000101110111000110001011110001011110011010110110101110010011111011100110001101110010011110111110110010011110110101110010111110011010110101110')
    b = list(map(lambda z : int(z),a))
    
        
    
# fin du fichier chiffrement_xor

        
