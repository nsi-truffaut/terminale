def indice(lettre,alphabet):
    k = 0
    while lettre != alphabet[k] :
        k=k+1
    return k
    

def vigenere(chaine,cle):
    n = len(chaine)
    m=len(cle)
    s = ""
    for k in range(n):
        a = indice(chaine[k],alphabet)
        b = indice(cle[k%m],alphabet)
        s = s + alphabet[(a+b)%26]
    return s

alphabet="abcdefghijklmnopqrstuvwxyz"
