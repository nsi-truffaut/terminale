#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : module_chiffrement.py
#  auteur : Gregory MAUPU
#    date : 2022/03/30

def dec_to_bin(d : int) -> list:
    """ Fonction qui convertit un entier en binaire.
        Le résultat renvoyé est une liste dont le bit de poids fort est le premier
        élément

        >>> dec_to_bin(0)
        [0]
        >>> dec_to_bin(235)
        [0, 1, 1, 1, 0, 1, 0, 1, 1]
    """
    if d < 2 :
        return [d%2]
    else :
        return dec_to_bin(d//2) + [d%2]

def completion(l : list,k : int) -> list:
    """
    Fonction qui permet de compléter l'écriture d'un nombre binaire à un nombre de bits défini
    Attention : pour compléter à un octet, on écrira 9
    """
    while len(l) < k :
        l =  [0] + l
    return l

def bin_to_dec(l : list) -> int:
    """ Fonction qui convertit un nombre binaire (sous forme d'une liste) en un entier
    >>> bin_to_dec([0,0,1,0,1,1])
    11
    >>> bin_to_dec([1,1,1,1,0])
    30
    """
    d = 0
    for i in range(len(l)):
        d = d + l[i]*2**(len(l)-i-1)
    return d
    
def chaine_vers_bin(chaine : str) -> list : # 1 octet par caractère
    """ Fonction qui transforme une chaine de caractères en binaire.
        La conversion passe par le codage ASCII du caractère.
        On détermine d'abord l'entier associé au caractère qu'on convertit en binaire.
        Les caractères sont codés un par un et sur 9 bits.
        On obtient un liste de longueur 9*nb de caractères.(pas toujours)

        >>> chaine_vers_bin("a")
        [0, 0, 1, 1, 0, 0, 0, 0, 1]
        >>> chaine_vers_bin("bref")
        [0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0]
        >>> chaine_vers_bin("123")
        [0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1]

        
    """
    u=[]
    for k in range(len(chaine)):
         n = completion(dec_to_bin(ord(chaine[k])),8)
         u = u+n
    return u

def bin_vers_chaine(l : list) -> str:
    """ Fonction qui transforme une liste de 0 et 1 en chaine de caractères.
        Les caractères utilisés sont réduits à ceux dont le code ASCII est compris entre 33 et 126
        >>> bin_vers_chaine([0,1])
        ''
        >>> bin_vers_chaine([0,0,1,1,0,1,1,1,0])
        'n'
        >>> bin_vers_chaine([1, 0, 0, 0, 1, 1, 1, 1])
        '\x8f'
    """    
    s = ""
    for i in range(len(l)//8) :
        m = []
        for j in range(i*8,(i+1)*8) :
            m.append(l[j])
        d = bin_to_dec(m)
        s = s + chr(d)        
    return s

if __name__=="__main__" :
    import doctest
    doctest.testmod()

# fin du fichier module_chiffrement
