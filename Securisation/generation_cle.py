#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : generation_cle.py
#  auteur : Gregory MAUPU
#    date : 2022/03/30

import random

def choix_suivant(choix_fait,C):
    """ Fonction qui permet de donner la liste qui suit immédiatement choix_fait quand on ajoute 1
        au dernier élément sans dépasser les valeurs de C.
        Il s'agit d'un compteur mécanique.
        Si on imagine un arbre qui permet de lister toutes les combinaisons,
        choix_fait représente un chemin de la racine à une feuille, C la combinaison
        maximale et choix_suivant renvoie la combinaison suivante comme
        si on faisait un parcours en profondeur
        >>> choix_suivant([0,0,0],[1,1,1])
        [0, 0, 1]
        >>> choix_suivant([0,1,3],[2,2,3])
        [0, 2, 0]
    """
    e = len(C)-1
    E = 0
    while e > E and choix_fait[e] == C[e] :
        choix_fait[e] = 0
        e = e-1
    if e >= E :
        choix_fait[e] = choix_fait[e] + 1
    return choix_fait

def enumerer(C):
    """Fonction qui renvoie toutes les combinaisons possibles de choix en partant de
      [0,0,..,0] jusqu'à C
    """
    L = []
    choix_fait = [0 for _ in range(len(C))]
    while choix_fait !=C :
        u = choix_fait.copy()
        L.append(u)
        choix_fait = choix_suivant(choix_fait,C)
    L.append(C)
    return L

def cle(n):
    """ Fonction qui choisit aléatoirement une clé en utilisant la loi uniforme"
        n représente la taille de la clé
    """
    liste_cle = enumerer([1 for _ in range(n)])
    t = len(liste_cle)
    k = int(random.uniform(0,t-1))
    return liste_cle[k]

if __name__=="__main__" :
    import doctest
    doctest.testmod()

# fin du fichier generation_cle
    
