


def recherche_naive(chaine,mot):
    """ Fonction qui renvoie un booléen dès la première occurence de la chaîne trouvée
    """
    n = len(mot)
    m = len(chaine)
    k = 0
    while k < m - n :
        j = 0
        while j < n and mot[j] == chaine[k+j] :
            j = j +1
        if j ==  n :
            return True
        else :
            k = k+1
    return False

def recherche_naive_indice(chaine,mot):
    """ Fonction qui renvoie
    la liste des indices où apparaît le mot de la chaîne trouvée
    """
    n = len(mot)
    m = len(chaine)
    liste_indice = []
    k = 0
    while k < m - n :
        j = 0
        while j < n and mot[j] == chaine[k+j] :
            j = j +1
        if j ==  n :
            liste_indice.append(k)
        k = k+1
        
    return liste_indice

chaine = "ACTGGTCATTCGCCA"
mot = "ATTC"
mot1 = "ATCG"

def table_mc(alphabet,mot) :
    table = {}
    for k in range(len(mot)-1):
        table[mot[k]] = len(mot)-1 - k
    for lettre in alphabet :
        if lettre not in table.keys():
            table[lettre] = len(mot)
    return table

def recherche_MC(chaine, mot,alphabet):
    n = len(mot)
    m = len(chaine)
    table = table_mc(alphabet,mot)
    liste_indice = []
    for i in range(m-n) :
        j = n - 1
        while j >= 0 and chaine[i+j] == mot[j] :
            j = j-1
        if j < 0:
            liste_indice.append(i)
        else :
            i = i + max(1,j -n+1 + table[chaine[i+j]])
    return liste_indice
    
    
                   

if __name__=="__main__" :
    with open('alea_lettre.txt','r',encoding='utf-8') as file:
        chaine = file.readline()
        assert recherche_naive_indice(chaine,mot)== recherche_MC(chaine,mot,['A','T','C','G'])

    with open('PI.txt','r',encoding='utf-8') as file:
        ligne = file.readline().strip()
        chaine = ""
        while ligne != "" :
            chaine += ligne
            ligne = file.readline().strip()
        print(recherche_naive(chaine,"310778"))
