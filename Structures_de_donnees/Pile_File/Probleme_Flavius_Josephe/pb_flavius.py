#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : pb_flavius.py
#  auteur : Gregory MAUPU
#    date : 2021/10/04

from file import File

def longueur(file):
    """ Fonction qui renvoie la longueur d'une file, sans la modifier
    """
    aux = File()
    l = 0
    while not file.est_vide():
        aux.enfiler(file.defiler())
        l = l+1
    while not aux.est_vide() :
        file.enfiler(aux.defiler())
    return l

def flavius(file,k):
    l = []
    while longueur(file) >0 :
        for i in range(k-1) :
            a = file.defiler()
            file.enfiler(a)
            
        b = file.defiler()
        
    return b

# Test du programme

l = [k for k in range(1,41)]
f = File()
for e in l :
    f.enfiler(e)

print(flavius(f,3))

# fin du fichier pb_flavius.py
