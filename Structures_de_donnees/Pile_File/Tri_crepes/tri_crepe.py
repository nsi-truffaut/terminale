#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : tri_crepe.py
#  auteur : Gregory MAUPU
#    date : 2021/10/04

from module_pile import *

def indice_max_pile(pile,i):
    """ Renvoie l'indice de l'élément maximale de la Pile compris entre les indices 0 et i inclus
        La pile reste inchangée."""
    j_max = 0
    j = 0
    q = Pile()
    maxi = pile.depiler()
    q.empiler(maxi)   # Adaptation de l'algorithme du recherche d'un maximum à une Pile
    while j < i :
        a = pile.depiler()
        j = j+1
        if a > maxi :
            maxi = a
            j_max = j
        q.empiler(a)
        
    while not q.est_vide():
        pile.empiler(q.depiler())
    
    return j_max
   


def retourner_crepe(pile,j):
    """ Fonction qui retourne les éléments de la pile compris
        entre 0 et j inclus"""
    q = Pile()
    r = Pile()
    k = 0
    while k < j+1:            # On dépile jusqu'à l'indice j
        a = pile.depiler()
        q.empiler(a)
        k = k +1
    while not q.est_vide() : # On dépile la pile intermédiaire dans une autre pour l'inverser
        r.empiler(q.depiler())
    while not r.est_vide(): # On empile sur la pile de départ
        pile.empiler(r.depiler())
    
    
def tri_crepe(pile,n):
    """ Principe du tri de crepes"""
    j = 1
    while j < n :
        ind_max = indice_max_pile(pile,n-j)
        retourner_crepe(pile,ind_max)
        retourner_crepe(pile,n-j)  
        j = j+1
    
if __name__=="__main__":

    pil=(5,9,3,6,1,5,8,4)
    crepes1=Pile()
    for el in pil:
        crepes1.empiler(el)
    print(crepes1)
    tri_crepe(crepes1,8)
    print(crepes1)
