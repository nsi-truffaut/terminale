from module_pile import Pile
from turtle import *
from random import *



def modif_coordo(x,y,debut,fin):
    if debut=="A" and fin=="C":
        x = x +300
    elif (debut == "A" and fin =="B") or (debut == "B" and fin =="C") :
        x = x+150
    elif debut=="C" and fin=="A" :
        x = x - 300
    elif (debut == "B" and fin =="A") or (debut == "C" and fin =="B") :
        x = x-150
    return x,y

class Decor :

    def __init__(self):
        self.t = Turtle()
        
        self.t.hideturtle()
        self.t.up()

    def draw(self):
        self.t.goto(-150,-10)
        self.t.down()
        self.t.forward(300)
        self.t.up()
        
class Rectangle :

    def __init__(self):
        self.t = Turtle()
        self.t.up()
        self.t.shape("square")
        self.t.color(255//randint(1,6),255//randint(1,6),255//randint(1,6))

    def transform(self,s):
        self.t.shapesize(1,s,2)
        

    def move(self,x,y):
        self.t.setx(x)
        self.t.sety(y)

    def get_x(self):
        return self.t.pos()[0]

    def get_y(self):
        return self.t.pos()[1]

liste_position=[]           
def hanoi(debut,intermediaire,fin,hauteur):
    global liste_position 
    if hauteur == 1 :
       x,y = debut[0].depiler()
       if not fin[0].est_vide():
           a = fin[0].depiler()
           fin[0].empiler(a)
       else :
           a = (0,0)
       fin[0].empiler((modif_coordo(x,25+a[1],debut[1],fin[1])))
       liste_position.append((hauteur,modif_coordo(x,25+a[1],debut[1],fin[1])))
                             
       #print("disque",hauteur,debut[1],fin[1],a)
        
    else :
        
        hanoi(debut,fin,intermediaire,hauteur-1)
        #print("Avant","d",debut,"i",intermediaire,"f",fin)
        x,y = debut[0].depiler()
        if not fin[0].est_vide():
           a = fin[0].depiler()
           fin[0].empiler(a)
        else :
            a = (0,0)
        
        fin[0].empiler( (modif_coordo(x,a[1]+25,debut[1],fin[1])))
        liste_position.append((hauteur,modif_coordo(x,a[1]+25,debut[1],fin[1])))
        #print("Après","d",debut,"i",intermediaire,"f",fin)
        #print("disque",hauteur,debut[1],fin[1],a)
        hanoi(intermediaire,debut,fin,hauteur-1)
    return liste_position
        
if __name__=="__main__" :
    debut = Pile()
    inter = Pile()
    fin = Pile()
    n = int(input("Combien de tours souhaitez-vous ?"))
    colormode(255)
    d = Decor()
    d.draw()
    t = []
   
    for k in range(n,0,-1):
         debut.empiler((-150,25*(n-k+1)))
         t.append(Rectangle())
    
    for k in range(len(t)):
        t[k].transform(5*(k+1)/5)
        t[k].move(-150,20*(n-k-1))
    h = hanoi((debut,"A"),(inter,"B"),(fin,"C"),n)
    for k in range(len(h)):
    
        j = h[k][0]-1
        t[j].move(t[j].get_x(),150)
        #t[j].sety(150)
        t[j].move(h[k][1][0],t[j].get_y())
        t[j].move(t[j].get_x(),h[k][1][1]-25)
                    
    

    




