#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : calculatrice_npi.py
#  auteur : Gregory MAUPU
#    date : 2021/12/1

# Importation des modules

from tkinter import *
from notation_polonaise import *

# Création de la fenêtre

fenetre = Tk()

# Création de la zone de saisie

expression = StringVar()
saisie = Entry(fenetre,textvariable=expression)
saisie.pack()

# fonction

def calcul():
    
    resultat.set(str(npi(expression.get())))

# Creation d'un bouton

bouton1 = Button(fenetre, text = "valider",command=calcul)
bouton1.pack()

# Création d'un label

resultat = StringVar()
label=Label(fenetre,textvariable=resultat)
label.pack()

# Execution

fenetre.mainloop()
    
    

# fin du fichier calculatrice_npi.py


