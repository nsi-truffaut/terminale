#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : notation_polonaise.py
#  auteur : Gregory MAUPU
#    date : 2021/09/30

from module_pile import Pile

##def npi(s):
##    
##    temp = ""
##    p = Pile()
##    for car in s :
##        if car not in ['+','*','-','/',' '] :
##            temp = temp + car
##        else :
##             if temp !='' and not '.' in temp:
##                 p.empiler(int(temp))
##             else :
##                 p.empiler(float(temp))
##             temp = ''
##             if car !=' ':
##                 
##            
##                a = p.depiler()
##                b = p.depiler()
##                if car == "*" :
##                    p.empiler(a*b)
##                if car == "+":
##                    p.empiler(a+b)
##                if car == "/" :
##                    p.empiler(b/a)
##                if car =="-":
##                    p.empiler(b-a)
##        
##    return p.depiler()

def npi(s):
    
    temp = ""
    p = Pile()
    for car in s :
       
        if car not in ['+','*','-','/',' '] :
            temp = temp + car
        else :
             if temp!= "":
                 if '.' in temp :
                     p.empiler(float(temp))
                 else :
                     p.empiler(int(temp))
                 temp =''
                 
             else :
                 if car !=" " :
                    a = p.depiler()
                    b = p.depiler()
                    if car == "*" :
                        p.empiler(a*b)
                    if car == "+":
                        p.empiler(a+b)
                    if car == "/" :
                        p.empiler(b/a)
                    if car =="-":
                        p.empiler(b-a)
             
       
    return p.depiler()

            
if __name__=="__main__" :

    assert(npi('9 8 * 7 +')==7+8*9)
    assert(npi('8 3*5+11 4+/')==(5+8*3)/(11+4))
    
    assert(npi('3 7 5*-')==3-7*5)
    
    assert(npi('8 11*13-15 2 3/+*')==(8*11-13)*(15 + 2/3))
    print(npi('1 1+'))
    print(npi('2.5 3.7+'))
    print(npi("7 3 4*+11 5*8+/"))
