#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : calculatrice_complete_npi.py
#  auteur : Gregory MAUPU
#    date : 2021/12/1

# Importation des modules

# A compléter - importer votre module npi
from tkinter import *


# Création de la fenêtre

fenetre = Tk()
# A compléter - La fenêtre doit faire 440 par 370
# Ajouter un titre


# Creation des frames - Zone à l'intérieur de la fenêtre

input_frame = Frame(fenetre, width=300, height=50, bd=0, highlightbackground="black", highlightcolor="black", highlightthickness=2)
input_frame.pack(side=TOP)

# A compléter - Créer une frame pour accueillir les futures boutons - Elle aura un fond gris

# Création de la zone de saisie

expression = StringVar()
saisie = Entry(input_frame,textvariable=expression,width="300",font=('arial', 18, 'bold'))
saisie.grid(row = 0,column=0)

# fonction

affiche = ""

def bouton_click(item):
    global affiche
    # Compléter pour que la variable affiche se modifie
    # à chaque fois qu'un bouton est pressé et que l'expression se modifie également

def calcul():
       
    result = str(npi(expression.get()))
    saisie.delete(0,"end")
    saisie.insert(0,result)
    
def bouton_clear():
    global affiche
    # Doit effacer la zone de saisie - A compléter

# Creation des boutons

#Ligne du haut

Exemple de création de deux boutons

clear = Button(btns_frame,text = "C",fg = "black",width = 24, height = 3, bd = 1, bg = "#fff", cursor = "hand2",command=lambda :bouton_clear())
clear.grid(row=0,column=0,columnspan=2,padx=0)

space =  Button(btns_frame,text = "Espace",fg = "black",width = 10, height = 3, bd = 1, bg = "#fff", cursor = "hand2",command=lambda :bouton_click(" "))
space.grid(row=0,column=2,padx = 1, pady = 1)

# Compléter pour ajouter l'ensemble des autres boutons






# Execution

fenetre.mainloop()
    
    

# fin du fichier calculatrice_complete_npi.py


