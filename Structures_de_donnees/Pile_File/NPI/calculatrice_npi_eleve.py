#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : calculatrice_npi_eleve.py
#  auteur : Gregory MAUPU
#    date : 2021/12/1

# Importation des modules

from tkinter import *
# A compléter - Importer votre module NPI

# Création de la fenêtre

fenetre = Tk()

# Création de la zone de saisie

expression = StringVar() # On déclare un objet qui va contenir l'expression saisie au clavier
saisie = Entry(fenetre,textvariable=expression)# Créer une zone de saisie associée avec la variable expression
saisie.pack() # Permet de placer la zone de saisie dans la fenêtre

# Création d'un label destiné à afficher le résultat

resultat =        # A compléter - On déclare une variable qui contiendra une chaîne de caractères
label=Label(fenetre,textvariable=resultat)# Créer un label associé avec la variable résultat
label.pack()

# fonction

def calcul():
        
    # A compléter - Modifier l'état de la variable résultat pour qu'elle affiche le résultat de l'expression
    # saisie dans la variable expression

# Creation d'un bouton

bouton1 = Button(fenetre, text = "valider",command=calcul)
bouton1.pack()



# Execution

fenetre.mainloop()
    
    

# fin du fichier calculatrice_npi.py


