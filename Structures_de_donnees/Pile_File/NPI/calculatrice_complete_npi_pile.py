#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : calculatrice_complete_npi.py
#  auteur : Gregory MAUPU
#    date : 2021/12/1

from notation_polonaise import *
from tkinter import *
from module_pile import Pile

# création de la pile

p=Pile()
# Création de la fenêtre

fenetre = Tk()
fenetre.geometry("320x320")


# Creation des frames

input_frame = Frame(fenetre, width=300, height=50, bd=0, highlightbackground="black", highlightcolor="black", highlightthickness=2)
input_frame.pack(side=TOP)

btns_frame = Frame(fenetre, width=300, height=272.5, bg="grey")
btns_frame.pack()

# Création de la zone de saisie

expression = StringVar()
saisie = Entry(input_frame,textvariable=expression,width="300",font=('arial', 18, 'bold'))
saisie.grid(row = 0,column=0)

# fonction

affiche = ""

def bouton_click(item):
    global affiche
    temp = ""
    car = str(item)
    if car not in ['+','*','-','/',' '] :
            temp = temp + str(item)
            affiche = affiche + str(item)
            expression.set(affiche)
    else :
             if temp !='' :
                 p.empiler(int(temp))
                 temp = ''
                
             if car !=' ':
                 
            
                a = p.depiler()
                b = p.depiler()
                if car == "*" :
                    p.empiler(a*b)
                    affiche = str(a*b)
                if car == "+":
                    p.empiler(a+b)
                    affiche = str(a+b)
                    
                if car == "/" :
                    p.empiler(b/a)
                    affiche = str(b/a)
                if car =="-":
                    p.empiler(b-a)
                    affiche = str(b-a)
                expression.set(affiche)

def calcul():
       
    result = str(npi(expression.get()))
    saisie.delete(0,"end")
    saisie.insert(0,result)
    
def bouton_clear():
    global affiche
    affiche = ""
    expression.set(affiche)
# Creation d'un bouton



#Ligne du haut

clear = Button(btns_frame,text = "C",fg = "black",width = 24, height = 3, bd = 1, bg = "#fff", cursor = "hand2",command=lambda :bouton_clear())
clear.grid(row=0,column=0,columnspan=2,padx=0)

space =  Button(btns_frame,text = "Espace",fg = "black",width = 10, height = 3, bd = 1, bg = "#fff", cursor = "hand2",command=lambda :bouton_click(" "))
space.grid(row=0,column=2,padx = 1, pady = 1)

diviser = Button(btns_frame,text = "/",fg = "black",width = 10, height = 3, bd = 1, bg = "#fff", cursor = "hand2",command=lambda :bouton_click("/"))
diviser.grid(row=0,column=3,padx = 1, pady = 1)

# Ligne suivante

sept = Button(btns_frame,text = "7",fg = "black",width = 10, height = 3, bd =1, bg = "#fff", cursor = "hand2",command=lambda :bouton_click(7))
sept.grid(row=1,column=0,padx = 1, pady = 1)
huit = Button(btns_frame,text = "8",fg = "black",width = 10, height = 3, bd =1, bg = "#fff", cursor = "hand2",command=lambda :bouton_click(8))
huit.grid(row=1,column=1,padx = 1, pady = 1)
neuf = Button(btns_frame,text = "9",fg = "black",width = 10, height = 3, bd = 1, bg = "#fff", cursor = "hand2",command=lambda :bouton_click(9))
neuf.grid(row=1,column=2,padx = 1, pady = 1)
mul = Button(btns_frame,text = "*",fg = "black",width = 10, height = 3, bd = 1, bg = "#fff", cursor = "hand2",command=lambda :bouton_click("*"))
mul.grid(row=1,column=3,padx = 1, pady = 1)

# Ligne suivante

quatre = Button(btns_frame,text = "4",fg = "black",width = 10, height = 3, bd = 1, bg = "#fff", cursor = "hand2",command=lambda :bouton_click(4))
quatre.grid(row=2,column=0,padx = 1, pady = 1)
cinq = Button(btns_frame,text = "5",fg = "black",width = 10, height = 3, bd = 1, bg = "#fff", cursor = "hand2",command=lambda :bouton_click(5))
cinq.grid(row=2,column=1,padx = 1, pady = 1)
six = Button(btns_frame,text = "6",fg = "black",width = 10, height = 3, bd = 1, bg = "#fff", cursor = "hand2",command=lambda :bouton_click(6))
six.grid(row=2,column=2,padx = 1, pady = 1)

moins =  Button(btns_frame,text = "-",fg = "black",width = 10, height = 3, bd = 1, bg = "#fff", cursor = "hand2",command=lambda :bouton_click("-"))

moins.grid(row=2,column=3,padx = 1, pady = 1)
# Ligne suivante

un = Button(btns_frame,text = "1",fg = "black",width = 10, height = 3, bd = 1, bg = "#fff", cursor = "hand2",command=lambda :bouton_click(1))
un.grid(row=3,column=0,padx = 1, pady = 1)
deux = Button(btns_frame,text = "2",fg = "black",width = 10, height = 3, bd = 1, bg = "#fff", cursor = "hand2",command=lambda :bouton_click(2))
deux.grid(row=3,column=1,padx = 1, pady = 1)
trois = Button(btns_frame,text = "3",fg = "black",width = 10, height = 3, bd = 1, bg = "#fff", cursor = "hand2",command=lambda :bouton_click(3))
trois.grid(row=3,column=2,padx = 1, pady = 1)

plus =  Button(btns_frame,text = "+",fg = "black",width = 10, height = 3, bd = 1, bg = "#fff", cursor = "hand2",command=lambda :bouton_click("+"))

plus.grid(row=3,column=3,padx = 1, pady = 1)

# Dernière ligne

zero =  Button(btns_frame, text = "0",fg = "black",width = 24, height = 3, bd =1, bg = "#fff", cursor = "hand2",command=lambda :bouton_click(0))
zero.grid(row=4,column=0,columnspan=2,padx=0,pady=1)
point =  Button(btns_frame, text = ".",fg = "black",width = 10, height = 3, bd =1, bg = "#fff", cursor = "hand2",command=lambda :bouton_click("."))
point.grid(row=4,column=2,padx=1,pady=1)
egal = Button(btns_frame, text = "=",fg = "black",width = 10, height = 3, bd = 1, bg = "#fff", cursor = "hand2",command=lambda :calcul())
egal.grid(row=4,column=3,padx=1,pady =1)

# Affichage dans l'entrée



# Execution

fenetre.mainloop()
    
    

# fin du fichier calculatrice_complete_npi.py


