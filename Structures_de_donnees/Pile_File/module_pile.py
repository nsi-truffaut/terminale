#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : module_pile.py
#  auteur : Gregory MAUPU
#    date : 2021/09/30


class Pile:
    
    def __init__(self):
        """Crée une Pile vide"""
        self.pile=[]
    
    def est_vide(self):
        """Renvoie si la Pile est vide"""
        return self.pile == []
    
    def empiler(self,e):
        """Ajoute un élément e au sommet de la Pile"""
        self.pile.append(e)
        
    def depiler(self):
        """Renvoie le sommet de la pile et modifie la pile en enlevant le sommet"""
        if self.est_vide():
            raise IndexError('Dépiler une pile vide !') #Levée d'une exception
        sommet = self.pile[len(self.pile)-1]
        self.pile = self.pile[0:len(self.pile)-1]
        return sommet
    
    def __repr__(self):
        s = ""
        if self.est_vide():
            return s
        else :
            j = 0
            while j < len(self.pile)-1:
                s = s + "|" + str(self.pile[len(self.pile)-1-j])+"|" +"\n"
                j = j+1
        return s + "|" + str(self.pile[len(self.pile)-1])+"|" 

if __name__=="__main__":

    from random import *
    p = Pile()
    for k in range(5):
        p.empiler(randint(-10,10))
    print(p)
    print(p.depiler())
    q = Pile()
    q.depiler()
