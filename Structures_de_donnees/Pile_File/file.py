#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : file.py
#  auteur : Gregory MAUPU
#    date : 2022/10/17

# Implémentation par liste chainee

from liste_chainee import *
from module_pile import Pile

class File :
    
    def __init__(self):
        """ Initialise une file vide
        """
        self.contenu = Liste_chainee()
        
    def est_vide(self) :
        """ Renvoie True si la file est vide, False sinon
        """
        return self.contenu.est_vide()
    
    def enfiler(self,e):
        """ Ajoute un élément à la File"""
        self.contenu.ajouter_fin(e)
    
    def defiler(self):
        """ Défile l'élément de tête et le renvoie"""
        if self.est_vide() :
            raise IndexError("Défiler une file vide")
        else :
            return self.contenu.supprimer_en_tete()
    
    def __repr__(self) :
        """ Réprésentation d'une file. Le premier élément enfilé est le plus à droite
        """
        maillon = self.contenu.premier
        s=""
        while maillon is not None :
            s = str(maillon.t) + '->' + s
            maillon = maillon.s
        return s
    
# Implémentation à l'aide des listes

class File_l :
    
    def __init__(self):
        """ Initialise une file vide
        """
        self.contenu =[]
        
    def est_vide(self) :
        """ Renvoie True si la file est vide, False sinon
        """
        return self.contenu == []
    
    def enfiler(self,e):
        """ Ajoute un élément à la File"""
        self.contenu.append(e)
    
    def defiler(self):
        """ Défile l'élément de tête et le renvoie"""
        if self.est_vide() :
            raise IndexError("Défiler une file vide")
        else :
            a = self.contenu[0]
            self.contenu = self.contenu[1:len(self.contenu)]
            return a
    
    def __repr__(self) :
        """ Réprésentation d'une file. Le premier élément enfilé est le plus à droite
        """
        s=""
        for i in range(len(self.contenu)-1) :
            s = s + str(self.contenu[len(self.contenu)-1-i]) + '->'
        return s + str(self.contenu[0])

# implémentation à l'aide de deux piles

class File_2p :

    def __init__(self):
        """ Initialise une file vide
        """
        self.entree = Pile()
        self.sortie = Pile()
        
    def est_vide(self) :
        """ Renvoie True si la file est vide, False sinon
        """
        return self.entree.est_vide() and self.sortie.est_vide()
    
    def enfiler(self,e):
        """ Ajoute un élément à la File"""
        self.entree.empiler(e)
    
    def defiler(self):
        """ Défile l'élément de tête et le renvoie"""
        if self.est_vide() :
            raise IndexError("Défiler une file vide")
        else :
            if self.sortie.est_vide() :
                while not self.entree.est_vide() :
                    self.sortie.empiler(self.entree.depiler())
            return self.sortie.depiler()
    
    def __repr__(self) :
        """ Réprésentation d'une file. Le premier élément enfilé est le plus à droite
        """
        s=""
        if self.est_vide() :
            return s
        else :
            temp_sortie = Pile()
            temp_entree = Pile()
            if self.entree.est_vide() and not self.sortie.est_vide():
                while not self.sortie.est_vide() :
                    a = self.sortie.depiler()
                    temp_sortie.empiler(a)
                    s = '->' + str(a) + s
            elif self.sortie.est_vide() and not self.entree.est_vide() :
                while not self.entree.est_vide() :
                    a = self.entree.depiler()
                    temp_entree.empiler(a)
                    s = s + '->' + str(a)
            else :
                 while not self.sortie.est_vide() :
                    a = self.sortie.depiler()
                    temp_sortie.empiler(a)
                    s = '->' + str(a) + s
                 while not self.entree.est_vide() :
                    a = self.entree.depiler()
                    temp_entree.empiler(a)
                    s = '->' + str(a) + s
            while not temp_sortie.est_vide() :
                    self.sortie.empiler(temp_sortie.depiler())
            while not temp_entree.est_vide() :
                    self.entree.empiler(temp_entree.depiler())
                    
        return s[2:len(s)] 

if __name__ == "__main__" :

    # Test implémentation 2
##    f = File_l()
##    f.enfiler(5)
##    f.enfiler(8)
##    f.enfiler(6)
##    print(f)
##    print(f.defiler())
##    print(f)
    # Test implémentation 6
    f = File_2p()
    f.enfiler(5)
    f.enfiler(8)
    f.enfiler(6)
    print(f)
    print(f.defiler())
    print(f)
    f.enfiler(22)
    print(f)
# fin du fichier file.py

