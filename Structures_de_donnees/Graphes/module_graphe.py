class GrapheMa :

    def __init__(self,sommets):
        self.sommet = sommets
        self.n = len(sommets)
        self.ma = [[0 for i in range(self.n)] for j in range(self.n)]

    def get_ma(self):
        return self.ma
    
    def sommets(self):
        return self.sommet

    def ajouter_arc(self,s1,s2) :
        """
        Permet d'ajouter l'arc de s1 vers s2
        Attention avec les index, on a une inversion sur ligne/colonne
        """
        i = self.sommet.index(s1)
        j = self.sommet.index(s2)
        self.ma[j][i] = 1
        
    def ajouter_arete(self,s1,s2):
        i = self.sommet.index(s1)
        j = self.sommet.index(s2)
        self.ma[i][j] = 1
        self.ma[j][i] = 1

    def voisins(self,s):
        i = self.sommet.index(s)
        v = []
        for j in range(len(self.ma)):
            if self.ma[j][i] == 1 :
                v.append(self.sommet[j])
        return v

    def __repr__(self):
        return str(self.ma)

class GrapheD :

    def __init__(self,sommets):
        self.s = sommets
        self.dic = dict()
    # def __init__(self):
    #    self.dic = {}


    def ajouter_sommet(self,s):
        self.dic[s]=[]

             
    def ajouter_arete(self,s1,s2):
        if s1 not in self.dic:
            self.dic[s1]=[s2]
        else :
            self.dic[s1].append(s2)
        if s2 not in self.dic:
            self.dic[s2]=[s1]
        else :
            self.dic[s2].append(s1)

    def sommets(self):
        return self.s

    def ajouter_arc(self,s1,s2):
        if s1 not in self.dic:
            self.dic[s1]=[s2]
        else :
            self.dic[s1].append(s2)
            
    def voisins(self,s):
        return self.dic[s]

    def __repr__(self):
        return str(self.dic)



def Dic_vers_Mat(G,o = False):
    s = G.sommets()
    Gm = GrapheMa(s)
    for sommet in s :
        for v in G.voisins(sommet) :
            if not o :
                Gm.ajouter_arete(sommet,v)
            else :
                Gm.ajouter_arc(sommet,v)
                
    return Gm
    

def Mat_vers_Dic(G):
    s = G.sommets()
    Gd = GrapheD(G.sommets())
    for som in s :
        Gd.ajouter_sommet(som)
    n = len(s)
    l = G.get_ma()
    for i in range(n) :
        for j in range(n) :
                if l[i][j] == 1  :
               
                   Gd.ajouter_arc(s[j],s[i])
    return Gd

if __name__=='__main__' :

    # Test graphe non orienté
    
    G = GrapheD(["A","B","C","D","E"])
    G.ajouter_arete("A","C")
    G.ajouter_arete("A","E")
    G.ajouter_arete("A","D")
    G.ajouter_arete("A","B")
    G.ajouter_arete("B","D")
    G.ajouter_arete("E","D")
    G.ajouter_arete("E","C")
    Gm = Dic_vers_Mat(G)
    print(Gm)
    Gd = Mat_vers_Dic(Gm)
    print(Gd)
    
    # Test graphe orienté

    Go = GrapheMa(["A","B","C","D","E"])
    Go.ajouter_arc("B","C")
    Go.ajouter_arc("A","E")
    Go.ajouter_arc("B","D")
    Go.ajouter_arc("A","B")
    Go.ajouter_arc("B","E")
    Go.ajouter_arc("D","C")
    print(Go)
    GoD = Mat_vers_Dic(Go)
    print(GoD)
    
    
        
        
