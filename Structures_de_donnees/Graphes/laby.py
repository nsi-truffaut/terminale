#!/usr/bin/python3
# -*- coding: utf-8 -*-

from tkinter import *
import random
from module_graphe import *

CAN_WIDTH = 800
CAN_HEIGHT = 600
BG_COLOR = 'black'


def attribuerPoids(G):
    """Attribue aléatoirement des poids aux arêtes du graphe G"""
    poids = {}
    som = G.sommets()
    for s in som:
        for v in G.voisins(s) :
            x = random.uniform(0,1)
            poids[(s,v)]= x
            poids[(v,s)] = x
            
    return poids

def algoPrim(G,s) :
    A = GrapheD(G.sommets()) # Un graphe presque vide...
    pere = {}
    nonVu = {}
    vu = set()
    poidsAretes = attribuerPoids(G)
    for u in G.sommets() :
        nonVu[u]= float('inf') # à chaque sommet on associe un poids infini
        pere[u] = u # chaque sommet est son propre père
          

    
    
     # La file de priorité
   
    
    
    
    s = (0,0) # tous les sommets ont poids infini sauf s qui a 0
    nonVu[s] = 0
    while nonVu !={} :
       u = min(nonVu, key=nonVu.get)
       A.ajouter_arete(u,pere[u])
       vu.add(u)
       for v in G.voisins(u):
            if v not in vu:
                if poidsAretes[(u,v)] < nonVu[v] :
                    pere[v] = u # nouveau père pour v
                    nonVu[v] = poidsAretes[(u,v)]
       nonVu.pop(u)
    return A

fen_princ = Tk() #création d'une fenetre
fen_princ.geometry("900x900") #taille de la fenetre : 900x900
monCanvas = Canvas(fen_princ, width=800, height=800, bg='white',border = 10) #widget canvas
#il permet de dessiner des formes diverses
monCanvas.pack() #place le widget dans la fenetre
#fen_princ.bind('<KeyPress-r>', onkeypressed) #permet d'appeler onkeypressed lors l'appuie sur la touche <r>
#def onkeypressed(event): forme de la fonction onkeypressed
#...
#monCanvas.create_rectangle(x, y, x + 40, y + 40, fill='blue') #création d'un rectangle de couleur bleue
#de dimensions 40x40
 #lance le gestionnaire d'événements qui interceptera les actions de l'utilisateur

def represente_laby(canevas,G):
    som = G.sommets()
    print("s",som)
    for s in som :
        if G.voisins(s) == [] :
            canevas.create_rectangle(s[0], s[1], s[0] + 40, s[1] + 40, fill='black')
        else :
            for v in G.voisins(s):
                if v[0] == s[0]+1 :
                    canevas.create_rectangle(s[0]+40, s[1], s[0] + 80, s[1] + 40, fill='white')
                if v[1] == s[1]+1 :
                    canevas.create_rectangle(s[0], s[1]+40, s[0] + 40, s[1] + 80, fill='white')
                if v[0] == s[0]+1 and v[1] == s[1]+1 :
                    canevas.create_rectangle(s[0]+40, s[1]+40, s[0] + 80, s[1] + 80, fill='white')
        
if __name__ == '__main__':
    #main()
    L = []
    L = [(i,j) for i in range(8) for j in range(8)]
    K = [[(i,j) for i in range(8)] for j in range(8)]
    
    G = GrapheD(L)
    for k in range(len(K) -1) :
        for i in range(len(K)-1) :
        
            G.ajouter_arete(K[k][i],K[k+1][i])
    print(G)    
    A = algoPrim(G,(0,0))
    #print(A)
    represente_laby(monCanvas,G)            
    fen_princ.mainloop()
    
