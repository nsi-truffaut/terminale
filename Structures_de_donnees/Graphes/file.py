#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : file.py
#  auteur : Gregory MAUPU
#    date : 2021/09/03

from liste_chainee import *

class File :
    
    def __init__(self):
        """ Initialise une file vide
        """
        self.contenu = Liste_chainee()
        
    def est_vide(self) :
        """ Renvoie True si la file est vide, False sinon
        """
        return self.contenu.est_vide()
    
    def enfiler(self,e):
        """ Ajoute un élément à la File"""
        self.contenu.ajouter_fin(e)
    
    def defiler(self):
        """ Défile l'élément de tête et le renvoie"""
        if self.est_vide() :
            raise IndexError("Défiler une file vide")
        else :
            return self.contenu.supprimer_en_tete()
    
    def __repr__(self) :
        """ Réprésentation d'une file. Le premier élément enfilé est le plus à droite
        """
        maillon = self.contenu.premier
        s=""
        while maillon is not None :
            s = str(maillon.t) + '->' + s
            maillon = maillon.s
        return s
    
if __name__=="__main__":
    
    f = File()
    f.enfiler(2)
    f.enfiler(3)
    print(f)
    
    
# fin du fichier file.py

