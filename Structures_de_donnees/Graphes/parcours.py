from module_graphe import GrapheD,GrapheMa
from file import File
from module_pile import Pile

# Classe pour un graphe pondérée

class GrapheDP :

    def __init__(self):
        self.dic = dict()
        self.s = []
    # def __init__(self):
    #    self.dic = {}

    def get_taille(self):
        return len(self.sommets())
    
    def ajouter_sommet(self,s):
        self.dic[s]=[]

             
    def ajouter(self,s1,s2,poids):
        if s1 not in self.dic:
            self.dic[s1]=[(s2,poids)]
        else :
            self.dic[s1].append((s2,poids))
        if s2 not in self.dic:
            self.dic[s2]=[(s1,poids)]
        else :
            self.dic[s2].append((s1,poids))

    def sommets(self):
        return self.dic.keys()

    def ajouter_arc(self,s1,s2):
        if s1 not in self.dic:
            self.dic[s1]=[s2]
        else :
            self.dic[s1].append(s2)
            
    def voisins(self,s):
        return self.dic[s]

    def __repr__(self):
        return str(self.dic)
# Parcours en largeur pour tout type de graphe

def parcours_en_largeur(G,sommet):
    f = File()
    f.enfiler(sommet)
    visite = [sommet]
    while not f.est_vide():
        a = f.defiler()
        voisin = G.voisins(a)
        for v in voisin :
            if v not in visite :
                visite.append(v)
                f.enfiler(v)
                
    return visite

# Parcours en largeur avec retour de la distance
# On remplace visite par un dictionnaire pour en tenir compte.

def parcours_en_largeur2(G,sommet):
    f = File()
    f.enfiler(sommet)
    visite = {sommet:0}
    
    while not f.est_vide():
        a = f.defiler()
        voisin = G.voisins(a)
        for v in voisin :
            if v not in visite.keys() :
                visite[v] = visite[a]+1
                f.enfiler(v)
       
    return visite

# Parcours en profondeur récursif pour tout type de graphe ?

def parcours_en_profondeur_rec(G,visite,sommet):
    if sommet not in visite :
        visite.append(sommet)
        for v in G.voisins(sommet) :
            parcours_en_profondeur_rec(G,visite,v)
    return visite

# Parcours en profondeur itératif à l'aide d'une pile pour tout type de graphe

def parcours_en_profondeur_it(G,sommet):
    
    p = Pile()
    p.empiler(sommet)
    visite = []
    while not p.est_vide():
        a = p.depiler()
        if a not in visite :
            visite.append(a)
        voisin = G.voisins(a)
        for v in voisin :
            if v not in visite :
                p.empiler(v)
                
    return visite

# Test de la présence d'un chemin
def chemin(G,s1,s2):
    return s2 in parcours_en_profondeur_rec(G,[],s1)



# Graphe connexe

def est_connexe(G):
    rep = True
    for s in G.sommets() :
        for som in G.sommets() :
            if not chemin (G,s,som) :
                rep =False
    return rep
# Algorithme qui renvoie le chemin entre 2 sommets u et v pour un graphe orienté

# On renvoie pour le parcours son prédécesseur (algo orienté)

def parcours_en_profondeur_bis(G,visite,org,sommet):
    if sommet not in visite :
        visite[sommet]=org
        for v in G.voisins(sommet) :
            parcours_en_profondeur_bis(G,visite,sommet,v)
         
    return visite

def chemin_bis(G,s1,s2) :
    #visite = {}
    #parcours_en_profondeur_bis(G,visite,None,s1)
    # Ne permet pas de détecter les cycles ! s1 !=s2 
    visite = parcours_en_profondeur_bis(G,{},None,s1)
    if s2 not in visite :
        return None
    
    ch = []
    s = s2
    while s is not None :
         ch.append(s)
         s = visite[s]
    ch.reverse()
       
    return ch 

### Algorithme de recherche de chaîne

# Algo qui renvoie le chemin, version itérative

def chemin_it(G,depart,arrivee):
    """ Ne permet pas de construire un chemin qui revient à son point de départ"""
    p = Pile()
    p.empiler((depart,[depart]))
    visite = [depart]
    while not p.est_vide():
        sommet,chemin = p.depiler()
        if sommet not in visite :
            visite.append(sommet)
        voisin = G.voisins(sommet)
##        new_voisin = []
##        for v in voisin :
##            if v not in chemin :
##                new_voisin.append(v)
##        for v in new_voisin :
##            if v == arrivee :
##                return chemin + [v]
##            else :
##                p.empiler((v,chemin+[v]))
        for v in voisin :
            if v not in visite :
                p.empiler((v,chemin+[v]))
            if v == arrivee :
                return chemin + [v]
    return None # utile s'il n'y a pas de chemin entre départ et arrivée

## Algorithme de présence de cycle dans un graphe orienté

def parcours_cycle(G,couleur,s):
    if couleur[s] == "gris":
        return True
    if couleur[s] == "noir":
        return False
    couleur[s] = "gris"
    for v in G.voisins(s):
        print(s,couleur[s])
        return parcours_cycle(G,couleur,v)
    couleur[s] = "noir"
    
    return False

def cycle(G):
    couleur = {}
    for s in G.sommets():
        couleur[s]="blanc"
    for s in G.sommets():
        return parcours_cycle(G,couleur,s)
    return False

## Version itérative  dans un graphe non orienté
## Complètement foireuse
        
##def possede_cycle_it(G,s, parents : set):
##    p = Pile()
##    p.empiler(s)
##       
##    while not p.est_vide():
##        a = p.depiler()
##        if a in parents :
##            return True,parents
##        parents.add(a)
##        for voisin in G.voisins(a) :
##            
##            if voisin not in parents :
##                p.empiler(voisin)
##        print(p,parents)
##    return False

def possede_cycle_it(G,depart,prec) :
    p = File()
    visite = [depart]
    p.enfiler((depart,[depart]))
    prec[depart] = depart
    while not p.est_vide():
        courant,chaine = p.defiler()
        print(G.voisins(courant))
        for v in G.voisins(courant) :
           
            if v not in visite :
               p.enfiler((v,chaine+[v]))
               visite.append(v)
               prec[v] = courant
               
            else :
                
                if prec[courant] != v :
                    return True,visite
        print('file',p,'parent',prec,'visite',visite)
    return False
                    
## Version itérative - écriture cycle

def possede_cycle(G, sommet, parents : dict):
    p = Pile()
    p.empiler((sommet,None))
    while not p.est_vide() :
        s,P = p.depiler()
        if s in parents :
            return s,P
        parents[s] = P
        for voisin in G.voisins(s):
            if voisin not in parents:
                p.empiler((voisin,s))
        
    return None, None

def trouver_cycle(G,S):
    M = {}
    a,P = possede_cycle(G, S, M)
    if a is not None :
        L = [P,a]
        N = M[a]
        while N != P :
            L.append(N)
            N = M[N]
        return L
    return []
            

def candidats(A,G,s) :
    c = s
    d = math.inf
    for v,p in G.voisins(s) :
        if v not in A.sommets() :
            if p < d :
               d = p
               c = v
        candidat = (s,c,d)
    return candidat

def candidats(A,G,s) :
    choix = s
    cout_mini = math.inf
    for v in G.voisins(s) :
        if .................. :
            ........ :
               .....
               .....
        candidat = .......
    return candidat

def meilleur_candidat(liste_candidat) :
    return min(liste_candidat,key = lambda a : a[2])

def prim(G,s):
    A = GrapheDP()
    A.ajouter_sommet(s)
    n = G.get_taille()
    while A.get_taille() < n-1 :
        candidat = []
        for s in A.sommets():
                          
                
            candidat.append(candidats(A,G,s))
            
        
        cand = min(candidat,key = lambda a : a[2])
       
        visite.append(cand[1])
        A.ajouter(cand[0],cand[1],cand[2])
    return A
    
def arbre_couvrant_min(G,s):
    A = ...... # On crée un arbre vide
    A.ajouter_sommet(s)
    n = ...... # n est la taille du graphe G
    while ..... < n-1 : # condition sur la taille de l'arbre A
        liste_candidat = []
        for .........  :
            .......................
            
        meilleur_candidat = ............
       
        
        A.ajouter_sommet(.......................) # On ajoute le sommet à A
        ....................  # On ajoute l'arếte aussi à A
    return A


if __name__=='__main__' :
    
##    G = GrapheD(["A","B","C","D","E","F","G","H"])
##    G.ajouter_arete("A","D")
##    G.ajouter_arete("E","A")
##    G.ajouter_arete("A","B")
##    G.ajouter_arete("B","C")
##    G.ajouter_arete("D","C")
##    G.ajouter_arete("E","D")
##    G.ajouter_arete("F","E")
##    G.ajouter_arete("G","E")
##    G.ajouter_arete("F","G")
##    G.ajouter_arete("G","H")
    #print(G)
##    print(parcours_en_largeur(G,"A"))
##    print(parcours_en_profondeur_bis(G,{},None,"A"))
    #print(possede_cycle_it(G,"A",dict()))
    #print(trouver_cycle(G,"D"))
##   
##   
##    print(chemin_bis(G,"A","H"))
    # Graphe Orienté
##    Go = GrapheMa(["A","B","C","D","E","F","G"])
##    Go.ajouter_arc("C","B")
##    Go.ajouter_arc("E","A")
##    Go.ajouter_arc("B","D")
##    Go.ajouter_arc("A","B")
##    Go.ajouter_arc("B","E")
##    Go.ajouter_arc("C","D")
##    Go.ajouter_arc("A","F")
##    Go.ajouter_arc("F","G")
##    print(parcours_en_profondeur_it(Go,"A"))
##    print(chemin_it(Go,"A","C"))
##    print(chemin_bis(Go,"A","C"))
##    G = {}
##    G["France"] = ["Bresil","Suriname"]
##    G["Argentine"] = ["Bolivie","Bresil","Chili","Paraguay","Uruguay"]
##    G["Bolivie"] = ["Argentine","Bresil","Chili","Paraguay","Perou","Uruguay"]
##    G["Bresil"] = ["Argentine","Bolivie","Colombie","France","Guyana","Paraguay","Perou","Suriname","Uruguay","Venezuela"]
##    G["Chili"]=["Argentine","Bolivie","Perou"]
##    G["Colombie"] = ["Bresil","Equateur","Perou","Venezuela"]
##    G["Equateur"] = ["Colombie","Perou"]
##    G["Guyana"] = ["Bresil","Suriname","Venezuela"]
##    G["Paraguay"] = ["Argentine","Bolivie","Bresil"]
##    G["Perou"] = ["Bolivie","Bresil","Chili","Colombie","Equateur"]
##    G["Suriname"] = ["Bresil","France","Guyana"]
##    G["Uruguay"] = ["Argentine","Bolivie","Bresil"]
##    G["Venezuela"] = ["Bresil","Colombie","Guyana"]
##    l = []
##    for cle in G :
##        l.append(cle)
##    Pays = GrapheD(l)
##    
##    for cle in G :
##        for v in G[cle]:
##                Pays.ajouter_arete(cle,v)
   
        
    #print(parcours_en_profondeur22(Pays,"Bolivie"))
        
##    print(parcours_en_largeur(G,"A"))
##    print(parcours_en_profondeur(G,[],"A"))
    ################
    d = {"A":["B"],
          "B": ["A","C"],
          "C":["B","D"],
          "D":["B","C"],
          }
    l=[]
    
    for cle in d :
        l.append(cle)
    G1 = GrapheD(l)
    
    for cle in d :
        if d[cle] != [] :
            for v in d[cle]:
                G1.ajouter_arc(cle,v)
        else :
            G1.ajouter_sommet(cle)
           #print(parcours_en_profondeur2(G,"A","D"))
    
    #print(cycle(G))
    # Pour Prim
    import math
    G = GrapheDP()
    s = ["A","B","C","D","E","F","G"]
    for c in s :
        G.ajouter_sommet(c)
    G.ajouter("A","B",1)
    G.ajouter("A","C",4)
    G.ajouter("A","D",11)
    G.ajouter("A","F",10)
    G.ajouter("B","F",7)
    G.ajouter("B","C",2)
    G.ajouter("B","D",13)
    G.ajouter("B","F",7)
    G.ajouter("C","D",8)
    G.ajouter("C","E",9)
    G.ajouter("C","F",12)
    G.ajouter("D","E",3)
    G.ajouter("E","F",6)
    print(G)
    
