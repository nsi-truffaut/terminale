from module_liste import *

class File :
    
    def __init__(self):
        self.contenu = Liste_chainee()
        
    def est_vide(self) :
        return self.contenu.est_vide()
    
    def enfiler(self,e):
        self.contenu.ajouter_fin(e)
    
    def defiler(self):
        return self.contenu.supprimer_debut()
    
    def __repr__(self) :
        return "{}".format(self.contenu)
    
f = File()
f.enfiler(2)
f.enfiler(3)
print(f)
f.defiler()
print(f)

