#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : arbre_recherche.py
#  auteur : Gregory MAUPU
#    date : 2022/12/5

# Importation des modules

from arbre import *
# Implémentation de l'arbre binaire de recherche - Une classe

class ABR :

    def __init__(self,racine = None) :
        self.racine = racine
        if racine is not None :
            self.gauche = ABR()
            self.droite = ABR()

    def est_vide(self):
        return self.racine is None

    def insere(self,x) :
        if self.est_vide() :
            self.racine = x
            self.gauche = ABR()
            self.droite = ABR()
        else :
            if x > self.racine :
                self.droite.insere(x)
            else :
                self.gauche.insere(x)

    def contient(self,x) :
        if self.est_vide() :
            return False
        else :
            if x == self.racine :
                return True
            elif x > self.racine :
                 return self.droite.contient(x)
            else :
                 return self.gauche.contient(x)

    def __repr__(self) :
        if self.est_vide() :
            return "()"
        else :
            return "({},{},{})".format(self.gauche.__repr__(),self.racine,self.droite.__repr__())
        

def minimum(abr) :
    if abr.est_vide() :
        raise Exception("Arbre vide")
    elif abr.gauche.est_vide() :
            return abr.racine
    else :
        return minimum(abr.gauche)

def maximum(abr) :
    if abr.est_vide() :
        raise Exception("Arbre vide")
    elif abr.droite.est_vide() :
            return abr.racine
    else :
        return maximum(abr.droite)

def est_ABR(arbre, mini, maxi) :
    """ Teste si un arbre binaire issu de la classe arbre est un ABR"""
    if arbre is None :
        return True
    elif arbre.v is None :
        return True
    elif arbre.v > maxi or arbre.v < mini :
        return False
    else :
        return est_ABR(arbre.g,mini,arbre.v) and est_ABR(arbre.d,arbre.v,maxi)

def isBST(arbre):
    return est_ABR(arbre,e_min(arbre),e_max(arbre))
                   
### Implémentation des ABR avec deux classes

class Noeud :

    def __init__(self,gauche,valeur,droite):
        self.cle = valeur
        self.g = gauche
        self.d = droite

    def __repr__(self):
        if self is None :
            return "()"
        else :
            return "({},{},{})".format(self.g,self.cle,self.d)

    # On écrit une méthode ajoute Noeud pour expliquer comment raccorder deux noeuds

class ABR2 :

    def __init__(self,racine=None):
        self.racine = racine # Ici il faut comprendre que ce sera un noeud

    def est_vide(self) :
        return self.racine is None
    
    def insere(self,x):
        if self.racine is None :
            self.racine = Noeud(ABR2(),x,ABR2())
        else :
            if x > self.racine.cle : # On utilise la méthode de Noeud
                self.racine.d.insere(x)
            elif x < self.racine.cle :
                self.racine.g.insere(x)

    def minimum(self) :
        if self.est_vide():
            raise ValueError("L'arbre est vide, le minimum n'existe pas")
        elif self.racine.g.est_vide() and self.racine.d.est_vide():
            return self.racine.cle
        else :
            return self.racine.g.minimum()

    def __repr__(self):
       
        if self.racine is None:
            return "()"
        else :
            while self.racine is not None :
                return  "{}".format(self.racine)




        
# Fonction qui renvoie les extrema d'un ABR
# Dans un ABR, le minimum est la feuille la plus à gauche, le maximum la feuille la plus à droite

##def minimum(arbre):
##    if arbre is None :
##        raise ValueError ("Arbre vide")
##    else :
##        mini = arbre.cle
##        if arbre.g is not None :
##           mini =min(arbre.cle,minimum(arbre.g))
##    return mini
            
##def maximum(arbre):
##    if arbre is None :
##        raise ValueError ("Arbre vide")
##    else :
##        maxi = arbre.v
##        if arbre.d is not None :
##           maxi =min(arbre.v,minimum(arbre.d))
##    return maxi



if __name__=="__main__" :

    # Fonction qui crée un ABR à partir d'une liste
    
    def inserer_valeurs(abr,l):
        for elt in l :
            abr.insere(elt)
            
    # Création d'exemples
    # Exemple 1
    a = ABR()
    l = [25, 60, 35, 10, 5, 20, 65, 45, 70, 40, 50, 55, 30, 15]
    inserer_valeurs(a,l)
    print("Affichage de l'ABR a")
    print(a)
    print(a.contient(20))
    print(a.contient(133))
    print("Affichage du minimum")
    print(minimum(a))
    
    print("---------------------------------")
    # Exemple 2 - Avec la deuxième implémentation
    a2 = ABR2()
    inserer_valeurs(a2,l)
    print("Affichage de l'ABR a2")
    print(a2)
    
    print("---------------------------------")
    # Exemple 2
    #print(minimum(a))
    #print(minimum(a))
    L = [22,20,33,10,21,31,35,9,13,7,4,12,15]
    b = ABR()
    inserer_valeurs(b,L)
    print("Affichage de l'ABR b")
    print(b)
    print("---------------------------------")
    c = Arbre(8,
              Arbre(4,
                    Arbre(-2,None,None),Arbre(6,None,None)),
              Arbre(89,
                    Arbre(5,
                          Arbre(9,None,None),Arbre(17,None,None)),
                    Arbre(90,None,None)))
    print("Test ABR pour l'arbre c")
    print(e_max(c))
    print(e_min(c))
    print(isBST(c))

