#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : arbre.py
#  auteur : Gregory MAUPU
#    date : 2021/11/16

#Importation des modules

from file import File

# Implémentation de l'arbre

class Arbre :

    def __init__(self,valeur,gauche,droit):
        self.v = valeur
        self.g = gauche
        self.d = droit

    def est_feuille(self):
        """ Revoie un booléen. Indique si un arbre est une feuille"""
        return self.g is None and self.d is None

    def hauteur(self):
        """ Renvoie la hauteur d'un arbre. Ici, l'arbre vide a une hauteur de 0"""
        if self.g is None or self.d is None :
                return 1
        elif self.est_feuille():
            return 1
        else :
            return 1 + max(self.g.hauteur(),self.d.hauteur())

    def __len__(self):
        """ Renvoie la taille d'un arbre."""
        def taille(arbre) :
            if arbre is None :
                return 0
            elif arbre.est_feuille() :
                return 1
            else :
                return 1 + taille(arbre.g)+ taille(arbre.d)
        return taille(self)

    def parcours_prefixe(self):
        result = []
        if self.est_feuille() :
            return [self.v]
        elif self.g is None :
            result.append(self.v)
            result = result + self.d.parcours_prefixe()
            return result
        elif self.d is None:
            result.append(self.v)
            result = result + self.g.parcours_prefixe() 
            return result
           
        
        else :
            result.append(self.v)
            result = result + self.g.parcours_prefixe() + self.d.parcours_prefixe()
        return result
      
    
    def parcours_postfixe(self):
        result = []
               
        if self.est_feuille() :
            return [self.v]
        elif self.g is None :
              result = result + self.d.parcours_postfixe()
              result.append(self.v)
              
        elif self.d is None :
             result = result + self.g.parcours_postfixe()
             
             return result
        else :
            result = result + self.g.parcours_postfixe() + self.d.parcours_postfixe()
            result.append(self.v)
        return result

    def parcours_infixe(self):
        result = []
        if self.est_feuille() :
             return [self.v]
        elif self.g is None :
              
              result.append(self.v)
              result = result + self.d.parcours_infixe()
              
        elif self.d is None :
             
             result.append(self.v)
             result = result + self.g.parcours_infixe()
             
        
        else :
            result = result + self.g.parcours_infixe()
            result.append(self.v)
            result =  result + self.d.parcours_infixe()
        return result

    def parcours_en_largeur(self):
        """ Renvoie la liste des noeuds d'un arbre lors d'un parcours en largeur"""
        file =File()
        marque = []
        file.enfiler(self)
    
        while not file.est_vide()  :
            arbre_en_cours = file.defiler()
            marque.append(arbre_en_cours.v)
            if arbre_en_cours.g is not None :
                file.enfiler(arbre_en_cours.g)
                
            if arbre_en_cours.d is not None :
                file.enfiler(arbre_en_cours.d)
           
        return marque

    def __repr__(self):
        if self is None :
            return "()"
        if self.est_feuille() :
            return self.v
        else :
        
            return "({},{},{})".format(self.g.__repr__(),self.v,self.d.__repr__())


        

#Fonctions autres

def somme(arbre) :
    if arbre is None :
        return ""
    else :
        return arbre.v + somme(arbre.g) + somme(arbre.d)

def e_max(arbre):
    if arbre is None :
        return -100
    if arbre.g is None and arbre.d is None :
        return arbre.v
    else :
        
        return max(arbre.v,e_max(arbre.g),e_max(arbre.d))

    
if __name__=="__main__" :
    # 5 exemples d'arbres
    
    a = Arbre(4,
              Arbre(8,
                    Arbre(-2,None,None),Arbre(6,None,None)),
              Arbre(89,
                    Arbre(15,
                          Arbre(1,None,None),Arbre(2,None,None)),
                    Arbre(17,None,None)))
    b = Arbre("E",
              Arbre("C",
                    Arbre("T",Arbre("M",None,None),Arbre("E",None,None)),
                    Arbre("Y",None,
                          Arbre("C",
                                Arbre("L",Arbre("H",None,None),Arbre("Y",None,None)),None)
                          )),
             Arbre("N",
                   Arbre("L",None,None),
                   Arbre("A",
                         Arbre("E",
                               Arbre("O",None,None),Arbre("H",None,None)),
                         Arbre("X",None,None))))
    c = Arbre(12,
              Arbre(3,
                    Arbre(17,Arbre(15,None,None),Arbre(-2,None,None)),
                    Arbre(3,None,
                          Arbre(4,
                                Arbre(10,Arbre(8,None,None),Arbre(14,None,None)),
                                None))),
             Arbre(152,
                   Arbre(31,None,None),
                   Arbre(15,
                         Arbre(3,
                               Arbre(7,None,None),Arbre(25,None,None)),
                         Arbre(32,None,None))))
    d = Arbre(11,
              Arbre(9,
                    Arbre(7, Arbre(4,None,Arbre(5,None,None)),Arbre(8,None,None)),
                    Arbre(10,None,None)),
              Arbre(13,None,None))

    e = Arbre(2,
            Arbre(
                5,Arbre(8,None, None),Arbre(4,None,None)
                ),Arbre(3,None,None))
    g = Arbre(7,
              Arbre(6,
                    Arbre(3,None,
                    Arbre(2,None,None)),
                    Arbre(4,None,None)),
              Arbre(1,
                    Arbre(2,None,None),None))

    # Affichage des arbres
    print(a)
    print(b)
    print(c)
    print(d)
    print(e)
    print(g)
    # Affichage de la taille et de la hauteur
    print(len(a),a.hauteur())
    print(len(b),b.hauteur())
    print(len(c),c.hauteur())
    print(len(d),d.hauteur())
    print(len(e),e.hauteur())
    print(len(g),g.hauteur())
    # Test des parcours pour les différents arbres
    print("Test des parcours prefixe")
    print(a.parcours_prefixe())
    print(b.parcours_prefixe())
    print(c.parcours_prefixe())
    print(d.parcours_prefixe())
    print(e.parcours_prefixe())
    print("Test des parcours postfixe")
    print(a.parcours_postfixe())
    print(b.parcours_postfixe())
    print(c.parcours_postfixe())
    print(d.parcours_postfixe())
    print(e.parcours_postfixe())
    print("Test des parcours infixe")
    print(a.parcours_infixe())
    print(b.parcours_infixe())
    print(c.parcours_infixe())
    print(d.parcours_infixe())
    print(e.parcours_infixe())
    print("Test des parcours en largeur")
    print(a.parcours_en_largeur())
    print(b.parcours_en_largeur())
    print(c.parcours_en_largeur())
    print(d.parcours_en_largeur())
    print(e.parcours_en_largeur())
    

    
    

