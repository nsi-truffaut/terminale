#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : module_liste.py
#  auteur : Gregory MAUPU
#    date : 2021/09/28

def afficher(lst):
    if lst is None :
        return ""
    else :
        return "{} -> {}".format(lst.t,afficher(lst.s))

def taille(lst):
    """Renvoie la longueur de la liste chaînée"""
    if lst is None :
        return 0
    else :
        return 1 + taille(lst.s)

class Maillon:
    def __init__(self,tete,suivant):
        self.t = tete
        self.s = suivant

    def __repr__(self):
        return afficher(self)
    
class Liste_chainee :

    def __init__(self):
        """ Initialise la liste chaînée par une maillon vide"""
        self.premier = None
       
    def __len__(self):
        """ Renvoie la taille de la liste"""
        return taille(self.premier)
    
    def est_vide(self):
        """ Teste si la liste chaînée est vide"""
        return self.premier is None

    def ajouter_debut(self,e):
        """Ajoute un maillon e en début de liste"""
        self.premier = Maillon(e,self.premier)

    def ajouter_fin(self,e) :
        """Ajoute un maillon e en fin de liste"""
        if self.premier is None :
            self.premier = Maillon(e,None)
        else:
            precedent = self.premier
            courant = self.premier.s
            while courant != None:
                precedent = courant
                courant = courant.s
            dernier = Maillon(e,None)
            precedent.s = dernier
        return self.premier
        

    def supprimer_debut(self):
        element = self.premier.t
        self.premier = self.premier.s
        return element

if __name__=="__main__" :
    L = Liste_chainee()
    L.ajouter_debut(2)
    L.ajouter_debut(3)
    print(L.supprimer_debut())
    
# fin du fichier module_liste.py

    
