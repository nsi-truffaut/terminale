#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : arbre.py
#  auteur : Gregory MAUPU
#    date : 2021/11/16

#Importation des modules

from file import File

# Implémentation de l'arbre
# On définit la classe Noeud et un arbre binaire est un ensemble de Noeud
# On définit ensuite les fonctions à l'extérieur (pas de méthode car c'est un noeud pas un arbre)

class Noeud : 

    def __init__(self,valeur,gauche,droit):
        self.g = gauche # Ici, c'est la valeur de la racine du sous-arbre gauche
        self.v = valeur
        self.d = droit

    def __repr__(self) :
        if self.g is None and self.d is None:
            return "((),{},())".format(self.v)
        else :
            return "({} , {} ,{})".format(self.g,self.v,self.d)

# Fonctions

def hauteur(arbre):
    """ Renvoie la hauteur d'un arbre. Ici, l'arbre vide a une hauteur de 0"""
    if arbre is None : # Ici a du sens car par construction, on arrive à la valeur None
        return 0
    else :
        return 1 + max(hauteur(arbre.g),hauteur(arbre.d))

def taille(arbre):
    """ Renvoie la taille d'un arbre."""
    
    if arbre is None :
        return 0
    elif arbre.v is None :
        return 0
    else :
        return 1 + taille(arbre.g)+ taille(arbre.d)

def est_feuille(arbre):
    """ Revoie un booléen. Indique si un arbre est une feuille"""
    return arbre.g is None and arbre.d is None




# Implémentation des parcours en profondeur

# Parcours prefixe

def parcours_prefixe(arbre):
      
    print(arbre.v,end = " ; ")
    if arbre.g is not None :
        parcours_prefixe(arbre.g)
    if arbre.d is not None :
        parcours_prefixe(arbre.d)

def parcours_prefixe2(arbre):
    result = []
    if arbre is None :
        return []
    if est_feuille(arbre) :
       result.append(arbre.v)
    else :
       result = [arbre.v]  + parcours_prefixe2(arbre.g) + parcours_prefixe2(arbre.d)
    return result
# Parcours postfixe

def parcours_postfixe(arbre):
    if arbre.g is not None :
        parcours_postfixe(arbre.g)
    if arbre.d is not None :
        parcours_postfixe(arbre.d)
    print(arbre.v,end=" ; ")

# parcours infixe
def parcours_infixe(arbre):
    if arbre.g is not None :
        parcours_infixe(arbre.g)
    print(arbre.v,end =" ; ")
    if arbre.d is not None:
        parcours_infixe(arbre.d)

# Parcours en largeur (BFS)
# Implémentation itérative

def parcours_en_largeur(arbre):
    """ Renvoie la liste des noeuds d'un arbre lors d'un parcours en largeur"""
    if arbre.v is None :
        raise Exception("Noeud vide")
    file =File()
    marque = []
    file.enfiler(arbre)
    
    while not file.est_vide()  :
        arbre_en_cours = file.defiler()
        marque.append(arbre_en_cours.v)
        if arbre_en_cours.g is not None :
                file.enfiler(arbre_en_cours.g)
                
        if arbre_en_cours.d is not None :
                file.enfiler(arbre_en_cours.d)
           
    return marque

# Fonctions autres

def somme(arbre) :
    if arbre is None :
        return 0
    elif arbre.v is None :
        raise Exception("Noeud vide")
    else :
        return arbre.v + somme(arbre.g) + somme(arbre.d)
import math

def e_max(arbre):
    if arbre is None :
        return -math.inf
    if arbre.g is None and arbre.d is None :
        return arbre.v
    else :
        
        return max(arbre.v,e_max(arbre.g),e_max(arbre.d))

def e_min(arbre) :
    if arbre is None :
        return +math.inf
    elif arbre.v is None :
        raise Exception("Noeud vide")
    elif arbre.g is None and arbre.d is None :
        return arbre.v
    else :
        return min(arbre.v,e_min(arbre.g),e_min(arbre.d))
    
def affiche(arbre):
    if arbre is None :
        return "()"
    elif arbre.v is None :
        return "()"
    elif est_feuille(arbre) :
        return "{}".format(arbre.v)
    else :
        
        return "({},{},{})".format(affiche(arbre.g),arbre.v,affiche(arbre.d))

def nb_feuilles(arbre):
     
    if arbre is None :
        return 0
    elif est_feuille(arbre):
        return 1
    else :
        return nb_feuilles(arbre.g) + nb_feuilles(arbre.d)

# Implémentation sous forme de méthode
# On définit une classe AB pour arbre binaire

class AB :

    def __init__(self,racine = None) :
        self.racine = Noeud(racine,None,None) 
        
    def est_feuille(self):
        return self.racine.g is None and self.racine.d is None

    def est_vide(self):
        return self.racine.v is None

    def hauteur(self):
        """ Renvoie la taille d'un arbre."""
        if self.est_vide() :
            return 0
        else :
           
           return 1 + max(self.racine.g.hauteur(),self.racine.d.hauteur())

    def taille(self):
        """
        Renvoie le nombre de noeuds dans l'arbre
        """

        if self.est_vide():
            return 0
        
        else :
            return 1 + self.racine.g.taille() + self.racine.d.taille()
        
    def prefixe(self):
        """ Renvoie la taille d'un arbre."""
        if self.est_vide() :
            return []
        
        else :
           return [self.racine.v] + self.racine.g.prefixe() + self.racine.d.prefixe()

    def __repr__(self):
        if self.est_vide():
            return "()"
        else :
            return "({},{},{})".format(self.racine.g.__repr__(),self.racine.v,self.racine.d.__repr__())

# Autre implémentation mais pas forcément correct au niveau abstrait car
# l'AB vide n'est pas alors un noeud vide mais un pointeur vers None

class AB2 :

    def __init__(self,racine = None):
        self.racine = racine
        if self.racine is not None :
            self.g = AB()
            self.d = AB()

    def est_vide(self):
        return self.racine is None

    def est_feuille(self) :
        return self.g.est_vide() and self.d.est_vide()

    def taille(self):
        if self.est_vide():
            return 0
        else :
            return 1 + self.g.taille() + self.d.taille()

    def hauteur(self):
        if self.est_vide():
            return 0
        else :
            return 1 + max(self.g.hauteur(),self.d.hauteur())

    def prefixe(self):
        """ Renvoie la taille d'un arbre."""
        if self.est_vide() :
            return []
        
        else :
           return [self.racine] + self.g.prefixe() + self.d.prefixe()

    def __repr__(self):
        if self.est_vide() :
            return "()"
        else :
            return "({},{},{})".format(self.g.__repr__(),self.racine,self.d.__repr__())


##def est_equilibre(arbre):
##    if arbre is None :
##        return ....
##    else :
##        balance = ....
##        rep = balance in [-1,0,1]
##        return rep  and ... and ....
    

    
if __name__=="__main__" :
    # 3 exemples d'arbres avec la première implémentation
    
    a = Noeud(4,
              Noeud(8,
                    Noeud(-2,None,None),Noeud(6,None,None)),
              Noeud(89,
                    Noeud(15,
                          Noeud(1,None,None),Noeud(2,None,None)),
                    Noeud(17,None,None)))
    b = Noeud("E",
              Noeud("C",
                    Noeud("T",Noeud("M",None,None),Noeud("E",None,None)),
                    Noeud("Y",None,
                          Noeud("C",
                                Noeud("L",Noeud("H",None,None),Noeud("Y",None,None)),None)
                          )),
             Noeud("N",
                   Noeud("L",None,None),
                   Noeud("A",
                         Noeud("E",
                               Noeud("O",None,None),Noeud("H",None,None)),
                         Noeud("X",None,None))))
    c = Noeud(12,
              Noeud(3,
                    Noeud(17,Noeud(15,None,None),Noeud(-2,None,None)),
                    Noeud(3,None,
                          Noeud(4,
                                Noeud(10,Noeud(8,None,None),Noeud(14,None,None)),
                                None))),
             Noeud(152,
                   Noeud(31,None,None),
                   Noeud(15,
                         Noeud(3,
                               Noeud(7,None,None),Noeud(25,None,None)),
                         Noeud(32,None,None))))
    d = Noeud(11,
              Noeud(9,
                    Noeud(7, Noeud(4,None,Noeud(5,None,None)),Noeud(8,None,None)),
                    Noeud(10,None,None)),
              Noeud(13,None,None))

    e = Noeud(2,
            Noeud(
                5,Noeud(8,None, None),Noeud(4,None,None)
                ),Noeud(3,None,None))

    f = Noeud(9,
              Noeud(6,
                    Noeud(3,
                          Noeud(2,None,None),None),
                    Noeud(7,None,None)),
              Noeud(15,
                   None,
                    Noeud(18,
                          Noeud(16,None,None),None)))

    g = Noeud(7,
              Noeud(6,
                    Noeud(3,None,
                    Noeud(2,None,None)),
                    Noeud(4,None,None)),
              Noeud(1,
                    Noeud(2,None,None),None))
    # Tests
    print(affiche(e))
    print("Test hauteur")
    print(hauteur(b))
    
    print(affiche(d))
    print(e_max(e))
    print(parcours_prefixe2(a))
    print('\n')
    print(parcours_prefixe2(a))
    print('\n')
    parcours_postfixe(b)
    print('\n')
    parcours_infixe(b)
    print('\n')
    print(parcours_en_largeur(b))
    print('\n')
    print("taille a : " ,taille(a))
    print(nb_feuilles(a))
    print('\n')
    print(somme(g))

    # Exemples avec la deuxième implémentation - Lourd à implémenter !
    # Il faut bien identifier chaque fils comme un objet de la classe AB et déclarer les arbres vides

    a1 = AB(4)
    a1.racine.g = AB(8)
    a1.racine.g.racine.g = AB(-2)
    a1.racine.g.racine.g.racine.g = AB()
    a1.racine.g.racine.g.racine.d = AB()
    a1.racine.g.racine.d = AB(6)
    a1.racine.g.racine.d.racine.g = AB()
    a1.racine.g.racine.d.racine.d = AB()
    a1.racine.d = AB(89)
    a1.racine.d.racine.g = AB(15)
    a1.racine.d.racine.g.racine.g = AB(1)
    a1.racine.d.racine.g.racine.g.racine.g = AB()
    a1.racine.d.racine.g.racine.g.racine.d = AB()
    a1.racine.d.racine.g.racine.d = AB(2)
    a1.racine.d.racine.g.racine.d.racine.g = AB()
    a1.racine.d.racine.g.racine.d.racine.d = AB()
    a1.racine.d.racine.d = AB(17)
    a1.racine.d.racine.d.racine.g = AB()
    a1.racine.d.racine.d.racine.d = AB()
    print(a1.hauteur())
    print(a1.prefixe())
    print(a1.taille())

    # Même arbre mais avec l'autre classe AB2
    a2 = AB2(4)
    a2.g = AB2(8)
    a2.g.g = AB2(-2)
    a2.g.d = AB2(6)
    a2.d = AB2(89)
    a2.d.g = AB2(15)
    a2.d.g.g = AB2(1)
    a2.d.g.d = AB2(2)
    a2.d.d = AB2(17)
    print(a2.hauteur())
    print(a2.prefixe())
    print(a2.taille())
    
    
