#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : liste_chainee_i.py
#  auteur : Gregory MAUPU
#    date : 2021/09/28

# Fonction impérative pour la classe Maillon
# Une liste chaînée est un ensemble de maillon

class Maillon:
    def __init__(self,tete,suivant):
        self.t = tete
        self.s = suivant

    def __repr__(self):
        return afficher(self)

def afficher(lst) :
    if lst is None :
        return ""
    else :
        precedent = lst.t
        courant = lst.s
        s = str(precedent)
        while courant is not None :
            s = s + "->" + str(courant.t)
            precedent = courant
            courant = courant.s
        return s + " ->"

# Ajoute un élément en début de chaîne - Renvoie une nouvelle chaîne

def ajouter_debut(lst,e):
    """Ajoute un maillon e en début de liste"""
    return Maillon(e,lst)
   
# Ajoute un élément en fin de chaîne, c'est une modification en place

def ajouter_fin(lst,e):
    if lst is None :
        return Maillon(e,None)
    if lst.s is None :
        lst.s = Maillon(e,None)
    else :
        precedent = lst.t
        courant = lst.s
        while courant is not None :
            precedent = courant
            courant = courant.s
        precedent.s = Maillon(e,None)
    
# Récupère le maillon d'indice i de la chaîne ou l'élément

def get_maillon(lst,i) :
    if lst is None:
            raise IndexError("La liste est vide")
    else :
        precedent = Maillon(lst.t,None) #lst.t pour la valeur
        courant = lst.s
        j = 0
        while j < i :
            precedent = courant
            courant = courant.s
            j=j+1
        return Maillon(precedent.t,None) # precedent.t pour la valeur
    
def inserer(lst,e,i):
        """ Insère un maillon en place i de la liste
        """
        if i == 0 :
            ajouter_debut(lst,e)
        else :
            j = 0
            precedent = Maillon(lst.t,None)
            courant = lst.s
            while j < i-1:
                precedent = courant
                courant = courant.s
                j=j+1
             
        precedent.s = Maillon(e,courant)
        return precedent
        

def supprimer(lst,i):
        """ supprime l'élément en place i de la liste
        """
        if lst is None :
            raise IndexError("La liste est vide")
        if i == 0 :
            return lst.s
       
        else :
            j = 0
            precedent = Maillon(lst.t,None)
            courant = lst.s
            
            while j < i-1:
                ajouter_fin(precedent,courant.t)
                           
                courant = courant.s
                j = j+1
            courant = courant.s
            while courant is not None :
                ajouter_fin(precedent,courant.t)
                courant = courant.s
            return precedent
                
           
            
        
if __name__=='__main__' :
    u = Maillon(1,Maillon(2,Maillon(3,Maillon(4,None))))
    e = Maillon(1,None)
    y= ajouter_fin(e,2)
    z = ajouter_fin(e,78)
    print(e)
    ajouter_fin(u,122)
    print('*',u)
    ajouter_fin(u,17)
    w = ajouter_debut(u,2000)
    print('***',w)
    print(u)
    z = inserer(w,1999,1)
    print('****',z)
    a = supprimer(z,7)
    print('++',a)

# fin du fichier liste_chainee_i.py
