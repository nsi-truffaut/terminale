class Maillon:
    def __init__(self,tete,suivant):
        self.t = tete
        self.s = suivant

    def ajouter(self,e):
        if self.s is None :
            self.s = Maillon(e,None)
        else :
            return Maillon(self.t,self.s.ajouter(e))

    def __len__(self):
        return 1
    
    def __repr__(self):
        if self.s is None :
            return "{} -> ".format(self.t)
        else :
            return "{} -> {}".format(self.t,self.s)

# Fonction auxiliaire

class ListeChainee :

    def __init__(self) :
        self.lst = None

    def __repr__(self):
        return "{}".format(self.lst)
##        if self is None :
##            return ""
##        else :
##            return "{} -> {}".format(self.lst.t,self.lst.s.__repr__())


    def ajouter_fin(self,e) :
        """Ajoute un maillon e en fin de liste"""
        if self.lst is None :
            self.lst = Maillon(e,None)
        else :
            self.lst.ajouter(e)
        

    def ajouter_debut(self,e):
        """Ajoute un maillon e en début de liste"""
        if self.lst is None :
            self.lst = Maillon(e,None)
        else :
            self.lst = Maillon(e,self.lst)

    def __len__(self):
        c = 1
        precedent = self.lst.t
        courant = self.lst.s
        while courant is not None :
            predecent = courant
            courant = courant.s
            c = c+1
        return c
            
        
                
            
            
            

def inserer(lst,e,i):
    """Insere un maillon e à la position i"""
    if i == 0 :
        return ajouter_debut(lst,e)
    else :
        return Maillon(lst.t,inserer(lst.s,e,i-1))

def supprimer(lst,i):
    if lst is None:
            raise IndexError("La liste est vide")
    if i==0 :
        return lst.s
    else :
        return Maillon(lst.t,supprimer(lst.s,i-1))

def taille(lst):
    """Renvoie la longueur de la liste chaînée"""
    if lst is None :
        return 0
    else :
        return 1 + taille(lst.s)

def get_maillon(lst,i):
    if lst is None:
            raise IndexError("La liste est vide")
    if i == 0 :
        return lst.t
    else :
        return get_maillon(lst.s,i-1)

    
if __name__=='__main__' :
    u = ListeChainee()
    u.ajouter_debut(2)
    u.ajouter_debut(1)
    u.ajouter_debut(6)
    #u = Maillon(1,Maillon(2,Maillon(3,Maillon(4,None))))
    print(u)
    
##    v = u.ajouter_fin(122)
##    print(v)
##    w = u.ajouter_debut(2000)
##    print(w)
##    z = inserer(w,1999,1)
##    print(z)
##    a = supprimer(z,5)
##    print(a)
