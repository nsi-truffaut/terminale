#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : module_liste.py
#  auteur : Gregory MAUPU
#    date : 2021/09/28


def taille(lst):
    """Renvoie la longueur de la liste chaînée"""
    if lst is None :
        return 0
    else :
        return 1 + taille(lst.s)

class Maillon:
    def __init__(self,tete,suivant):
        self.t = tete
        self.suivant = suivant

    def __repr__(self):
        if self.suivant is None :
            return "{} ->".format(self.t)
        else :
            return "{} -> {}".format(self.t,self.suivant)
    
class Liste_chainee :

    def __init__(self):
        """ Initialise la liste chaînée par une maillon vide"""
        self.premier = None
       
##    def __len__(self):
##        """ Renvoie la taille de la liste"""
##        if self.premier is None :
##            return 0
##        else :
##            precedent = self.premier
##            courant = precedent.suivant
##            c = 0
##            while courant is not None :
##                precedent = courant
##                courant = precedent.suivant
##                c = c + 1
##            return c
        
    def __len__(self):
        return taille(self.premier)
    
    def est_vide(self):
        """ Teste si la liste chaînée est vide"""
        return self.premier is None

    def ajouter_debut(self,e):
        """Ajoute un maillon e en début de liste"""
        self.premier = Maillon(e,self.premier)

    def ajouter_fin(self,e) :
        """Ajoute un maillon e en fin de liste"""
        if self.premier is None :
            return Maillon(e,None)
        else:
            precedent = self.premier
            courant = self.premier.suivant
            while courant != None:
                precedent = courant
                courant = courant.suivant
            dernier = Maillon(e,None)
            precedent.suivant = dernier

    def supprimer_debut(self):
        element = self.premier.t
        self.premier = self.premier.suivant
        return element

    def __repr__(self):
        return "{}".format(self.premier)

# Autre implémentation des listes chaînées - Récursive

class ListeChainee :

    def __init__(self,premier=None) :
        self.premier = premier
        if self.premier is not None :
            self.s = ListeChainee()

    def est_vide(self):
        return self.premier is None

    def ajouter_debut(self,e):
        if self.premier is None :
            self.premier = e
            self.s = ListeChainee()
        else :
            self.s.ajouter_debut(self.premier)
            self.premier = e

    def ajouter_fin(self,e):
        if self.premier is None :
            self.premier = e
            self.s =ListeChainee()
        
        else :
            self.s.ajouter_fin(e)

    def __repr__(self):
        if self.est_vide()  :
            return " "
        else :
            return "{} -> {}".format(self.premier,self.s)
if __name__=="__main__" :
    L = ListeChainee(3)
    print(L)
    L.ajouter_debut(2)
    print(L)
    L.ajouter_fin(5)
    print(L)
    
# fin du fichier module_liste.py

    
