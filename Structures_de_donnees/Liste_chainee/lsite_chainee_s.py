class Maillon:
    def __init__(self,tete,suivant):
        self.t = tete
        self.s = suivant

    def __repr__(self):
        return afficher(self)

# Fonction auxiliaire

def afficher(lst):
    if lst is None :
        return ""
    else :
        return "{} -> {}".format(lst.t,afficher(lst.s))

class Liste_chainee :

    def __init__(self):
        self.premier = None

    def est_vide(self) :
        return self.premier is None

    def ajouter_debut(self,e):
          self.premier = Maillon(e,self.premier)

    def ajouter_fin(self,e):
        if self.premier is None :
            return Maillon(e,None)
        
        else :
            precedent = self.premier
            courant = self.premier.s
            while courant is not None :
                precedent = courant
                courant = courant.s
        precedent.s = Maillon(e,None)

    def supprimer_debut(self) :
        element = self.premier.t
        self.premier = self.premier.s
        return element

    def __repr__(self):
        return "{}".format(self.premier)
        
