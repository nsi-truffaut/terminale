#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : recherche_min.py
#  auteur : Gregory MAUPU
#    date : 2021/09/28

from random import *

def recherche_min(L):
    if L == []:
        raise Exception('Liste vide')
    mini = L[0]
    for i in range(1,len(L)):
        if L[i] < mini :
            mini = L[i]
    return mini

if __name__=="__main__" :
    #import doctest
    #doctest.testmod(verbose=True)
    L = [ randint(-100,100) for i in range(15)]
    print(L)
    #recherche_min([])
    assert recherche_min(L) == min(L)
