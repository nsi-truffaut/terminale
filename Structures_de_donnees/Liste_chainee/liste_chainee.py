def afficher(lst):
    if lst is None :
        return ""
    else :
        return "{} -> {}".format(lst.t,afficher(lst.s))

def taille(lst):
    """Renvoie la longueur de la liste chaînée"""
    if lst is None :
        return 0
    else :
        return 1 + taille(lst.s)


class Maillon:
    def __init__(self,tete,suivant):
        self.t = tete
        self.s = suivant

    def __repr__(self):
        return afficher(self)

    
def get_maillon(lst,i):
    if lst is None:
            raise IndexError("La liste est vide")
    if i == 0 :
        return lst.t
    else :
        return get_maillon(lst.s,i-1)

class Liste_chainee :

    def __init__(self):
        """ Initialise la liste chaînée par une maillon vide"""
        self.premier = None
       
    def __len__(self):
        """ Renvoie la taille de la liste"""
        return taille(self.premier)
    
    def est_vide(self):
        """ Teste si la liste chaînée est vide"""
        return self.premier is None

    def ajouter_debut(self,e):
        """Ajoute un maillon e en début de liste"""
        self.premier = Maillon(e,self.premier)

    
    def ajouter_fin(self,e) :
        """Ajoute un maillon e en fin de liste"""
        if self.premier is None :
            return Maillon(e,None)
        else:
            precedent = self.premier
            courant = self.premier.s
            while courant != None:
                precedent = courant
                courant = courant.s
            dernier = Maillon(e,None)
            precedent.s = dernier
        

    def inserer(self,e,i):
        """ Insère un maillon en place i de la liste
        """
        j = 0
        precedent = self.premier
        courant = self.premier.s
        while j < i-1 :
            precedent = courant
            courant = courant.s
            j=j+1
                   
        precedent.s = Maillon(e,courant)
            
        
    def supprimer(self,i):
        """ supprime l'élément en place i de la liste
        """
        if self.premier is None :
            raise IndexError("La liste est vide")
        j = 0
        precedent = self.premier
        courant = self.premier.s
        while j < i-1 :
            precedent = courant
            courant = courant.s
            j = j+1
        precedent.s = courant.s
            

    def __getitem__(self,i):
        """ Renvoie l'élément i de la liste
        """
        if self.premier is None or i > len(self) :
            raise IndexError("Index out of range or empty list")
        return get_maillon(self.premier,i)

    def __repr__(self):
            """ Affiche la liste chainée sous la forme a -> b ->
            """
            if self.premier is None :
                return " "
            j = 1
            s = str(get_maillon(self.premier,0))
            while j < len(self) :
                s = s + " -> " + str(self.__getitem__(j))
                j = j+1
            return s+" ->"
            
if __name__=="__main__" :
    l = Liste_chainee()
    print(l)
    l.ajouter_debut(5)
    l.ajouter_debut(6)
    l.ajouter_debut(7)
    l.ajouter_debut(8)
    l.ajouter_fin(12)
    l.supprimer(1)
    l.supprimer(3)
    print(l)
    

