class Maillon:
    def __init__(self,tete,suivant):
        self.t = tete
        self.s = suivant

    def __repr__(self):
        return afficher(self)

# Fonction auxiliaire

def afficher(lst):
    if lst is None :
        return ""
    else :
        return "{} -> {}".format(lst.t,afficher(lst.s))


def ajouter_fin(lst,e) :
    """Ajoute un maillon e en fin de liste"""
    if lst is None :
        return Maillon(e,None)
    
    return Maillon(lst.t,ajouter_fin(lst.s,e))

def ajouter_debut(lst,e):
    """Ajoute un maillon e en début de liste"""
    return Maillon(e,lst)

def inserer(lst,e,i):
    """Insere un maillon e à la position i"""
    if i == 0 :
        return ajouter_debut(lst,e)
    else :
        return Maillon(lst.t,inserer(lst.s,e,i-1))

def supprimer(lst,i):
    if lst is None:
            raise IndexError("La liste est vide")
    if i==0 :
        return lst.s
    else :
        return Maillon(lst.t,supprimer(lst.s,i-1))

def taille(lst):
    """Renvoie la longueur de la liste chaînée"""
    if lst is None :
        return 0
    else :
        return 1 + taille(lst.s)

def get_maillon(lst,i):
    if lst is None:
            raise IndexError("La liste est vide")
    if i == 0 :
        return lst.t
    else :
        return get_maillon(lst.s,i-1)

    
if __name__=='__main__' :
    u = Maillon(1,Maillon(2,Maillon(3,Maillon(4,None))))
    print(u)
    v = ajouter_fin(u,122)
    print(v)
    w = ajouter_debut(u,2000)
    print(w)
    z = inserer(w,1999,1)
    print(z)
    a = supprimer(z,5)
    print(a)
