#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : notation_polonaise.py
#  auteur : Gregory MAUPU
#    date : 2021/09/30

from module_pile import Pile

def npi(s):
    
    temp = ""
    p = Pile()
    for car in s :
        if car not in ['+','*','-','/',' '] :
            temp = temp + car
        else :
             if temp !='' :
                 p.empiler(int(temp))
                 temp = ''
             if car !=' ':
                 
            
                a = p.depiler()
                b = p.depiler()
                if car == "*" :
                    p.empiler(a*b)
                if car == "+":
                    p.empiler(a+b)
                if car == "/" :
                    p.empiler(b/a)
                if car =="-":
                    p.empiler(b-a)
        
    return p.depiler()

print(npi('2 7-5+3-4-11-'))
