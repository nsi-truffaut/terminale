from random import *

# Problème 1

def maximum(n,m):
    return n if n > m else m

# Problème 2

def max_tableau(tab):
    maxi = tab[0]
    for k in range(1,len(tab)):
        if tab[k] > maxi :
            maxi = tab[k]
    return maxi

def indice_max_tableau(tab):
    indice_max = 0
    maxi= tab[indice_max]
    for k in range(1,len(tab)):
        if tab[k] > maxi :
            indice_max = k
            maxi = tab[k]
    return indice_max

def indices_max_tableau(tab):
    indice_max = 0
    liste = []
    maxi=max_tableau(tab)
    for k in range(1,len(tab)):
        if tab[k] == maxi :
           liste.append(k)
    
    return liste

# Problème 3

def moyenne(tab,coeff):
    assert len(coeff) > 0, "La liste des coeffs est vide"
    assert len(tab) == len(coeff), "Problème de taille des listes"
    s = 0
    c = 0
    for k in range(len(tab)):
        s = s + tab[k]*coeff[k]
        c = c + coeff[k]
    return s/c

# Problème 4

def moyenne_bis(tab,debut,fin):
    s = 0
    for k in range(debut,fin+1):
        s = s + tab[k]
    return s / (fin - debut+1)

def moyenne_glissante(tab,k):
    res = []
    for l in range(k//2) :
        res.append(None)
    for j in range(k//2,len(tab)-k//2):
        res.append(moyenne_bis(tab,j-k//2,j+k//2))
    for j in range(len(tab)-k//2,len(tab)):
        res.append(None)
    return res

def moyenne_ter(tab):
    return moyenne_glissante(tab,len(tab))

# Problème 5

def occurences(chaine):
    dico = {}
    for car in chaine :
        if car not in dico.keys():
            dico[car] = 1
        else :
            dico[car] +=1
    return dico

# Problème 6

def binaire(nombre):
    ecrit_binaire = []
    while nombre !=0 :
        ecrit_binaire = [nombre%2]+ ecrit_binaire
        nombre = nombre//2
    return ecrit_binaire

def conversion(nombre,k):
    conv = []
    while nombre !=0 :
        conv = [nombre%k]+ conv
        nombre = nombre//k
    return conv
    
def inv_dico(dico):
    new_dico={}
    for cle,val in dico.items() :
        print(cle,val)
        if val not in new_dico.keys() :
            new_dico[val] = [cle]
        else :
            new_dico[val].append(cle)
    return new_dico










# Problème 10
def accum(s) :
    n= len(s)
    st = ""
    for k in range(n-1):
        st += s[k].upper()+s[k]*k+'-'
    return st+s[n-1].upper()+s[n-1]*(n-1)


def som(tab,i,j):
    s = 0
    for k in range(i,j):
        s = s + tab[k]
    return s

def parts_egale(tab):
    n = len(tab)
    k = 0
    while k < n and som(tab,0,k) != som(tab,k+1,n):
        k = k+1
    if k == n:
        return -1
    else :
        return k

def echanger(tab,i,j):
    c = tab[j]
    tab[j] = tab[i]
    tab[i] = c
    
def tri_impaire(tab):
    n = len(tab)
    for k in range(n) :
        if tab[k] % 2 == 1 :
            i_mini = k
            for j in range(k+1,n) :
                if tab[j]%2 == 1 and tab[j] < tab[i_mini] :
                        i_mini = j
            echanger(tab,i_mini,k)
    return tab
            
    


u = [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
print(u)
tri_impaire(u)

def plier(tab):
    t = tab.copy()
    n = len(t)
    k = n
    while k > 1 :
        u = []
        #print(k,u,t)
        for e in range(k//2):
            u.append(t[e]+t[k-e-1])
        if k%2 == 1 :
            u.append(t[k//2])
        
        k = len(u)
        t = u
        #print(k,u,t)
    return u[0]

def somme_pair_consecutif(tab):
    l = []
    s = 0
    k=0
    n = len(tab)
   
    while k < len(tab)-2:
        s = 0
        j = k
        while j <= len(tab)-2 and tab[j]%2 == tab[j+1]%2 :
            s = s + tab[j]
            j = j+1
        if  j !=k or j==len(tab)-2:
            l.append(s+tab[j])
            k=j+1
        else :
            l.append(tab[k])
            k = k+1
        #print(k,j,l)   
    if k == len(tab)-1 :
        l.append(tab[k])
    return l

def somme(tab):
    k = 0
    t = tab.copy()
    u = []
    while k <= len(t)-2 :
       
        if t[k]%2 == t[k+1]%2 :
            u = somme_pair_consecutif(t)
            t=u
            k = 0
        else :
            k = k+1
        
        #print(k,t)
    return t 
u = [20, 5, 12, 15, 8, 12, 8, 2, 8, 4, 19, 6, 0, 5, 11, 2, 8, 12, 4, 18]
t = [randint(0,20) for k in range(20)]
print(t)
print(somme_pair_consecutif(t))

        
##assert equal_side([1,2,3,4,3,2,1])==3
##assert equal_side([1,100,50,-51,1,1])==1
##assert equal_side([20,10,-80,10,10,15,35])==0
##print(equal_side([14,-3,5,-6,-14,-5,5,-6,0,-3]))
