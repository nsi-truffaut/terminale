#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : module_pile.py
#  auteur : Gregory MAUPU
#    date : 2021/09/30


class Pile:
    
    def __init__(self):
        "Crée une Pile vide"
        self.pile=[]
    
    def est_vide(self):
        "Renvoie si la Pile est vide"
        return self.pile == []
    
    def empiler(self,e):
        self.pile.append(e)
        
    def depiler(self):
        sommet = self.pile[len(self.pile)-1]
        self.pile = self.pile[0:len(self.pile)-1]
        return sommet
    
    def __repr__(self):
        s = " "
        if self.est_vide():
            return s
        else :
            j = 0
            n = len(self.pile)
            while j < len(self.pile):
                s = s + "|" +  str(self.pile[n-j-1]) +"|" + "\n " 
                j = j+1
        return s
    
