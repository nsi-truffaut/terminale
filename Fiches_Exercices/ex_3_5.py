from turtle import *

def carre(n):
    for i in range(4):
        forward(n)
        right(90)

def carre_rempli(n):
        color('black')
        begin_fill()
        carre(n)
        
        end_fill()
        
def matrice_fichier(nom_fichier,n):
        with open(nom_fichier,'r',encoding='utf-8') as fichier :
            l = fichier.readline().strip('\n').split(',')
            k = 1
            
            while l !=[''] :
                
                down()
                for elt in l :
                    
                    if elt == '0':
                        carre(n)
                    else :
                        carre_rempli(n)
                    forward(n)
                l = fichier.readline().strip('\n').split(',')
                up()
                goto(0,-n*k)
                k = k+1

speed(0)   
matrice_fichier('laby.csv',20)

