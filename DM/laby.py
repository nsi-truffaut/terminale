from DM2 import solution
from copy import deepcopy
from turtle import *
lab1 = [[1,1,1,1,1,0,0,0,0,0,1],
        [1,0,0,0,1,0,1,0,1,0,1],
        [1,0,1,0,1,0,1,0,1,0,1],
        [1,0,1,0,1,0,1,0,1,0,1],
        [1,0,1,0,1,0,1,0,1,0,1],
        [2,0,1,0,0,0,1,0,1,0,3],
        [1,1,1,1,1,1,1,0,1,0,1],
        [1,0,0,0,1,0,0,0,1,0,1],
        [1,0,1,0,1,0,1,1,1,0,1],
        [1,0,1,1,1,0,1,0,0,0,1],
        [1,0,0,0,0,0,1,1,0,1,1]]

def carre(n):
    ''' Fonction qui dessine un carré de taille n'''
    for i in range(4):
        forward(n)
        right(90)

def carre_rempli(n,couleur):
    '''Fonction qui dessine un carré noir de taille n'''
    color(couleur)
    begin_fill()
    carre(n)
    end_fill()
    
def matrice(l,n):
    p = len(l)
    for i in range(p) :
        down()
        for j in range(len(l[i])):
            
            if l[i][j] == 0 :
                carre(n)
            elif l[i][j] == 1 :
                carre_rempli(n,"black")
            else :
                carre_rempli(n,"red")
            forward(n)
        up()
        goto(0,-n*(i+1))

def laby_solution(lab):
   
    copy_lab = deepcopy(lab)
    soluce = solution(copy_lab)
    
    for elt in soluce :
        i = elt[0]
        j = elt[1]
        copy_lab[i][j] = 4
    
    return copy_lab

speed(0)

matrice(laby_solution(lab1),10)
