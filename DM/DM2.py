#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : DM2.py
#  auteur : Gregory MAUPU
#    date : 2021/10/29

def chemin(y,x):
    """
    >>> chemin(4,2)
    [(4, 2), (3, 2), (2, 2), (1, 1), (0, 0)]
    >>> chemin(1, 7)
    [(1, 7), (1, 6), (1, 5), (1, 4), (1, 3), (1, 2), (1, 1), (0, 0)]
    >>> chemin(4, 4)
    [(4, 4), (3, 3), (2, 2), (1, 1), (0, 0)]
    """
    l = [(y,x)]
    
    while y != x :
        if y > x :
            y = y-1
        else :
            x =x -1
        l.append((y,x))
    while x !=0 and y !=0:
        x = x-1
        y = y-1
        l.append((y,x))
    return l


# Correction du DM - Exercice 2

# 1) lab2[1][0] = 2

lab1 = [[1,1,1,1,1,0,0,0,0,0,1],
        [1,0,0,0,1,0,1,0,1,0,1],
        [1,0,1,0,1,0,1,0,1,0,1],
        [1,0,1,0,1,0,1,0,1,0,1],
        [1,0,1,0,1,0,1,0,1,0,1],
        [2,0,1,0,0,0,1,0,1,0,3],
        [1,1,1,1,1,1,1,0,1,0,1],
        [1,0,0,0,1,0,0,0,1,0,1],
        [1,0,1,0,1,0,1,1,1,0,1],
        [1,0,1,1,1,0,1,0,0,0,1],
        [1,0,0,0,0,0,1,1,0,1,1]]

lab2 = [[1,1,1,1,1,1,1],
        [2,0,0,0,0,0,1],
        [1,1,1,1,1,0,1],
        [1,0,1,0,0,0,1],
        [1,0,1,0,1,0,1],
        [1,0,0,0,1,0,1],
        [1,1,1,1,1,3,1]]



def est_valide(i,j,n,m):
    """
    >>> est_valide(5, 2, 10, 10)
    True
    >>> est_valide(-3, 4, 10, 10)
    False
    """
    return (i >= 0 and i< n) and (j>=0 and j<n)


def depart(lab) :
    """
    
    >>> depart(lab2)
    (1, 0)
    """
    n = len(lab)
    m = len(lab[0])
    i = 0
    j = 0
    while i < n and lab[i][j] != 2:
        while j < m and lab[i][j] != 2 :
            
            j = j +1
            
        i = i+1
        j = 0
       
    return (i,j)
            


def nb_cases_vides(lab):
    """
    >>> nb_cases_vides(lab1)
    58
    """
    n = len(lab)
    m = len(lab[0])
    c = 0
    for i in range(n) :
        for j in range(m) :
            if lab[i][j] !=1 :
                c = c +1
    return c

### Programme non demandé dans le devoir
### la réponse à la question est [(1,1), (2,2)]

def voisines(i,j,lab):
    """ Fonction qui renvoie les cases voisines d'une case donné qui ne
        sont pas des murs

    >>> voisines(1,2,[[1, 1, 4], [0, 0, 0], [1, 1, 0]])
    [(2, 2), (1, 1)]
    """
    liste = []
    n = len(lab)
    if (i >0 and i < n-1) and (j>0 and j < n-1) :
        if lab[i-1][j] == 0 or lab[i-1][j] == 3:
            liste.append((i-1,j))
        if lab[i+1][j] == 0 or lab[i+1][j] == 3:
            liste.append((i+1,j))
        if lab[i][j+1] == 0 or lab[i][j+1] == 3:
            liste.append((i,j+1))
        if lab[i][j-1] == 0 or lab[i][j-1] == 3:
            liste.append((i,j-1))
    elif (i == 0) and (j> 0 and j < n-1):
        if lab[i][j-1] == 0 or lab[i][j-1] == 3 :
            liste.append((0,j-1))
        if lab[i][j+1] == 0 or lab[i][j+1] == 3 :
            liste.append((0,j+1))
        if lab[i+1][j] == 0 or lab[i+1][j] == 3 :
            liste.append((i+1,j))
    elif i == n-1 and (j> 0 and j < n-1):
        if lab[i][j-1] == 0 or lab[i][j-1] == 3 :
            liste.append((i,j-1))
        if lab[i][j+1] == 0 or lab[i][j+1] == 3 :
            liste.append((i,j+1))
        if lab[i-1][j] == 0 or lab[i-1][j] == 3 :
            liste.append((i-1,j))
    elif (i> 0 and i < n-1) and (j == 0 or j == n-1):
        if lab[i-1][j] == 0 or lab[i-1][j] == 3:
            liste.append((i-1,j))
        if lab[i+1][j] == 0 or lab[i+1][j] == 3:
            liste.append((i+1,j))
        if j == n-1 and (lab[i][j-1] == 0 or lab[i][j-1] == 3) :
            liste.append((i,j-1))
        if j == 0 and (lab[i][j+1] == 0 or lab[i][j+1] == 3) :
            liste.append((i,j+1))
    elif i==0 and j == 0:
        if lab[i+1][j] == 0 or lab[i+1][j] == 3:
            liste.append((i+1,j))
        if lab[i][j+1] == 0 or lab[i][j+1] == 3:
            liste.append((i,j+1))
    elif i==0 and j == n-1:
        if lab[i+1][j] == 0 or lab[i+1][j] == 3:
            liste.append((i+1,j))
        if lab[i][j-1] == 0 or lab[i][j-1] == 3:
            liste.append((i,j-1))
    elif i==n-1 and j == n-1:
        if lab[i-1][j] == 0 or lab[i-1][j] == 3:
            liste.append((i-1,j))
        if lab[i][j-1] == 0 or lab[i][j-1] == 3:
            liste.append((i,j-1))
    elif i==n-1 and j == 0:
        if lab[i-1][j] == 0 or lab[i-1][j] == 3:
            liste.append((i-1,j))
        if lab[i][j+1] == 0 or lab[i][j+1] == 3 :
            liste.append((i,j+1))
    return liste

from copy import deepcopy

def solution(lab):
    """
    >>> solution(lab2)
    [(1, 0), (1, 1), (1, 2), (1, 3), (1, 4), (1, 5), (2, 5), (3, 5), (4, 5), (5, 5), (6, 5)]
    """
    laby = deepcopy(lab)
    chemin=[depart(lab)]
    case = chemin[0]
    i = case[0]
    j = case[1]
    
    while laby[i][j] != 3 :
        
        laby[i][j] = 4
        
        voisinage = voisines(i,j,laby)
        
        if voisinage != [] :
            case = voisinage[0]
            i = case[0]
            j = case[1]
            chemin.append(case)
        else :
            chemin.pop()
            case = chemin[len(chemin)-1]
            i = case[0]
            j = case[1]
        
    return chemin

if __name__=='__main__' :
    import doctest
    doctest.testmod(verbose=True)
