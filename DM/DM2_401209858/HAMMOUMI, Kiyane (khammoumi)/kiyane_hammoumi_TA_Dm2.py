#/usr/bin/env python3
#-*- coding: utf-8 -*-

#fichier: kiyane_hammoumi_TA_Dm2
#auteur: Kiyane HAMMOUMI
#date: 04/11/2021
#

from turtle import*
from copy import deepcopy
 
#Exo1

def chemin(y,x):
    assert y>=0, "y doit être supérieur ou égal à 0"
    assert x>=0, "x doit être supérieur ou égal à 0"
    way=[]
    while y<x:
        way.append((y,x))
        x=x-1
    while y>x:
        way.append((y,x))
        y=y-1
    while 0<x:
        way.append((y,x))
        x=x-1
        y=y-1
    way.append((0,0))
    return way

#Exo2
#PartiA

#1) lab2[1][0]=2 (il est reporté lus bas dans les test

#2)
def est_valide(i,j,m,n):
    return 0<=i and i<n and 0<=j and j<m

#3)
def depart(lab):
    n=len(lab)
    m=len(lab[0])
    i=0
    start=-1
    while i<n and start==-1:
        if lab[i][0]==2:
            start=2
        i=i+1
    return (i-1,0)

#4)
def nb_case_vide(lab):
    Cv=2
    for n in range(len(lab)):
        for m in range(len(lab[n])):
            if lab[n][m]==0:
                Cv=Cv+1
    return Cv

L=[[1,1,1,1,1,1,1],[2,0,0,0,0,0,1],[1,1,1,1,1,0,1],[1,0,1,0,0,0,1],[1,0,1,0,1,0,1],[1,0,0,0,1,0,1,],[1,1,1,1,1,3,1]]

#PartiB

def voisines(i,j,lab):  #fonction voisines que j'ai fait de mes mains, avant d'avoir été sur l'espace de travail
    voisin=[]
    k=i-1
    n=len(lab)
    m=len(lab[0])
    if k>=0:
        if lab[k][j]==0 or lab[k][j]==3:
            voisin.append((k,j))
    k=i+1
    if k<n:
        if lab[k][j]==0 or lab[k][j]==3:
            voisin.append((k,j))
    k=j-1
    if k>=0:
        if lab[i][k]==0 or lab[i][k]==3:
            voisin.append((i,k))
    k=j+1
    if k<m:
        if lab[i][k]==0 or lab[i][k]==3:
            voisin.append((i,k))
    return voisin

#1)
#    0 1 2
#    - - - 
#0 | 1|1|4
#1 | 0|0|0 <- coordonnée (1,2)
#2 | 1|1|0
#
# la coordonnée (1,2) a pour voisin [(1,1),(2,2)]

#2)
#a-
# chemin = [(1,0)]
# chemin.append((1,1))
# chemin.append((2,1))
# chemin.pop()
# chemin.append((1,2))
# chemin.append((1,3))
# chemin.append((2,3))
# chemin.append((3,3))
# chemin.append((3,4))
# chemin.pop()
# chemin.pop()
# chemin.pop()
# chemin.append((2,4))
# chemin.append((2,5))

#b-
def solution(lab):
    way=[depart(lab)]
    case=way[0]
    i=case[0]
    j=case[1]
    while not(lab[i][j]==3):
        voisin=voisines(i,j,lab)
        if voisin==[]:
            lab[i][j]=4
            way.pop()
            m=len(way)-1
            case=way[m]
        else:
            lab[i][j]=4
            case=voisin[0]
            way.append(case)
        i=case[0]
        j=case[1]
    return way

#partiC

#fonction matrice modifiée

speed(0)

def carre(n):
    for i in range(4):
        forward(n)
        right(90)

def carre_noir(n):
    color("black")
    begin_fill()
    carre(n)
    end_fill()
    
def carre_rouge(n):
    color("red")
    begin_fill()
    carre(n)
    end_fill()

def matrice(l,n):
    for liste in l:
        for nombre in liste:
            if nombre==1:
                carre_noir(n)
            elif nombre==5:
                carre_rouge(n)
            else:
                carre(n)
            forward(n)
        penup()
        backward(n*len(liste))
        right(90)
        forward(n)
        left(90)
        pendown()

#a) en fichier externe nommé lab.csv

#b)
def laby(fichier):
    with open(fichier,"r") as lab:
        maze=[]
        ligne=lab.readline().strip().split(',')
        while ligne!=['']:
            maze.append(ligne)
            ligne=lab.readline().strip().split(',')
        for i in range(len(maze)):
            for j in range(len(maze[i])):
                maze[i][j]=int(maze[i][j])
        return maze

#c)
def affiche_lab(fichier,taille):
    lab=laby(fichier)
    maze = deepcopy(lab)
    way=solution(maze)
    for element in way:
        maze[element[0]][element[1]]=5
    matrice(maze,taille)

#test
lab2=[[1,1,1,1,1,1,1],[1,0,0,0,0,0,1],[1,1,1,1,1,0,1],[1,0,1,0,0,0,1],[1,0,1,0,1,0,1],[1,0,0,0,1,0,1,],[1,1,1,1,1,3,1]]
lab2[1][0]=2
print(lab2)
print(chemin(2,3),est_valide(6,8,10,10),est_valide(6,8,1,10),est_valide(6,8,10,1))
print(depart(lab2),nb_case_vide(lab2))
print(solution(lab2))
print(laby('lab.csv'))
affiche_lab('lab.csv',20)
#penup()
#goto(-400,250)
#affiche_lab('lab2.csv',20)  