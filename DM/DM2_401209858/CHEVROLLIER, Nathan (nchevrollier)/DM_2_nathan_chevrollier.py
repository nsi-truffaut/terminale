## !/usr/bin/env Python 3
## -*- coding: utf-8 -*-
##
## Nom du ficher: DM_2_nathan_chevrollier.py
##        Auteur: Chevrollier Nathan
##          Date: 06/11/2021




##Exercice 1

def chemin(x, y):
    """
    fonction qui renvoie une liste de couple de coordonnées pour rejoindre depuis cette position
    le coin en haut a gauche du tableau,en rejoingnant d'abord la diagonale en se rapprochant de
    l'arrivée,puis en suivant cette diagonale jusqu'au coin en question

    >>> chemin(1, 7)
    [(1, 7), (1, 6), (1, 5), (1, 4), (1, 3), (1, 2), (1, 1), (0, 0)]
    >>> chemin(4, 4)
    [(4, 4), (3, 3), (2, 2), (1, 1), (0, 0)]
    >>> chemin(4, 2)
    [(4, 2), (3, 2), (2, 2), (1, 1), (0, 0)]
    """
    
    way=[(x,y)]
    avance=(x,y)
    while x != 0 and y != 0:
        if x<y:
            while x != y :
                y=y-1
                avance=(x,y)
                way.append(avance)
    
        elif y<x:
            while x != y:
                x=x-1
                avance=(x,y)
                way.append(avance)
            
        x=x-1
        y=y-1
        avance=(x,y)
        way.append(avance)
            
    
    return way
            
##Exercice 2
##Partie A
#1)

"lab2[1][0]=2"

#2)

def est_valide(i,j,n,m):
    """
    fonction qui renvoie True si le couple (i,j)correspond à des coordonnées
    valides pour un labyrinthe de taille (n,m) et False sinon.
    
    >>> est_valide(5, 2, 10, 10)
    True
    >>> est_valide(-3, 4, 10, 10)
    False
    """
    resultat=False
    if i <= n and i >= 0 and j <= m and j >= 0:
        resultat=True
    
    return resultat
    
#3)

def depart(lab):
    """
    fonction qui renvoie les coordonnées du départ du labyrinthe
    >>> depart([[1, 1, 1, 1, 1, 1, 1],[2, 0, 0, 0, 0, 0, 1],[1, 1, 1, 1, 1, 0, 1],[1, 0, 1, 0, 0, 0, 1],[1, 0, 1, 0, 1, 0, 1],[1, 0, 0, 0, 1, 0, 1],[1, 1, 1, 1, 1, 3, 1]])
    (1, 0)
    """
    n = len(lab)
    m = len(lab[0])
    for y in range (n):
        for x in range (m):
            if lab[y][x] == 2:
                return y,x

#4)

def nb_cases_vides(lab):
    """
    fonction qui renvoie le nombre de cases vides d'un labyrinthe (passage)
    >>> nb_cases_vides([[1, 1, 1, 1, 1, 1, 1],[2, 0, 0, 0, 0, 0, 1],[1, 1, 1, 1, 1, 0, 1],[1, 0, 1, 0, 0, 0, 1],[1, 0, 1, 0, 1, 0, 1],[1, 0, 0, 0, 1, 0, 1],[1, 1, 1, 1, 1, 3, 1]])
    19
    """
    n = len(lab)
    m = len(lab[0])
    resultat=0
    for y in range (n):
        for x in range (m):
            if lab[y][x] == 0 or lab[y][x] == 2 or lab[y][x] == 3:
                resultat=resultat+1
    return resultat


##Partie B
#1)

"cela renvoie [(1, 1), (2, 2)]"

#2)
#a}

#     >>> lab3=[[1, 1, 1, 1, 1, 1],[2, 0, 0, 0, 0, 3],[1, 0, 1, 0, 1, 1],[1, 1, 1, 0, 0, 1]]
#     >>> chemin = [(1, 0)]
#     >>> chemin.append((1,1))
#     >>> chemin.append((2,1))
#     >>> chemin.pop()
#     >>> chemin.append((1,2))
#     >>> chemin.append((1,3))
#     >>> chemin.append((2,3))

#         >>> chemin.append((3,3))
#         >>> chemin.append((3,4))
#         >>> chemin.pop()
#         >>> chemin.pop()
#         >>> chemin.append((2,4))
#         >>> chemin.append((2,5))
#         >>> chemin

#b}

def solution(lab):
    """
    fonction qui renvoie le chemin du labyrinthe représenté par le paramètre lab
    """
    
    chemin = [depart(lab)]
    case = chemin[0]
    i = case[0]
    j = case[1]
    n_voisin=0
    while lab[j][i] != 3:
        
        next=voisines(i,j,lab)
        chemin.append(next)
        if next[n_voisin] == []:
            while chemin[len(chemin)] == next[n_voisin]:
                chemin.pop()
            n_voisin = n_voisin + 1
        else:
            i=next[n_voisin][0]
            j=next[n_voisin][1]
    return chemin
    
    



if __name__=='__main__' :
    import doctest
    doctest.testmod(verbose=True)