##!/usr/bin/env Python 3
## -*- coding: utf-8 -*-
##
##Nom du fichier : programme_dm_sebastien_porthault.py
##        Auteur : PORTHAULT Sébastien
##          Date : 7/11/2021

##EXERCICE 1 :

def chemin(y,x):
    """Renvoie une liste de couples de coordonnées pour joindre depuis cette position le coin en haut à gauche du tableau ( arrivée ), en rejoignant d'abord la diagonale en se rapprochant de l'arrivée, puis en suivant cette diagonale jusqu'au coin en question
    >>> chemin(4, 2)
    [(4, 2), (3, 2), (2, 2), (1, 1), (0, 0)]
    >>> chemin(1, 7)
    [(1, 7), (1, 6), (1, 5), (1, 4), (1, 3), (1, 2), (1, 1), (0, 0)]
    >>> chemin(4, 4)
    [(4, 4), (3, 3), (2, 2), (1, 1), (0, 0)]
    """
    resultat=[(y,x)]
    plus=(y,x)
    while y!=0:
        if y>x:
            while y!=x:
                y=y-1
                plus=(y,x)
                resultat.append(plus)
        else:
            while x!=y:
                x=x-1
                avance=(y,x)
                resultat.append(plus)
        y=y-1
        x=x-1
        plus=(y,x)
        resultat.append(plus)
    return resultat

##EXERCICE 2 :

##PARTIE A :

##--------------------------------------------------------1)

##L'instruction permettant de placer le pion lab2 sur le point de départ serait ">>> lab2[1][0]=2".

##--------------------------------------------------------2)

def est_valide(i,j,n,m):
    """Renvoie True si le couple ( i, j ) correspond à des coordonnées valides pour un labyrinthe de taille ( n, m ) et False sinon.
    >>> est_valide(5, 2, 10, 10)
    True
    >>> est_valide(-3, 4, 10, 10)
    False
    """
    if i<=n and i>=0 and j<=m and j>=0:
        print("True")
    else:
        print("False")

##--------------------------------------------------------3)

def depart(lab):
    """Renvoie les coordonnées du départ du labyrinthe.
    """
    n = len(lab)
    m = len(lab[0])
    for y in range(n):
        for x in range(m):
            if lab[y][x]==2:
                return y,x

##--------------------------------------------------------4)

def nb_cases_vides(lab):
    """Renvoie le nombre de cases vides d'un labyrinthe ( passage ).
    >>> nb_cases_vides([[1, 1, 1, 1, 1, 1, 1],[2, 0, 0, 0, 0, 0, 1],[1, 1, 1, 1, 1, 0, 1],[1, 0, 1, 0, 0, 0, 1],[1, 0, 1, 0, 1, 0, 1],[1, 0, 0, 0, 1, 0, 1],[1, 1, 1, 1, 1, 3, 1]])
    19
    """
    a = len(lab)
    b = len(lab[0])
    resultat=0
    for y in range(a):
        for x in range(b):
            if lab[y][x]==0 or lab[y][x]==2 or lab[y][x]==3:
                resultat=resultat+1
    return resultat

##PARTIE B :

##--------------------------------------------------------1)

##L'appel voisines(1, 2, [[1, 1, 4], [0, 0, 0], [1, 1, 0]]) renvoie [(1, 1), (2, 2)].

##------------------------------------------------------2)a)

##La suite d'instructions est :

##    >>> lab3=[[1, 1, 1, 1, 1, 1],[2, 0, 0, 0, 0, 3],[1, 0, 1, 0, 1, 1],[1, 1, 1, 0, 0, 1]]
##    >>> chemin = [(1, 0)]
##    >>> chemin.append((1,1))
##    >>> chemin.append((2,1))
##    >>> chemin.pop()
##    >>> chemin.append((1,2))
##    >>> chemin.append((1,3))
##    >>> chemin.append((2,3))
##    >>> chemin.append((3,3))
##    >>> chemin.append((3,4))
##    >>> chemin.pop()
##    >>> chemin.pop()
##    >>> chemin.append((2,4))
##    >>> chemin.append((2,5))

##------------------------------------------------------2)b)

def solution(lab):
    """Renvoie le chemin du labyrinthe représenté par le paramètre lab.
    >>> solution([[1, 1, 1, 1, 1, 1, 1],[2, 0, 0, 0, 0, 0, 1],[1, 1, 1, 1, 1, 0, 1],[1, 0, 1, 0, 0, 0, 1],[1, 0, 1, 0, 1, 0, 1],[1, 0, 0, 0, 1, 0, 1],[1, 1, 1, 1, 1, 3, 1]])
    [(1, 0), (1,1), (1, 2), (1, 3),(1, 4), (1, 5), (2, 5), (3, 5), (4, 5), (5, 5), (6, 5)]
    """
    chemin=[depart(lab)]
    case=chemin[0]
    i=case[0]
    j=case[1]
    nombre_voisines=0
    resultat=[]
    while lab[i][j]!=3:
        a=voisines(i,j,lab)
        chemin.append(a)
        if a[nombre_voisines]==[]:
            while chemin[len(chemin)]==a[nombre_voisines]:
                chemin.pop()
            nombre_voisines=nombre_voisines+1
        else:
            i=a[nombre_voisines][0]
            j=a[nombre_voisines][1]
            resultat.append(a)
    return resultat

##PARTIE C :

##--------------------------------------------------------b)

##def laby(fichier):
    """Convertit le ficher labyrinthe.csv en une liste de liste.
    """

if __name__=="__main__":
    import doctest
    doctest.testmod(verbose=True)

##Fin du fichier programme_dm_sebastien_porthault.py
