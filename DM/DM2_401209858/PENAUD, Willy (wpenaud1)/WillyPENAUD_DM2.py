#!/usr/bin/env python3
# -*- coding : utf-8 -*-

#fichier: WillyPENAUD_DM.py
# Auteur: Willy Penaud
#   Date: 2021/11/07

##Exercice 1
def chemin(x,y):
    """
        renvoie le chemin de coordonées x,y vers 0,0 le coins en haut à gauche
    """
    res = []
    co = (x,y)
    res.append(co)
    while co[0] != co[1]:
        if co[0] < co[1] :
            co = (x,co[1]-1)
        if co[0] > co[1] :
            co = (co[0]-1,y)
        res.append(co)
    for i in range(co[0]):
        co = (co[0]-1,co[1]-1)
        res.append(co)
    return res
##Exercice 2
#2.
def est_vide(i,j,n,m):
    """
        revoie si le coordonées i,j son dans le labyrinthe composés de n lignes et m colonnes 
    """
    if i < 0 or j < 0 :
        res = False
    elif i > n or j > m :
        res = False
    else :
        res = True
    return res
#3.
def depart(lab):
    """
        renvoie les coordonées du point de départ du labyrinthe
    """
    n = len(lab)
    m = len(lab[0])
    for i in range(n):
        for j in range(m):
            if lab[i][j] == 2 :
                res = (i,j)
    return res
#4.    
def nb_cases_vides(lab):
    """
        renvoie le nombre de case qui ne son pas des murs dans le labyrinthe
    """
    res = 0
    n = len(lab)
    m = len(lab[0])
    for i in range(n):
        for j in range(m):
            if lab[i][j] != 1:
                res = res + 1
    return res
                
#Fin du fichier WillyPENAUD_DM.py
