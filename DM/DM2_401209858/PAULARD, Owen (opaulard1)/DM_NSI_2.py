#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# Fichier : DM_NSI_2.py
#  Auteur : Owen PAULARD
#    Date : 30/10/2021

#from voisines import *

#----------# Début du Fichier DM_NSI_2.py #----------#

###EX 1 :
def chemin(y, x):
    """Fonction qui renvoie une liste de couples de coordonnées correspondant au chemin emprunté pour rejoindre la position (0, 0) du tableau depuis la psition (x, y)"""
    route = [(y, x)]
    while y != 0 and x != 0 :
        if x > y :
            x = x-1
            route.append((x, y))
        elif x == y :
            x = x-1
            y = y-1
            route.append((x, y))
        elif x < y :
            y = y-1
            route.append((x, y))
    return route


###EX 2
##Partie A
#1)
lab2 = [[1, 1, 1, 1, 1, 1, 1],
	[1, 0, 0, 0, 0, 0, 1],
	[1, 1, 1, 1, 1, 0, 1],
	[1, 0, 1, 0, 0, 0, 1],
	[1, 0, 1, 0, 1, 0, 1],
	[1, 0, 0, 0, 1, 0, 1],
	[1, 1, 1, 1, 1, 3, 1]]

def placer_depart(lab, y, x):
    """Fonction qui place le départ d'un labyrinthe aux coordonnées (y, x)"""
    tempo = []
    tempo2 = []
    for n in range(len(lab[y])):
        tempo.append(lab[y].pop())
    for i in range(len(tempo)):
        if i == x :
            for j in range(len(tempo)-x) :
                tempo2.append(tempo.pop())
            lab[y].append(2)
            for k in range(1, len(tempo2)):
                lab[y].append(tempo2[k])

    return lab
            

#2)
def est_valide(i, j, n, m):
    """Fonction qui renvoie si les coordonnées (i, j) sont des coordonnées valide pour un labyrinthe de taille (n, m)"""
    valide = True
    if i > n or j > m:
        valide = False
    if i <= 0 or j <= 0:
        valide = False
    return valide

#3)
def depart(lab):
    """Fonction qui renvoie les coordonnées du départ du labyrinthe"""
    ligne = 0
    x = 0
    y = 0
    for i in range(len(lab)):
        for j in range(len(lab[i])):
            if lab[i][j] == 2 :
                x = j
                y = i
                return (y, x)
            
#4)
def nb_cases_vides(lab):
    """"""
    vides = 0
    for i in range(len(lab)):
        for j in range(len(lab[i])):
            if lab[i][j] == 0 or lab[i][j] == 2 or lab[i][j] == 3 :
                vides = vides + 1
    return vides
                

##Partie B
#1) voisines(1, 2, [[1,1,4],[0,0,0],[1,1,0]]) renvoie la liste [(1, 1), (2, 2)]
       
#2a)
lab3 = [[1,1,1,1,1,1],
        [2,0,0,0,0,3],
        [1,0,1,0,1,1],
        [1,1,1,0,0,1]]

# entrée: (1, 0), sortie: (1, 5)
##chemin = [(1, 0)]
##chemin.append((1, 1))
##chemin.append((2, 1))
##chemin.pop()
##chemin.append((1, 2))
##chemin.append((1, 3))
##chemin.append((2, 3))
###SUITE
##chemin.append((3, 3))
##chemin.append((3, 4))
##chemin.pop()
##chemin.pop()
##chemin.pop()
##chemin.append((1, 4))
##chemin.append((1, 5))

#2b)
def solution(lab):
    chemin = [depart(lab)]
    case = chemin[0]
    i = case[0]
    j = case[1]
    while lab[i][j] != 3:
        voisine_1 = voisines(i,j,lab)[0]
        case = (voisine_1[0],voisine_1[1])
        chemin.append(case)
        i = case[0]
        j = case[1]
        if lab[i][j] == 0 :
            lab[i][j] = 4

    return chemin
                








#----------# Fin du Fichier DM_NSI_2.py #----------#
