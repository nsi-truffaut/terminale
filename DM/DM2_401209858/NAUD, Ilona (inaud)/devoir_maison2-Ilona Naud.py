
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : devoir_maison2
#  auteur :Ilona NAUD
#    date : 2021/11/08


def chemin(y,x):
    """fonction qui renvoie le chemin a faire pour arriver a la sortie"""
    a=(0,0)
    print(y,x)
    while y > a[0]:
        if y >1:
            y = y-1
            print(y,x)
        while x > a[1]:
            if x == y:
                x= x-1
                y= y-1
                print(y,x)
            if x > y:
                x = x-1
                print(y,x)


def est_valide(i,j,n,m):
    """fonction qui renvoie si le couple correspond à des coordonées valides"""
    if 0<i<n and 0<i<m and 0<j<n and 0<j<m:
        
        return True
    else:
        return False

  
lab2 = [[1,1,1,1,1,1,1],[2,0,0,0,0,0,1],[1,1,1,1,1,0,1],[1,0,1,0,0,0,1],[1,0,1,0,1,0,1],[1,0,0,0,1,0,1],[1,1,1,1,1,3,1]]
def depart(lab):
    """fonction qui renvoie l'emplacement du depart"""
    n=len(lab)
    m=len(lab[0])
    i=0
    start=-1
    while i<n and start==-1:
        if lab[i][0]==2:
            start=2
        i=i+1
    return(i-1,0)

def nb_case_vide(lab):
    """fonction qui renvoie le nombre de case ou il n'y a pas de mur dans le labyrinthe"""
    r= 0
    for i in lab:
        for elem in i:
            if elem != 1:
                r=r+1
           
    return r
##je n'est pas réussi mais je vous montre ce que j'ai fait
def solution(lab):
    """fonction qui renvoie le chemin de la solution du labyrinthe"""
    chemin = [depart(lab)]
    case = chemin[0]
    i= case[0]
    j=case[1]
    v=[]
    for k in range(len(lab)):
        if j!= 1:
            i=i+1
            j=j+1
            v.append((i,j))
            return i,j
    return v

##    
