#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : HERMIER_dm2.py
#  auteur : HERMIER Maxime
#    date : 27/10/2021

#EXERCICE 1

def chemin(y,x):
    print(y,x)
    if y>x:
        while y!=x:
            y=y-1
            print(y,x)
        while x!=0:
            x=x-1
            y=y-1
            print(y,x)
    elif x>y:
        while x!=y:
            x=x-1
            print(y,x)
        while x!=0:
            x=x-1
            y=y-1
            print(y,x)
    elif x==y:
        while x!=0:
            x=x-1
            y=y-1
            print(y,x)
    return y,x

#EXERCICE 2, PARTIE A, 2)

def est_valide(i,j,n,m):
    if 0<i<n and 0<i<m and 0<j<n and 0<j<m:
        return True
    else :
        return False

lab1=[[1,1,1,1,1,0,0,0,0,0,1],
      [1,0,0,0,1,0,1,0,1,0,1],
      [1,0,1,0,1,0,1,0,1,0,1],
      [1,0,1,0,1,0,1,0,1,0,1],
      [2,0,1,0,1,0,1,0,1,0,1],
      [1,0,1,0,0,0,1,0,1,0,3],
      [1,1,1,1,1,1,1,0,1,0,1],
      [1,0,0,0,1,0,0,0,1,0,1],
      [1,0,1,0,1,0,1,1,1,0,1],
      [1,0,1,1,1,0,1,0,0,0,1],
      [1,0,0,0,0,0,1,1,0,1,1]]

#EXERCICE 2, PARTIE A, 3)

#Si la fonction depart retourne quelque chose d'autre que (-1,-1) alors le labyrinthe a une entrée.
def depart(lab):
    n=len(lab)
    m=len(lab[0])
    for i in range(n):
        for j in range(m):
            x=i
            y=j
            if lab[x][y]==2:
                return (x,y)
    return(-1,-1)

lab2=[[1,1,1,1,1,1,1],
      [2,0,0,0,0,0,1],
      [1,1,1,1,1,0,1],
      [1,0,1,0,0,0,1],
      [1,0,1,0,1,0,1],
      [1,0,0,0,1,0,1],
      [1,1,1,1,1,3,1]]

#EXERCICE 2, PARTIE A, 4)

def nb_cases_vides(lab):
    n=len(lab)
    m=len(lab[0])
    compteur=0
    for i in range(n):
        for j in range(m):
            x=i
            y=j
            if lab[x][y]==0 or lab[x][y]==2 or lab[x][y]==3:
                compteur=compteur+1
    return compteur

#fin du fichier