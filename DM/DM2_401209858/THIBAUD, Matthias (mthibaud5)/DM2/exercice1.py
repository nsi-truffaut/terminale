# !/usr/bin/env python3
#-*- coding: utf-8 -*-

#
# Fichier : exercice1.py
#  Auteur : THIBAUD Matthias
#    Date : 08/11/2021
#


# Début du fichier exercice1.py

def chemin(y,x):
    """
    Renvoie une liste de couple de coordonées pour rejoindre depuis cette position
    le coin en haut à gauche du tableau(arrivé), en rejoignant d'abord la diagonale en se
    rapprochant de l'arrivé, puis en suivant cette diagonale jusqu'au coin en question.
    >>> chemin(4,2)
    [(4, 2), (3, 2), (2, 2), (1, 1), (0, 0)]
    >>> chemin(1,7)
    [(1, 7), (1, 6), (1, 5), (1, 4), (1, 3), (1, 2), (1, 1), (0, 0)]
    >>> chemin(4,4)
    [(4, 4), (3, 3), (2, 2), (1, 1), (0, 0)]
    """
    tab = [(y,x)]
    while not(x == 0 and y == 0) :
        if x == y :
            x = x - 1
            y = y - 1
            tab.append((y,x))
        elif x < y :
            y = y - 1
            tab.append((y,x))
        else:
            x = x - 1
            tab.append((y,x))
    return tab

print(chemin(4,2))
print(chemin(1,7))
print(chemin(4,4))

# Fin du fichier exercice1.py
