# !/usr/bin/env python3
#-*- coding: utf-8 -*-

#
# Fichier : exercie2.py
#  Auteur : THIBAUD Matthias
#    Date : 08/11/2021
#

# Début du fichier exercie2.py

# Importations

from voisines import voisines
from copy import deepcopy
from matrice import *


# Définitions des labyrinthes

lab1 = [[1,1,1,1,1,0,0,0,0,0,1],
        [1,0,0,0,1,0,1,0,1,0,1],
        [1,0,1,0,1,0,1,0,1,0,1],
        [1,0,1,0,1,0,1,0,1,0,1],
        [1,0,1,0,1,0,1,0,1,0,1],
        [2,0,1,0,0,0,1,0,1,0,3],
        [1,1,1,1,1,1,1,0,1,0,1],
        [1,0,0,0,1,0,0,0,1,0,1],
        [1,0,1,0,1,0,1,1,1,0,1],
        [1,0,1,1,1,0,1,0,0,0,1],
        [1,0,0,0,0,0,1,1,0,1,1]]

lab2 = [[1,1,1,1,1,1,1],
        [1,0,0,0,0,0,1],
        [1,1,1,1,1,0,1],
        [1,0,1,0,0,0,1],
        [1,0,1,0,1,0,1],
        [1,0,0,0,1,0,1],
        [1,1,1,1,1,3,1]]

lab3 = [[1,1,1,1,1,1],
        [2,0,0,0,0,3],
        [1,0,1,0,1,1],
        [1,1,1,0,0,1]]


# PARTIE A
#1
lab2[1][0] = 2

#2
def est_valide(i,j,n,m):
    """
    Renvoie si le couple (i,j) correspond à des coordonnées valides pour un labyrithe de taille (n,m).
    >>> est_valide(5,2,10,10)
    True
    >>> est_valide(-3,4,10,10)
    False
    """
    return type(i) == int and i>=0 and j>=0 and type(j) == int and i <= n and i <=m and j <= n and j <=m and type(n) == int and type(m) == int and n > 0  and m > 0

#3
def depart(lab):
    """
    Renvoie la position du départ dans un labyrinthe, celui-ci doit en comporter un.
    >>> depart(lab2)
    (1, 0)
    """
    n = len(lab)
    m = len(lab[0])
    for i in range(n):
        for j in range(m):
            if lab[i][j] == 2:
                coordonnees = (i,j)
    return coordonnees

#4
def nb_cases_vides(lab):
    """
    Renvoie le nombre de cases vides dans un labyrinthe (y compris le départ et l'arrivée).
    >>> nb_case_vides(lab2)
    19
    """
    compteur = 0
    n = len(lab)
    m = len(lab[0])
    for i in range(n):
        for j in range(m):
            if lab[i][j] == 2 or lab[i][j]== 0 or lab[i][j] == 3:
                compteur = compteur + 1
    return compteur

# PARTIE B
#1
#L'appel de voisines(1, 2, [[1,1,4],[0,0,0],[1,1,0]] renvoie [(1,1),(2,2)].

#2a
# entrée: (1, 0), sortie(1, 5)
chemin = [(1, 0)]
chemin.append((1,1))
chemin.append((2,1))
chemin.pop()
chemin.append((1, 2))
chemin.append((1, 3))
chemin.append((2, 3))
#la suite
chemin.append((3, 3))
chemin.append((3, 4))
chemin.pop()
chemin.pop()
chemin.pop()
chemin.append((1, 4))
chemin.append((1, 5))

#2b
def solution(lab):
    """
    Renvoie le chemin pour aller à l'arrivé du labyrinthe en partant du départ.
    >>> solution(lab2)
    [(1, 0), (1, 1), (1, 2), (1, 3), (1, 4), (1, 5), (2, 5), (3, 5), (4, 5), (5, 5), (6, 5)]
    """
    L2 = deepcopy(lab)
    chemin = [depart(L2)]
    case = chemin[0]
    i = case[0]
    j = case[1]
    while L2[i][j] != 3:
        voisine_1 = voisines(i,j,L2)[0] #afin d'utiliser une seule fois le 1er résultat de la fonction voisines on la stocke dans une variable.
        case = (voisine_1[0],voisine_1[1])
        chemin.append(case)
        i = case[0]
        j = case[1]
        if L2[i][j] == 0:  # on rajoute cette condition sinon l'arrivé risque d'être modifié (3 -> 4).
            L2[i][j] = 4

    return chemin
# PARTIE C

#b
def laby(fichier):
    lab4 = []
    sous_liste = []
    with open(fichier, 'r', encoding='utf-8') as fichier:
        ligne_lue = fichier.readline().strip('\n')
        while ligne_lue != '':
            for k in range(len(ligne_lue.split(';'))):
                sous_liste.append(int(ligne_lue.split(';')[k]))
            lab4.append(sous_liste)
            sous_liste = []
            ligne_lue = fichier.readline().strip('\n')
    return lab4
#certes on aura pu se passer de sous_liste mais de cette manière on peut transformer les éléments de la liste en entiers


#c
def affichage(fichier,n):
    """
    Affiche seulement le labyrinthe sans le tracé de la solution en rouge.
    """
    lab = laby(fichier)
    matrice(lab,n)
    


# Fin du fichier exercie2.py