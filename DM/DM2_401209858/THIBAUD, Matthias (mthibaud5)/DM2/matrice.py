# !/usr/bin/env python3
#-*- coding: utf-8 -*-

#
# Fichier : exercie2.py
#  Auteur : THIBAUD Matthias
#    Date : 29/09/2021
#

# Début du fichier matrice.py

from turtle import*

def carre(n):
    """ Fonction qui dessine un carré de taille n"""
    for i in range(4):
        forward(n)
        right(90)
        speed(0)
def carre_rempli(n):
    """Fonction qui dessine un carré noir de taille n"""
    color("black")
    begin_fill()
    carre(n)
    end_fill()
    
def matrice(l,n):
    for i in range(len(l)):
        for j in range(len(l[i])):
            if l[i][j] == 0 or l[i][j] == 2 or l[i][j] == 3:
                carre(n)
            else:
                carre_rempli(n)
            forward(n)
        backward(n*j+n)
        right(90)
        forward(n)
        left(90)
# Fin du fichier matrice.py