#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : voisines.py
#  auteur : Gregory MAUPU
#    date : 2021/10/29

def voisines(i,j,lab):
    liste = []
    n = len(lab)
    if (i >0 and i < n-1) and (j>0 and j < n-1) :
        if lab[i-1][j] == 0 or lab[i-1][j] == 3:
            liste.append((i-1,j))
        if lab[i+1][j] == 0 or lab[i+1][j] == 3:
            liste.append((i+1,j))
        if lab[i][j+1] == 0 or lab[i][j+1] == 3:
            liste.append((i,j+1))
        if lab[i][j-1] == 0 or lab[i][j-1] == 3:
            liste.append((i,j-1))
    elif (i == 0) and (j> 0 and j < n-1):
        if lab[i][j-1] == 0 or lab[i][j-1] == 3 :
            liste.append((0,j-1))
        if lab[i][j+1] == 0 or lab[i][j+1] == 3 :
            liste.append((0,j+1))
        if lab[i+1][j] == 0 or lab[i+1][j] == 3 :
            liste.append((i+1,j))
    elif i == n-1 and (j> 0 and j < n-1):
        if lab[i][j-1] == 0 or lab[i][j-1] == 3 :
            liste.append((i,j-1))
        if lab[i][j+1] == 0 or lab[i][j+1] == 3 :
            liste.append((i,j+1))
        if lab[i-1][j] == 0 or lab[i-1][j] == 3 :
            liste.append((i-1,j))
    elif (i> 0 and i < n-1) and (j == 0 or j == n-1):
        if lab[i-1][j] == 0 or lab[i-1][j] == 3:
            liste.append((i-1,j))
        if lab[i+1][j] == 0 or lab[i+1][j] == 3:
            liste.append((i+1,j))
        if j == n-1 and (lab[i][j-1] == 0 or lab[i][j-1] == 3) :
            liste.append((i,j-1))
        if j == 0 and (lab[i][j+1] == 0 or lab[i][j+1] == 3) :
            liste.append((i,j+1))
    elif i==0 and j == 0:
        if lab[i+1][j] == 0 or lab[i+1][j] == 3:
            liste.append((i+1,j))
        if lab[i][j+1] == 0 or lab[i][j+1] == 3:
            liste.append((i,j+1))
    elif i==0 and j == n-1:
        if lab[i+1][j] == 0 or lab[i+1][j] == 3:
            liste.append((i+1,j))
        if lab[i][j-1] == 0 or lab[i][j-1] == 3:
            liste.append((i,j-1))
    elif i==n-1 and j == n-1:
        if lab[i-1][j] == 0 or lab[i-1][j] == 3:
            liste.append((i-1,j))
        if lab[i][j-1] == 0 or lab[i][j-1] == 3:
            liste.append((i,j-1))
    elif i==n-1 and j == 0:
        if lab[i-1][j] == 0 or lab[i-1][j] == 3:
            liste.append((i-1,j))
        if lab[i][j+1] == 0 or lab[i][j+1] == 3 :
            liste.append((i,j+1))
    return liste

# fin du fichier voisines.py
