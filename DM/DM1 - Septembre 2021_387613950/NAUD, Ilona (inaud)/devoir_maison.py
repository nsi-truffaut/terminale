#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : devoir_maison 
#  auteur :Ilona NAUD
#    date : 2021/09/27


# Déclaration des variables

lettres = "abcdefghijklmnopqrstuvwxyz"
valeurs_scrabble = {10 : 'kwxyz', 8 : 'jq', 4 : 'fhv', 3 : 'bcp', 2:'dmg'}

# Programme

def points(mot) :
    """Fonction qui renvoie le score d'un mot
        >>> points('pizza')
        25
        >>> points('whisky')
        36
        >>> points('dédramatiser')
        15
    """
    point_lettres = list(valeurs_scrabble.items())
    score = 0

    for car in mot:
        j=0
        while j < len(valeurs_scrabble) and not(car in point_lettres[j][1]): 
            j = j+1
        if j<len(valeurs_scrabble):
            score = score + point_lettres[j][0]
        else :
            score = score + 1
    return score
    
def lettres_scrabble(dictionnaire):
    """crée un dictionnaire qui associe chaque lettres de l'alphabets au nombre de points identiques au règle du scrabble
        >>> lettres_scrabble(valeurs_scrabble)
        {'a': 1, 'b': 3, 'c': 3, 'd': 2, 'e': 1, 'f': 4, 'g': 2, 'h': 4, 'i': 1, 'j': 8, 'k': 10, 'l': 1, 'm': 2, 'n': 1, 'o': 1, 'p': 3, 'q': 8, 'r': 1, 's': 1, 't': 1, 'u': 1, 'v': 4, 'w': 10, 'x': 10, 'y': 10, 'z': 10}
    """
    dic={}
    element = list(dictionnaire.items())
    for car in lettres:
        j=0
        while j<len(dictionnaire) and not (car in element[j][1]):
            j=j+1
        if j<len(dictionnaire):
            dic[car]=element[j][0]
        else:
            dic[car]=1
    return dic

def score_mot(mot):
    """Fonction qui renvoie le score d'un mot selon le dictionnaire
        >>> points('pizza')
        25
        >>> points('whisky')
        36
        >>> points('dédramatiser')
        15
    """
    dic=lettres_scrabble(valeurs_scrabble)
    total = 0
    for i in mot:
        total = total+dic[i]
    return total

    
                                    
    
   



##with open('dictionnaire.txt','r') as fichier :
##    
##    def extraction(n):
##       
##        """Fonction qui renvoie la liste des mots de 7 lettres avec leur score
##            
##        """
##    
##        liste_mot = []
##        with open(n,'r',encoding='ANSI') as fichier:
##            ligne_lue = fichier.readline().strip()
##            while ligne_lue !="":
##                if len(ligne_lue)==7:
##                    liste_mot.append((ligne_lue,score_mot(ligne_lue)))
##                ligne_lue=fichier.readline().strip()
##        return liste_mot

if __name__ == '__main__':
    import doctest
    doctest.testmod()
      
