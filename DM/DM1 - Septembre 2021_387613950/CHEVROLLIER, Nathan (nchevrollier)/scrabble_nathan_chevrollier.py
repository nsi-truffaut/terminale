#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : anagramme.py
#  auteur : Gregory MAUPU
#    date : 2021/09/03


# Déclaration des variables

lettres = "abcdefghijklmnopqrstuvwxyz"
valeurs_scrabble = {10 : 'kwxyz', 8 : 'jq', 4 : 'fhv', 3 : 'bcp', 2:'dmg'}

# Programme

def points(mot) :
    """Fonction qui renvoie le score d'un mot
    >>> points('pizza')
    25
    >>> points('whisky')
    36
    >>> points('dédramatiser')
    15
    """
    point_lettres = list(valeurs_scrabble.items())
    score = 0
    for car in lettres :
        j=0
        while j < len(valeurs_scrabbles)  :
            j = j+1
        if lettres == point_lettres:
            score = score + point_lettres[j]
        else :
            score = score + 1
    return score
    
def lettres_scrabble(dictionnaire):
    """ fonction qui crée un dictionnaires qui fait correspondre chaque lettre de l'alphabet a son score
    >>> lettres_scrabble({q})
    8
    >>> lettres_scrabble({b})
    3
    >>> lettres_scrabble({z})
    10
    >>> lettres_scrabble({a})
    1
    """
    dictionnaire = list(dictionnaire.items())
    
    for i in lettres :
        j=0
        while lettres != valeurs_scrabble.values():
            dictionnaire[]
    
    
    
    
    
    #for i in dictionnaire[i][1]:
        #if
        #elif
        #else
        
        #lettres_scrabble[i] = valeurs_scrabble.key[]






def score_mot(mot):
    """ fonction qui calcule la valeur d'un mot à partir du dictionnaire précédent
    >>> score_mot(entrais)
    7
    >>> score_mot(ratines)
    7
    >>> score_mot(satiner)
    7
    >>> score_mot(riantes)
    7
    >>> score_mot(transie)
    7
    """
    
    score=0
    for element in list(mot):
        score= score + lettres_scrabble[element]
    return score
    
#tous les mots valent le même nombre de points

def extraction(n):
    """Fonction qui renvoie la liste des mots de 7 lettres avec leur score

    >>> extraction("dictionnaire.txt")
    ["entrais",7,"ratines",7,"satiner",7,"riantes",7,"transie",7]
    
    """
    liste_mot = []
    with open(n) as fichier :
        ligne_lue = fichier_readline().strip()
        while ligne_lue !="":
            if len(ligne_lue) == 7:
                liste_mot.append(ligne_lue)
                liste_mot.append(score_mot(ligne_lue))
        
    return liste_mot    
        
    
    
    
    
    
    




if __name__=='__main__' :
    import doctest
    doctest.testmod(verbose=True)



