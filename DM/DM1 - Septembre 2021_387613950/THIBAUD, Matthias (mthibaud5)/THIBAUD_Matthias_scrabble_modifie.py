#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : scrabble_modifie.py
#  auteur : Gregory MAUPU, Matthias THIBAUD
#    date : 2021/09/03


# Déclaration des variables

lettres = "abcdefghijklmnopqrstuvwxyz"
valeurs_scrabble = {10 : 'kwxyz', 8 : 'jq', 4 : 'fhv', 3 : 'bcp', 2:'dmg'}

# Programme

def points(mot) :
    """ Fonction qui renvoie le score d'un mot
        >>> points("pizza")
        25
        >>> points("whisky")
        36
        """
    point_lettres = list(valeurs_scrabble.items())
    score = 0
    for car in lettres :
        j = 0
        valeur = mot
        while j < len(valeur) and car in mot:
            j = j+1
        if car in point_lettres[0][1] or point_lettres[1][1] or point_lettres[2][1] or point_lettres[3][1] or point_lettres[4][1]:
            score = score + point_lettres[0][0]
        else :
                score = score + 1
    return score

print(points("pizza"))
#la fonction est iccorect mais je ne trouve pas de solutions au problème, en plus ce qui est bizzare c'est qu'on dirait qu'il y a un
#problème au niveau de l'indentation, puis même si on corrige ça pourquoi parcourir chaque lettre de l'alphabet ?

def lettres_scrabble(dictionnaire):
    """ Fonction qui convertit le dictionnaire valeurs_scrabble de la manière suivante :
        {"a":1, "b":3,...}
    """
    lettres_scrabble = {}
    dictionnaire = list(dictionnaire.items())
    for k in lettres:
        if k in dictionnaire[0][1]:
            lettres_scrabble[k] = 10
        elif k in dictionnaire[1][1]:
            lettres_scrabble[k] = 8
        elif k in dictionnaire[2][1]:
            lettres_scrabble[k] = 4
        elif k in dictionnaire[3][1]:
            lettres_scrabble[k] = 3
        elif k in dictionnaire[4][1]:
            lettres_scrabble[k] = 2
        else:
            lettres_scrabble[k] = 1
    return lettres_scrabble

# C'est très laid mais ça fonctionne !
  

def score_mot(mot):
    """ Fonction qui renvoie le score d'un mot
    >>>score_mot("pizza")
    25
    >>>score_mot("dedramatiser")
    15
    """
    letrres_scrabble = lettres_scrabble(valeurs_scrabble)
    score = 0
    for car in mot:
        score = score + letrres_scrabble[car]
    return score

def extraction(n):
    """Fonction qui renvoie la liste des mots de 7 lettres avec leur score
    n étant le nom du fichier.
    """
    liste_mot = []
    with open(n,'r',encoding='ANSI') as fichier:
        ligne_lue = fichier.readline().strip()
        while ligne_lue !="" :
            if len(ligne_lue) == 7:
                liste_mot.append((ligne_lue,score_mot(ligne_lue)))
            ligne_lue = fichier.readline().strip()
    return liste_mot
    
# fin du fichier scrabble_modifie.py
