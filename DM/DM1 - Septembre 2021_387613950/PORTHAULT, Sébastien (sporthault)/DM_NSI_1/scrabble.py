##!/usr/bin/env python3
## -*- coding: utf-8 -*-

## fichier : anagramme.py
##  auteur : Gregory MAUPU
##    date : 2021/09/03


## Déclaration des variables

lettres = "abcdefghijklmnopqrstuvwxyz"
valeurs_scrabble = {10 : 'kwxyz', 8 : 'jq', 4 : 'fhv', 3 : 'bcp', 2:'dmg'}

## Programme

def points(mot) : #Ne fonctionne pas.
    """Fonction qui renvoie le score d'un mot
    >>> points('pizza')
    25
    >>> points('whisky')
    36
    >>> points('dédramatiser')
    15
    """
    point_lettres = list(valeurs_scrabble.items())
    score = 0
    for car in lettres :
        valeurs = mot
        j = 0
        while j < len(valeurs) and car==[j]: 
            j = j+1
        if lettres == point_lettres :
            score = score + point_lettres[j]
        else :
            score = score + 1
    return score

def lettres_scrabble(dictionnaire): #Me semblait bon mais ne fonctionne pas.
    """ Fonction qui créer le dictionnaire lettres_scrabble qui associe une lettre à sa valeur
    >>> lettres_scrabble("a")
    1
    >>> lettres_scrabble("z")
    10
    >>> lettres_scrabble("m")
    2
    """
    resultat={}
    dictionnaire=list(dictionnaire.items())
    for i in lettres:
        if i in dictionnaire[0:4][1]:
            resultat[i]=valeurs_scrabble.key(i)
        else:
            resultat[i]=1
        return resultat

def score_mot(mot): #Me semble bon mais le programme d'au-dessus n'étant pas bon, je n'ai pas pu le tester.
    """ Fonction qui renvoie le score
    >>> score_mot('pizza')
    25
    >>> score_mot('whisky')
    36
    >>> score_mot('dédramatiser')
    15
    """
    lettres_scrabble = lettres_scrabble(valeurs_scrabble)
    resultat = 0
    for i in mot:
        resultat = resultat+lettres_scrabble[i]
    return resultat 

def extraction(n): #Fonctionne
    """Fonction qui renvoie la liste des mots de 7 lettres avec leur score
    """
    liste_mot = []
    with open('dictionnaire.txt','r',encoding='utf-8') as fichier :
        ligne = fichier.readline().strip()
        while ligne != "":
            if len(ligne) == 7:
                liste_mot.append((ligne,score_mot(ligne)))
            ligne = fichier.readline().strip()
    return liste_mot

if __name__=='__main__' :
    import doctest
    doctest.testmod(verbose=True)   
