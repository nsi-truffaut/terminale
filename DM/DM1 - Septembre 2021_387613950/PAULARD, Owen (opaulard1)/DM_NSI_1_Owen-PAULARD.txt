	#ex0

### 1-
#eleve.items() renvoie :
#dict_items([(10, 'Barnabé'), (11, 'Léa')])

### 2-
#Ce code renvoie :
#[(10, 'Barnabé') (11, 'Léa')
#
#'element' est un tuple

### 3-
#Ce code permet d'afficher chaque élément de 'eleve' séparément

### 4-
# >>>l = []
# >>>for element in eleve.items():
#    l.append(element)

### 5-
def ex0():
    eleve = {10 : "Barnabé", 11 : "Léa"}
    l = []
    for element in eleve.items():
        l.append(element)
    for age, nom in l:
        print(nom)
        print(age)


	#ex1

### a-
# pizza  = 3 + 1 + 10 + 10 + 1 = 25
# whisky = 10 + 4 + 1 + 1 + 10 +10  = 36
# dédramatiser = 1 + 1 + 1 + 1 + 1 + 2 + 1 + 1 + 1 + 1 + 1 + 1 = 13

### b-c-d
#Fichier en pièce jointe

