#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : anagramme.py
#  auteur : Gregory MAUPU
#    date : 2021/09/03


# Déclaration des variables

lettres = "abcdefghijklmnopqrstuvwxyz"
valeurs_scrabble = {10 : 'kwxyz', 8 : 'jq', 4 : 'fhv', 3 : 'bcp', 2:'dmg'}

# Programme

def points(mot) :
    """Fonction qui renvoie le score d'un mot
    >>> points('pizza')
    25
    >>> points('whisky')
    36
    >>> points('dedramatiser')
    13
    """
    point_lettres = list(valeurs_scrabble.items())
    score = 0 #A compléter
    for car in lettres :
        valeurs = mot #A compléter
        j = 0
        while j < len(valeurs) and car == [j] : #A compléter 
            j = j+1
        if car == point_lettres : #A compléter
            score = score + point_lettres #A compléter
        else :
            score = score + 1
    return j
    
def lettres_scrabble(dictionnaire):
    """ A compléter
    """
    lettres_scrabble = {}
    dictionnaire = list(dictionnaire.items())
    for i in lettres:
        if i in dictionnaire[0][1]:
            lettres_scrabble[i] = 10
        elif i in dictionnaire[1][1]:
            lettres_scrabble[i] = 8
        elif i in dictionnaire[2][1]:
            lettres_scrabble[i] = 4
        elif i in dictionnaire[3][1]:
            lettres_scrabble[i] = 3
        elif i in dictionnaire[4][1]:
            lettres_scrabble[i] = 2
        else :
            lettres_scrabble[i] = 1
    return lettres_scrabble
    
    
lettres_scrabbles = {"a": 1, "b": 3, "c": 3, "d": 2, "e": 1,
                     "f": 4, "g": 2, "h": 4, "i": 1, "j": 8,
                     "k": 10, "l": 1, "m": 2, "n": 1, "o": 1,
                     "p": 3, "q": 8, "r": 1, "s": 1, "t": 1,
                     "u": 1, "v": 4, "w": 10, "x": 10, "y": 10,
                     "z": 10}

def score_mot(mot):
    """ Fonction qui renvoie le score d'un mot
    """
    score  = 0
    for lettre in mot :
        score = score + lettres_scrabbles[lettre]
    return score
    # A compléter

def extraction(n):
    """Fonction qui renvoie la liste des mots de 7 lettres avec leur score
    """
    liste_mot = []
    with open(n,'r') as liste:
        mots = liste.read().split('\n')
    for i in range(len(mots)):
        if len(mots[i]) == 7:
            liste_mot.append(mots[i])
            liste_mot.append(score_mot(mots[i]))
    return liste_mot
