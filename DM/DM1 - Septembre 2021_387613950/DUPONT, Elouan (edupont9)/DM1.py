#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#Nom du fichier : DM1.py
#Auteur : DUPONT Elouan
#Date : 26/09/2021

###0.5

def dico():
    eleve = {10 : "Barnabé", 11 : "Léa"}
    j=[]
    for element in eleve.items():
        j.append(element)
    for age, nom in j:
        print(nom)
        print(age)

###Exercice 1

lettres = "abcdefghijklmnopqrstuvwxyz"
valeurs_scrabble = {10 : 'kwxyz', 8 : 'jq', 4 : 'fhv', 3 : 'bcp', 2:'dmg'}

###b :

def points(mot) :
    """Fonction qui renvoie le score d'un mot
    >>> points('pizza')
    25
    >>> points('whisky')
    36
    >>> points('dédramatiser')
    15
    """
    point_lettres = list(valeurs_scrabble.items())
    score = 0
    for car in lettres :
        valeurs = mot
        while j < len(valeurs) and car==[j]:
            j = j+1
        if lettres==point_lettres:
            score = score + point_lettres
        else :
            score = score + 1
    return score
    
if __name__ == "__main__":
    import doctest
    doctest.testmod()

#c :

def lettres_scrable(dictionnaire):
    """Dictionnaire qui fait correspondre chaque lettre de l'alphabet à son score
    >>> lettres_scrable('a')
    1
    >>> lettres_scrable('z')
    10
    >>> lettres_scrable('p')
    3
    """
    lettres_scrable = {"a" : 1, "b" : 3, "c" : 3, "d" : 2, "e" : 1, "f" : 4,
                       "g" : 2, "h" : 4, "i" : 1, "j" : 8, "k" : 10, "l" : 1, "m" : 2,
                       "n" : 1, "o" : 1, "p" : 3, "q" : 8, "r" : 1, "s" : 1,
                       "t" : 1, "u" : 1,"v" : 4, "w" : 10, "x" : 10, "y" : 10, "z" : 10}
    for lettre, score in lettres_scrable.items():
        print (lettre, score)
    
    









