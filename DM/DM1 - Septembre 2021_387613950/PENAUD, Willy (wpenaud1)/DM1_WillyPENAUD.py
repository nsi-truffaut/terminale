#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : DM1_WillyPENAUD.py
#  auteur : Willy Penaud
#    date : 2021/09/26


# Déclaration des variables

lettres = "abcdefghijklmnopqrstuvwxyz"
valeurs_scrabble = {10 : 'kwxyz', 8 : 'jq', 4 : 'fhv', 3 : 'bcp', 2:'dmg'}

# Programme

def points(mot) :
    """Fonction qui renvoie le score d'un mot
    
       Compléter la documentation avec les tests (cf question 1)
    """
    point_lettres = list(valeurs_scrabble.items())
    score = 0 
    for i in range(len(mot)) :
        j = 0 
        while j < len(valeurs_scrabble) and mot[i] not in point_lettres[j][1] :
            j = j+1
            print(1)
        print('----')
        if j < 5 :
            score = score + point_lettres[j][0]
        else :
            score = score + 1
    return score
    
def lettres_scrabble(dictionnaire):
    """ A compléter
    """
    dictionaire_lettres_scrabble = {}
    point_lettres = list(valeurs_scrabble.items())
    for car in lettres :
        for i in range(len(point_lettres)):
            if car in point_lettres[i][1]:
                dictionaire_lettres_scrabble[car] = point_lettres[i][0]
        if car not in dictionaire_lettres_scrabble:
            dictionaire_lettres_scrabble[car] = 1
    return dictionaire_lettres_scrabble

def score_mot(mot):
    """
        Fonction qui renvoie le score d'un mot donné

        >>> score_mot('entrais')
        7
        >>> score_mot('ratines')
        7
        >>> score_mot('satiner')
        7
        >>> score_mot('riantes')
        7
        >>> score_mot('transie')
        7
    """
    x = lettres_scrabble(valeurs_scrabble)
    score = 0
    for i in mot:
        score = score + x[i]
    return score

def extraction(n):
    """Fonction qui renvoie la liste des mots de 7 lettres avec leur score
    """
    liste_mot = []


print(lettres_scrabble(valeurs_scrabble))
