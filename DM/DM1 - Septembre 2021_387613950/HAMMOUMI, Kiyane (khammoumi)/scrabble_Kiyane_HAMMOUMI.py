#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : scrabble_Kiyane_HAMMOUMI.py
#  auteur : Gregory MAUPU ; Kiyane HAMMOUMI
#    date : 2021/09/26


# Déclaration des variables

lettres = "abcdefghijklmnopqrstuvwxyz"
valeurs_scrabble = {10 : 'kwxyz', 8 : 'jq', 4 : 'fhv', 3 : 'bcp', 2:'dmg'}

# Programme

def points(mot) :
    """Fonction qui renvoie le score d'un mot
    
       >>> points('pizza')
       25
       >>> points('whisky')
       36
       >>> points('dedramatiser')
       15
    """
    point_lettres = list(valeurs_scrabble.items())
    score = 0
    for car in mot :
        j=0
        while j < len(valeurs_scrabble) and not(car in point_lettres[j][1]):
            j = j+1
        if j<len(valeurs_scrabble):
            score = score + point_lettres[j][0]
        else :
            score = score + 1
    return score
    
def lettres_scrabble(dictionnaire):
    """ transforme un dictionnaire associant un score et les lettres de l'alphabet en dictionnaire qui associe chaque lettre et son score
        (étant de 1 si il est absent dans le dictionnaire utilisé)
        
        >>>lettres_scrabble(valeurs_scrabble)
        {'a': 1, 'b': 3, 'c': 3, 'd': 2, 'e': 1, 'f': 4, 'g': 2, 'h': 4, 'i': 1, 'j': 8, 'k': 10, 'l': 1, 'm': 2, 'n': 1, 'o': 1, 'p': 3, 'q': 8, 'r': 1, 's': 1, 't': 1, 'u': 1, 'v': 4, 'w': 10, 'x': 10, 'y': 10, 'z': 10}
    """
    dico={}
    element = list(dictionnaire.items())
    for car in lettres:
        j=0
        while j<len(dictionnaire) and not(car in element[j][1]):
            j = j+1
        if j<len(dictionnaire):
            dico[car]=element[j][0]
        else:
            dico[car]=1
    return dico
        

def score_mot(mot):
    """ Calcule le score d'un mot et le renvoie
        >>> score_mot('pizza')
        25
        >>> score_mot('whisky')
        36
        >>> score_mot('dedramatiser')
        15
    """
    valeur=lettres_scrabble(valeurs_scrabble)
    score=0
    for car in mot:
        score=score+valeur[car]
    return score

def extraction(n):
    """Fonction qui renvoie la liste des mots de n lettres avec leur score
    """
    liste_mot = []
    
    with open ('dictionnaire.txt','r',encoding='utf-8') as fichier :
        mot=fichier.readline().strip()
        while mot!="":
            if len(mot)==n:
                liste_mot.append((mot,score_mot(mot)))
            mot=fichier.readline().strip()
        return liste_mot

def meilleur_score(n):
    """Fonction qui renvoie le(s) mot(s) de n lettres qui possede(nt) le plus de points
    """
    mots=extraction(n)
    best_score=0
    mot=""
    for i in range(len(mots)):
        if best_score<mots[i][1]:
            best_score=mots[i][1]
            mot=mots[i][0]
        elif best_score==mots[i][1]:
            mot=mot+","+mots[i][0]
    return "{} avec un total de {} point(s)".format(mot,best_score)

#fin du programme