#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : scrabble_HERMIER.py
#  auteur : Maxime HERMIER
#    date : 2021/09/25


# Déclaration des variables

lettres = "abcdefghijklmnopqrstuvwxyz"
valeurs_scrabble = {10 : 'kwxyz', 8 : 'jq', 4 : 'fhv', 3 : 'bcp', 2:'dmg'}
point_lettres = list(valeurs_scrabble.items())

# Programme

def points(mot) :
    """Fonction qui renvoie le score d'un mot

       >>>points('pizza')
       25
       >>>points('whisky')
       36
       >>>points('dedramatiser')
       15

    """
    point_lettres = list(valeurs_scrabble.items())
    score = 0
    for car in mot :
        x=0
        while x < len(valeurs_scrabble) and not(car in point_lettres[x][1]):
            x = x+1
        if x<len(valeurs_scrabble):
            score = score + point_lettres[x][0]
        else :
            score = score + 1
    return score

def lettres_scrabble(dictionnaire):
    """ Dictionnaire qui associe une lettre et son score au jeu

        >>>lettres_scrabble(valeurs_scrabble)
        {'a': 1, 'k' : 10, 'j' : 8, 'f' : 4, 'b' : 3, 'd' : 2}
    """
    dictionnaire_1={}
    liste_dico = list(dictionnaire.items())
    for car in lettres:
        x=0
        while x<len(dictionnaire) and not(car in liste_dico[x][1]):
            x=x+1
        if x<len(dictionnaire):
            dictionnaire_1[car]=liste_dico[x][0]
        else:
            dictionnaire_1[car]=1
    return dictionnaire_1


def score_mot(mot):
    """ Fonction qui renvoi le score d'un mot

        >>>score_mot('entrais')
        7
        >>>score_mot('ratines')
        7
        >>>score_mot('satiner')
        7
        >>>score_mot('riantes')
        7
        >>>score_mot('transie')
        7
    """
    valeur=lettres_scrabble(valeurs_scrabble)
    score1=0
    for car in lettres:
        score1=score1+valeur[car]
    return score1

def extraction(n):
    """Fonction qui renvoie la liste des mots de 7 lettres avec leur score
    """
    liste_mot = []

    # non réussi

#Fin de programme


