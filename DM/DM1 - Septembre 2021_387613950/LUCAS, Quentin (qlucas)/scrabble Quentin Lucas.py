#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : anagramme.py
#  auteur : Gregory MAUPU
#    date : 2021/09/03


# Déclaration des variables

lettres = "abcdefghijklmnopqrstuvwxyz"
valeurs_scrabble = {10 : 'kwxyz', 8 : 'jq', 4 : 'fhv', 3 : 'bcp', 2:'dmg'}

# Programme

def points(mot) :
    """Fonction qui renvoie le score d'un mot
    
       >>> points(pizza)
       25
       >>> points(whisky)
       36
       >>> points(dédramatiser)
       13
    """
    point_lettres = list(valeurs_scrabble.items())
    score = 0
    for car in lettres :
        j=0
        while j < len(valeurs) and car!="": 
            i=0
        if mot[car] == valeur_scrabble.items[i][j]:
            score = score + valeurs_scrabble.keys[i][j]
            i = i +1
        else :
            score = score + 1
            i = i +1

            j = j +1
    return score
    
##def lettres_scrabble(dictionnaire):
##    """ renvoie un dictoinaire qui associe chaque lettre a son score
##    """
##    return {"a": 1, "b": 3, "c": 3, "d": 2, "e": 1, "f": 4, "g": 2, "h": 4, "i": 1, "j": 8, "k": 10, "l": 1, "m": 2, "n": 1, "o": 1, "p": 3, "q": 8: "r": 1, "s": 1, "t": 1, "u": 1, "v": 4, "w": 10, "x": 10, "y": 10, "z": 10}

def score_mot(mot):
    """ fonction qui renvoie le score d'un mot

        >>> score_mot(pizza)
       25
       >>> score_mot(whisky)
       36
       >>> score_mot(dédramatiser)
       13
    """
    score = 0
    for car in lettres:
        j=0
        while j < len(lettres_scrabble()) and car!="":
            if mot[car] == lettres_scrabble.keys[j]:
                score = score + lettres_scrabble.items[j]
                j = j+1
            else :j = j+1
    return score
        
def extraction(n):
    """Fonction qui renvoie la liste des mots de 7 lettres avec leur score
    """
    liste_mot = []
    



