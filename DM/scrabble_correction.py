#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : anagramme.py
#  auteur : Gregory MAUPU
#    date : 2021/09/03

# Déclaration des variables

valeurs_scrabble = {10 : 'kwxyz', 8 : 'jq', 4 : 'fhv', 3 : 'bcp', 2:'dmg'}
lettres = "abcdefghijklmnopqrstuvwxyz"

lettres_scrabble ={'a': 1, 'b': 3, 'c': 3, 'd': 2, 'e': 1,
                   'f': 4, 'g': 2, 'h': 4, 'i': 1, 'j': 8,
                   'k': 10, 'l': 1, 'm': 2, 'n': 1, 'o': 1,
                   'p': 3, 'q': 8, 'r': 1, 's': 1, 't': 1,
                   'u': 1, 'v': 4, 'w': 10, 'x': 10, 'y': 10,
                   'z': 10}
#Programmes

def points(mot) :
    valeur = 0
    lettre_point = valeurs_scrabble.items()
    for lettre in mot :
        for points, glettre in lettre_point :
            if lettre in glettre :
                valeur = valeur + points
            else :
                valeur = valeur +1
           
    return valeur

       
def new_dic(dic) :
    dico = {}
    glettres = list(dic.items())
    for car in lettres :
        j = 0
        while j < len(glettres) and (car not in glettres[j][1]) :
            j = j +1
        if j < len(glettres) :
            dico[car] = glettres[j][0]
        else :
            dico[car] = 1
            
    return dico

def extraction(n):
    """Fonction qui renvoie la liste des mots existants
       à partir de tous les anagrammes possibles.
    """
    liste_mot = []
    
    with open('dictionnaire.txt','r',encoding='utf-8') as fichier :
        l = fichier.readline().rstrip('\n')
       
        while l !="" :
             if len(l) == n+1 :
                 liste_mot.append(l.rstrip('\n').lower())
                 
             l = fichier.readline()
    return liste_mot

    
def score_score(mot):
    """ Fonction qui renvoie le score d'un mot
    """
    score = 0
    for lettre in mot :
        score = score + dicc[lettre]
    return score

def max_score(l):
    m = meilleur_score(l[0])
    
    for mot in l :
        n = meilleur_score(mot)
        if  n >= m :
            m = n
    return m

def liste_max_score(l):
    n = max_score(l)
    ll = []
    for mot in l :
        if meilleur_score(mot) == n :
            ll.append(mot)
    return ll
            
            
    
    

# Test du programme

l = extraction(7)
##
print(liste_max_score(l))

# fin du fichier scrabble_correction.py
    
