#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : karatsuba.py
#  auteur : Gregory MAUPU
#    date : 2021/12/16

# importation des modules

from multiplication import school_grade
from karatsuba import karatsuba
from matplotlib import pyplot as plt
import time
import random

def compar_complexite(fonction_rec,fonction_it,taille_max) :
         
         k = []
         s_g = []
         for i in range(taille_max) :
             a = random.randint(10**i,10**(i+1)-1)
             b = random.randint(10**i,10**(i+1)-1)
             t = time.time()
             fonction_rec(a,b)
             t2 = time.time() - t
             
             k.append(t2)
             t = time.time()
             fonction_it(a,b)
             t2 = time.time() - t
             
             s_g.append(t2)
         return [k for k in range(taille_max)],k,s_g

p,y1,y2 = compar_complexite(karatsuba,school_grade,500)

plt.plot(p,y1,color='red')
plt.plot(p,y2)
plt.show()
