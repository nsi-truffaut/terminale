#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : multiplication.py
#  auteur : Gregory MAUPU
#    date : 2021/12/16

# importation des modules

import random

def ajout_zero(s,nb,gauche=True):
    """Ajoute des zéros à gauche ou à droite d'une chaine de caractère

    >>> ajout_zero('245',6)
    '000000245'
    >>> ajout_zero('78596',3,False)
    '78596000'
    """
    for i in range(nb):
        if gauche :
            s = '0' + s
        else :
            s = s + '0'
    return s

def school_grade(n1,n2) :
    """ Renvoie la multiplication de deux nombres entiers positifs suivant
        l'algorithme utilisée à l'école élémentaire

    
    """
    s = str(n1)
    t = str(n2)
    somme_partielle = 0
    if len(s) > len(t) :
        t = ajout_zero(t,len(s)-len(t))
    else :
        s = ajout_zero(s,len(t)-len(s))
    
    for i in range(len(t)):
        carry = 0
        somme_ligne = ""
        for j in range(len(s)):
            a = int(s[len(s)-j-1])*int(t[len(t)-i-1]) + int(carry)
            
            if a >= 10 :
                if j < len(s) - 1 :
                    carry = str(a)[0]
                    somme_ligne = str(a)[1]+ somme_ligne
                else :
                    somme_ligne = str(a)+ somme_ligne
            else :
                
                somme_ligne =  str(a)[0]+ somme_ligne
                carry = 0
        
        somme_ligne=ajout_zero(somme_ligne,i,False)
        
        somme_partielle = somme_partielle + int(somme_ligne)
        
    return somme_partielle

if __name__=='__main__':
    import doctest
    doctest.testmod(verbose=True)

    assert school_grade(2569,45871) == 2569*45871

    a = random.randint(10**130, 10**132-1)
    b=  random.randint(10**130, 10**132-1)

    assert school_grade(a,b) == a*b

# fin du fichier multiplication.py
