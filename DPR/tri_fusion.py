from random import randint

# Différentes versions de la fonction fusion

# Une version récursive

def fusion_rec(A,B):
    
    if len(A) == 0:
        return B
    elif len(B) == 0:
        return A
    else :
      
      if A[0] <= B[0] :
         
          return [A[0]] + fusion_rec(A[1:len(A)],B)
      else :
          
          return [B[0]]+fusion_rec(A,B[1:len(B)])
        
# Une version itérative

def fusion_it(A,B):
    L = []
##    if len(A) == 0:
##        return B
##    elif len(B) == 0:
##        return A
##    else :
    while len(A) !=0 and len(B) != 0 :
        if A[0] < B[0] :
            L.append(A[0])
            A.pop(0)
        else :
            L.append(B[0])
            B.pop(0)
    L = L + A + B
    return L

def fusion_it(A,B):
    L = []
    while len(A) !=0 and len(B) != 0 :
        if A[0] < B[0] :
            L.append(A[0])
            A.pop(0)
        else :
            L.append(B[0])
            B.pop(0)
    L = L + A + B
    return L

def tri_fusion_rec(T):
    
    if len(T)==1 :
        return T
    else :
        m = (0 + len(T)-1)//2
        t1 = tri_fusion_rec(T[0:m+1])
        t2 = tri_fusion_rec(T[m+1:len(T)])
       
    return fusion_rec(t1,t2)


def tri_fusion_rec2(T,debut,fin):
    
    if debut == fin :
        return [T[debut]]
    else :
        m = (debut+fin)//2
        t1 = tri_fusion_rec2(T,debut,m)
        t2 = tri_fusion_rec2(T,m+1,fin)
    
    return fusion_rec(t1,t2)
    
    

def tri_fusion(T):
    return tri_fusion_rec2(T,0,len(T)-1)

T = [randint(-3,11) for i in range(10)]
print(T)
U = [2,3,7,9]
V = [4,11]
Z = [-2,3,1,0,-5]
print(fusion_it(U,V))
print(T)
print(tri_fusion_rec(T))
