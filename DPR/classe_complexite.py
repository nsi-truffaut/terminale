#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : classe_complexite.py
#  auteur : Gregory MAUPU
#    date : 2021/12/12

# importation des modules

from matplotlib import pyplot as plt
import math

def carre(x) :
    return x**2

def log(x):
    if x == 0 :
        return 0
    else :
        return math.log(x,2)

def xlog(x):
    if x == 0 :
        return 0
    else :
        return x*math.log(x,2)

def ordonnee(fonction,debut,fin,pas):
    y = []
    for k in range(debut,fin):
        x = k*pas
        y.append(fonction(x))
    return y

n= 2000
x = []
pas = 0.01
for k in range(0,n) :
    x.append(k*pas)

plt.xlim(0,20)
plt.ylim(0,20)
y = ordonnee(carre,0,n,pas)
y1 = ordonnee(log,0,n,pas)
y2 = ordonnee(xlog,0,n,pas)
plt.plot(x,y,'-',label="quadratique")
plt.plot(x,x,'-.',label="linéaire")
plt.plot(x,y1,'--',label="logarithmique")
plt.plot(x,y2,':', label="quasi linéaire")
plt.text(5.5,12.5,'tri fusion O(nlog(n))',rotation=70)
plt.text(10.5,2.5,'recherche dichotomique O(log(n))')
plt.text(10,9,'min,max,occurence dans tableau O(n)',rotation=35)
plt.text(1.8,5,'tri insertion,sélection,à bulles O(n²)',rotation=80)
plt.legend()
plt.show()
