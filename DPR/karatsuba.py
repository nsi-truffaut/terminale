#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : karatsuba.py
#  auteur : Gregory MAUPU
#    date : 2021/12/16

# importation des modules

import time
from random import randint
from multiplication import ajout_zero

# La fonction

        
def karatsuba(nombre1,nombre2):
    """ Renvoie le produit de deux nombres entiers positifs suivant
        l'algorithme de karatsuba
    """
    s1 = str(nombre1)
    s2 = str(nombre2)
    n1 = len(s1)
    n2 = len(s2)
        
    if n1 == 1 and n2 == 1 :
        return int(s1)*int(s2)
    else :
        if n1 < n2 :
            s1 = ajout_zero(s1,n2-n1)
        else :
            s2 = ajout_zero(s2,n1-n2)
        n = len(s1)
        m = n//2
##        if n%2 != 0 :
##            m = m+1
        
        k = n - m
        l = k *2
        a = int(s1[0:m])
        b = int(s1[m:len(s1)])
        c = int(s2[0:m])
        d = int(s2[m:len(s2)])
        
        ac = karatsuba(a,c)
        bd = karatsuba(b,d)
        e = karatsuba(a+b,c+d)
        A = int(ajout_zero(str(ac),l,False))
        B = int(ajout_zero(str(e-ac-bd),k,False))
        
    return A + B + bd

if __name__=='__main__' :
    
    a = randint(10**500,10**501-1)
    b = randint(10**500,10**501-1)
    assert karatsuba(a,b)==a*b

# fin du fichier karatsuba.py

        
