#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : traitement_image.py
#  auteur : Gregory MAUPU
#    date : 2021/12/12

# Importation des modules

from PIL import Image


image = Image.open('images/hokusai.png') #Ouvre l'image

largeur,hauteur = image.size # Permet de récupérer la taille



def niveau_gris(image):
    pixels = image.load()
    for i in range(largeur):
        for j in range(hauteur):
            m = (pixels[i,j][0] + pixels[i,j][1] + pixels[i,j][2])//3
            pixels[i,j] = (m,m,m)
    image.show()

def miroir(image):
    pixels = image.load()
    im_dest = Image.new('RGB',(largeur,hauteur)) # Crée une image vide de même largeur
                                                # et même hauteur que l'image original
    pixels_dest=im_dest.load()
    for i in range(largeur):
        for j in range(hauteur):
            pixels_dest[i,j] = pixels[j,i]
    im_dest.save("test.jpg")



def rotation_rec(pixels,x,y,t):
    
    if t == 1 :
        return
    else :
        t = t//2
        rotation_rec(pixels,x,y,t)
        rotation_rec(pixels,x+t,y,t)
        rotation_rec(pixels,x,y+t,t)
        rotation_rec(pixels,x+t,y+t,t)
        

    for i in range(x,x+t) :
        for j in range(y,y+t):
            pixels[i,j+t],pixels[i+t,j+t],pixels[i+t,j],pixels[i,j]=pixels[i,j],pixels[i,j+t],pixels[i+t,j+t],pixels[i+t,j] 

def rotation(image,taille):
    pixels = image.load()
    rotation_rec(pixels,0,0,taille)
    
image.show()     
#niveau_gris(image)
miroir(image)
#rotation(image,512)
#image.show() 


            
