#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : traitement_image.py
#  auteur : Gregory MAUPU
#    date : 2021/12/12

# Importation des modules

from PIL import Image


image = Image.open('images/hokusai.png') #Ouvre l'image

# La fonction rotation_aux_rec ne renvoie rien
# Elle modifie l'image dont les coordonnées en haut à gauche sont (x,y) et en bas à droite (x+t,y+t)


def rotation_aux_rec(pixels,x,y,t):
    
    if t == 1 :
        return
    else :
        t = t//2
        # Appel récursif sur chaque sous-partie de l'image
         #  A compléter - Correspond au 1er quadrant (en haut et à gauche)
        # A compléter - correspond au 2e quadrant (en bas et à gauche)
        
        # A compléter - correspond au 3e quadrant (en bas et à droite)
       
        # A compléter - correspond au 4e quadrant (en haut et à droite)
        

    # Rotation d'un quart de tour de l'image par déplacement des pixels
    # Pour compléter, aidez vous de l'aide dans le TP
    
            
           
            
           


def rotation_quart(image,taille):
    pixels = image.load()
    rotation_aux_rec(pixels,0,0,taille)

if __name__=="__main__" :
    rotation_quart(image,512)
    image.show()
    
# fin du fichier traitement_image.py
