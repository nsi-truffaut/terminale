#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : DPR_exemples.py
#  auteur : Gregory MAUPU
#    date : 2021/12/12

# importation des modules

import time
from matplotlib import pyplot as plt
from random import *

# Algorithme de dichotomie

def dicho_it(T,e) :
    present = False
    debut = 0
    fin = len(T) - 1
    while debut <= fin and not present :
        m = (debut + fin)//2
        if T[m] == e :
            present = True
        else :
            if e < T[m] :
                fin = m -1
            else :
                debut = m+1
    return present

def dicho_rec(T,e,debut,fin):
    if fin < debut :
        return False
    m = (debut+fin)//2
    if T[m] == e :
        return True
    else :
        if e < T[m] :
            return dicho_rec(T,e,debut,m-1)
        else :
            return dicho_rec(T,e,m+1,fin)
        
def occurence(T,e,debut,fin):
    r = 0
    if fin < debut :
        return r
    m = (debut + fin)//2
    if T[m] == e :
        r = r+ 1 #+ occurence(T,e,debut,m-1) + occurence(T,e,m+1,fin)
    else :
        r = r+ occurence(T,e,debut,m-1) + occurence(T,e,m+1,fin)
    return r

def occurence_it(T,e):
    c = 0
    for i in range(len(T)) :
        if T[i]  == e :
            c = c +1
    return c

# Recherche tuple consécutif

def recherche_dpr(T,debut, fin):
    
    if debut == fin :
       return []
        
    else :
        m = (debut+fin)//2
        if T[m] +1 == T[m+1] :
            return   [(T[m],T[m+1])] + recherche_dpr(T,debut,m) +recherche_dpr(T,m+1,fin)
        else :
            return   recherche_dpr(T,debut,m) +recherche_dpr(T,m+1,fin)

print(recherche_dpr([1,2,3,4,5],0,4))

if __name__=="__main__" :
     # Construction de listes de nombres alétoires de plus en plus grandes

     def compar_complexite(fonction_rec,fonction_it,n) :
         x = []
         y_1 = []
         y_2 = []
         for i in range(1,n) :
             x.append([randint(0,500) for _ in range(i)])
             t = time.time()
             fonction_rec(x[i-1],6,0,len(x[i-1])-1)
             t2 = time.time() - t
             
             y_1.append(t2)
             t = time.time()
             fonction_it(x[i-1],6)
             t2 = time.time() - t
             
             y_2.append(t2)
         return [k for k in range(1,n)],y_1,y_2
        
     
     
     #p,y1,y2 = compar_complexite(occurence,occurence_it,1000)
     #p,y3,y4 = compar_complexite(dicho_rec,dicho_it,1000)

     # Comparaison occurence
     #plt.plot(p,y1,'.')
     #plt.plot(p,y2,'.')
     # Comparaison dichotomie
     #plt.plot(p,y3)
     #plt.plot(p,y4)
     
     #plt.show()
     # Complexité de la dichotomie
     
     
