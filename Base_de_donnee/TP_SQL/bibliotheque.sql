drop table Edite;
drop table Livre;
drop table Auteur;
drop table Editeur;


create table Auteur(id_auteur INT , nom VARCHAR(50), prenom VARCHAR(50), ann_naissance INT, langue VARCHAR(50), PRIMARY KEY(id_auteur));

create table Livre(id_livre INT NOT NULL, titre VARCHAR(50), date_publication INT, note REAL, id_auteur INT, FOREIGN KEY(id_auteur) REFERENCES Auteur(id_auteur), PRIMARY KEY(id_livre));
          
create table Editeur(id_editeur INT, nom VARCHAR(50),PRIMARY KEY(id_editeur));

create table Edite(id_livre INT, id_editeur INT, PRIMARY KEY(id_livre,id_editeur), FOREIGN KEY(id_livre) REFERENCES Livre(id_livre), FOREIGN KEY(id_editeur) REFERENCES Editeur(id_editeur));

INSERT INTO Auteur VALUES (1, 'Asimov', 'Isaac', 1920,'Anglais');
INSERT INTO Auteur VALUES (2, 'Herbert', 'Franck', 1920,'Anglais');
INSERT INTO Auteur VALUES (3, 'Lucazeau', 'Romain', 1981,'Français');
INSERT INTO Auteur VALUES (4, 'Anderson', 'Poul', 1926,'Anglais');
INSERT INTO Auteur VALUES (5, 'Orwell', 'Georges', 1903,'Anglais');
INSERT INTO Auteur VALUES (6, 'Van Vogt', 'A.E', 1912,'Anglais');
INSERT INTO Auteur VALUES (7, 'Robinson', 'Kim Stanley', 1952,'Anglais');
INSERT INTO Auteur VALUES (8, 'Tchaikovsky', 'Adrian', 1972,'Anglais');
INSERT INTO Auteur VALUES (9, 'Barjavel', 'René', 1911,'Français');
INSERT INTO Auteur VALUES (10, 'Glukhovsky', 'Dmitry', 1979,'Russe');
INSERT INTO Auteur VALUES (11, 'Cixin', 'Liu', 1963,'Chinois');
INSERT INTO Auteur VALUES (12, 'Eschbach', 'Andreas', 1959,'Allemand');

INSERT INTO Livre VALUES (1, 'Fondation', 2018, 8.1,1);
INSERT INTO Livre VALUES (2, 'Un défilé de robots', 2002, 7.6,1);
INSERT INTO Livre VALUES (3, 'Dune', 1970, 8.2,2);
INSERT INTO Livre VALUES (4, 'Latium 1', 2018, 6.8,3);
INSERT INTO Livre VALUES (5, 'Fondation et Empire', 1966, 8,1);
INSERT INTO Livre VALUES (6, 'Latium 2', 2018, 7.2,3);
INSERT INTO Livre VALUES (7, 'Tau Zero', 2012, 7,4);
INSERT INTO Livre VALUES (8, '1984', 1950, 8.3,5);
INSERT INTO Livre VALUES (9, 'Le monde des non A', 1952, 7.2,6);
INSERT INTO Livre VALUES (10, 'Mars la rouge', 2003, 7.2,7);
INSERT INTO Livre VALUES (11, 'Mars la bleue', 2003, 7.5,7);
INSERT INTO Livre VALUES (12, 'Mars la verte', 2003, 7.6,7);
INSERT INTO Livre VALUES (13, 'Dans la toile du temps', 2019, 7.5,8);
INSERT INTO Livre VALUES (14, 'Dans les profondeurs du temps', 2021, 6.6,7);
INSERT INTO Livre VALUES (15, 'Fondation et Empire', 2000, 8,1);
INSERT INTO Livre VALUES (16, 'Ravage', 2000, 7.3,9);
INSERT INTO Livre VALUES (17, 'Métro 2033', 2017, 7.1,10);
INSERT INTO Livre VALUES (18, 'Le problème à 3 corps', 2018, 7.3,11);
INSERT INTO Livre VALUES (19, 'Des milliards de tapis de cheveux', 1999, 7.8,12);
INSERT INTO Livre VALUES (20, NULL, NULL, NULL,8);

INSERT INTO Editeur VALUES (1, 'Gallimard');
INSERT INTO Editeur VALUES (2, 'Jai lu');
INSERT INTO Editeur VALUES (3, 'Denoel');
INSERT INTO Editeur VALUES (4, 'Robert Laffond');
INSERT INTO Editeur VALUES (5, 'Belial');
INSERT INTO Editeur VALUES (6, 'Hachette');
INSERT INTO Editeur VALUES (7, 'Pocket');
INSERT INTO Editeur VALUES (8, 'Folio');
INSERT INTO Editeur VALUES (9, 'Livre de poche');
INSERT INTO Editeur VALUES (10, 'Actes Sud');
INSERT INTO Editeur VALUES (11, 'Atalante');

INSERT INTO Edite VALUES (1,1);
INSERT INTO Edite VALUES (2,2);
INSERT INTO Edite VALUES (3,3);
INSERT INTO Edite VALUES (4,4);
INSERT INTO Edite VALUES (5,1);
INSERT INTO Edite VALUES (6,1);
INSERT INTO Edite VALUES (7,5);
INSERT INTO Edite VALUES (8,1);
INSERT INTO Edite VALUES (9,6);
INSERT INTO Edite VALUES (10,7);
INSERT INTO Edite VALUES (11,7);
INSERT INTO Edite VALUES (12,7);
INSERT INTO Edite VALUES (13,8);
INSERT INTO Edite VALUES (14,3);
INSERT INTO Edite VALUES (15,8);
INSERT INTO Edite VALUES (16,8);
INSERT INTO Edite VALUES (17,9);
INSERT INTO Edite VALUES (18,10);
INSERT INTO Edite VALUES (19,11);
INSERT INTO Edite VALUES (20,3);
