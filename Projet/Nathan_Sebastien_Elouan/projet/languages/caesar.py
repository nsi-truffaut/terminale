#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#Nom du fichier : caesar.py
#Auteur : DUPONT Elouan
#Date : 14/11/2021

alphabet = list('abcdefghijklmnopqrstuvwxyz')
alphabet2 = list('abcdefghijklmnopqrstuvwxyz')

phrase = input("Ecrivez une phrase:")
print(phrase)
clé = input("Valeur du decalage?")

def ceasar():
    x,y,z = 0,0,0
    while x<26:
        y = x+int(clé)                             #<---- idée de base(pas fonctionnel)
        z = y - 26
        if(y<26):
            alphabet[x] = alphabet[y]
        else:
            alphabet[x] = alphabet2[z]
        x = x + 1
    return(alphabet)

#2ème idée :

alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j","k","l","m",
            "n","o", "p","q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
cle = 3

def cryptage(cle, lettre):
    for i in range(len(alphabet)):
        if lettre == " ":
            return " "
        elif alphabet[i] == lettre:      #<---- crypte les lettres
            return alphabet(i + cle)
               
def caesar(cle, mot):
    mot_crypte = ""
    for lettre in mot:
        mot_crypte = mot_crypte + cryptage(cle, lettre)    #<---- retourne le mot crypté
    return(mot_crypte)


alphabet_decale={'a': 'D', 'b': 'E', 'c': 'F', 'd': 'G', 'e': 'H', 'f': 'I', 'g': 'J', 'h': 'K', 'i': 'L','j': 'M', 'k': 'N', 'l': 'O', 'm': 'P',
                'n': 'Q', 'o': 'R', 'p': 'S', 'q': 'T', 'r': 'U', 's': 'V','t': 'W', 'u': 'X', 'v': 'Y', 'w': 'Z', 'x': 'A', 'y': 'B', 'z': 'C'}
    
    
    



