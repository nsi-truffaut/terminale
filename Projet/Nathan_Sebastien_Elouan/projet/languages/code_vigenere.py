##!/usr/bin/env Python 3
## -*- coding: utf-8 -*-
##
## Nom du fichier : code_vigenere.py
##         Auteur : PORTHAULT Sébastien
##           Date : 04/12/2021

def encodage(phr,cle):
    """Encode un message(phr) en vigenère avec la clé(cle)
    >>> encodage(['b','o','n','j','o','u','r'],['b','j','r'])
    'dyflymt'
    >>> encodage(['s','a','l','u','t'],['o','m','g'])
    'mnsjg'
    """
    alphabet=[' ','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
    stock_cond=1
    stock=0
    resultat=''
    if len(phr)!=len(cle):
        cle.append(cle[stock_cond])
        stock_cond=stock_cond+1
        if stock_cond>len(cle):
            stock_cond=1
    else:
        for i in range(len(phr)-1):
            stock=alphabet.index(phr[i])+alphabet.index(cle[i])
            if stock>26:
                stock=stock-26
            else:
                resultat=resultat+alphabet[stock]
    return resultat

def decodage(resultat,cle):
    """Encode un message(phr) en vigenère avec la clé(cle)
    >>> decodage(['d','y','f','l','y','m','t'],['b','j','r'])
    'bonjour'
    >>> decodage(['m','n','s','j','g'],['o','m','g'])
    'salut'
    """
    alphabet=[' ','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
    stock_cond=1
    stock=0
    phr=''
    if len(resultat)!=len(cle):
        cle.append(cle[stock_cond])
        stock_cond=stock_cond+1
        if stock_cond>len(cle):
            stock_cond=1
    else:
        for i in range(len(resultat)-1):
            stock=alphabet.index(resultat[i])-alphabet.index(cle[i])
            if stock<26:
                stock=stock+26
            else:
                phr=phr+alphabet[stock]
    return phr

if __name__=="__main__":
    import doctest
    doctest.testmod(verbose=True)

##Fin du fichier code_vigenere.py
