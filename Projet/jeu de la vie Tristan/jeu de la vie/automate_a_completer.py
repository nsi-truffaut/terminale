#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : automate_a_completer.py
#  auteur : Gregory MAUPU & Tristan Roy-Salas
#    date : 02/02/2023


# Importation des modules

import os
import time

class Cell :

    def __init__(self):
        self.etat = 0 #definie une cellule comme morte a l'état initial 

    def __repr__(self):
        if self.etat == 1:
            return "V"
        else:
            return " "
   
    def vivant(self):
        self.etat = 1 #definie une cellule comme vivante

    def mort(self):
        self.etat = 0 #definie une cellule comme morte
        
class Grille :

    def __init__(self,taille):
        self.taille = taille
        self.grille = [[Cell()for j in range(taille)] for i in range(taille)] #créer une grille rempli de cellule morte et de taille souhaite
       
    def get_cell(self,i,j):
        return self.grille[i][j] #permet de recuperer l'etat d'une cellule de coordonne i j 

    def __repr__(self):
        txt = ""
        for i in range(self.taille):
            txt = txt + "\n"
            for j in range(self.taille):
                txt = txt+ str(self.get_cell(i,j))
        return txt
            
            

    def __len__(self):
        return self.taille

class Jeu :

    def __init__(self,grille) :
        self.grille = grille
        self.n = len(grille)
       
    def update(self):
        """
        permet de mettre a jour l'etat de toute les cellule
        """
        Ngrille = Grille(self.n)
        for i in range (self.n):
            for j in range (self.n):
                    #cas des coins
                if i==0 and j==0:                     #haut gauche
                    Voisin = [self.grille.get_cell(self.n-1,self.n-1),self.grille.get_cell(self.n-1,j),self.grille.get_cell(self.n-1,j+1),self.grille.get_cell(i,self.n-1),self.grille.get_cell(i,j+1),self.grille.get_cell(i-1,self.n-1),self.grille.get_cell(i-1,j),self.grille.get_cell(i-1,j+1)]
                elif i==0 and j==self.n-1:            #haut droite
                    Voisin = [self.grille.get_cell(self.n-1,j-1),self.grille.get_cell(self.n-1,j),self.grille.get_cell(self.n-1,0),self.grille.get_cell(i,j-1),self.grille.get_cell(i,0),self.grille.get_cell(i+1,j-1),self.grille.get_cell(i+1,j),self.grille.get_cell(i+1,0)]
                elif i==self.n-1 and j==0:            #bas gauche
                    Voisin = [self.grille.get_cell(i-1,self.n-1),self.grille.get_cell(i-1,j),self.grille.get_cell(i-1,j+1),self.grille.get_cell(i,self.n-1),self.grille.get_cell(i,j+1),self.grille.get_cell(0,self.n-1),self.grille.get_cell(0,j),self.grille.get_cell(0,j+1)]
                elif i==self.n-1 and j==self.n-1:     #bas droite
                    Voisin = [self.grille.get_cell(i-1,j-1),self.grille.get_cell(i-1,j),self.grille.get_cell(i-1,0),self.grille.get_cell(i,j-1),self.grille.get_cell(i,0),self.grille.get_cell(0,j-1),self.grille.get_cell(0,j),self.grille.get_cell(0,0)]
                    #cas des bords simple
                elif j+1>self.n-1:                    #droite
                    Voisin = [self.grille.get_cell(i-1,j-1),self.grille.get_cell(i-1,j),self.grille.get_cell(i-1,0),self.grille.get_cell(i,j-1),self.grille.get_cell(i,0),self.grille.get_cell(i+1,j-1),self.grille.get_cell(i+1,j),self.grille.get_cell(i+1,0)]
                elif j-1<0:                           #gauche
                    Voisin = [self.grille.get_cell(i-1,self.n-1),self.grille.get_cell(i-1,j),self.grille.get_cell(i-1,j+1),self.grille.get_cell(i,self.n-1),self.grille.get_cell(i,j+1),self.grille.get_cell(i+1,self.n-1),self.grille.get_cell(i+1,j),self.grille.get_cell(i+1,j+1)]
                elif i+1>self.n-1:                    #bas
                    Voisin = [self.grille.get_cell(i-1,j-1),self.grille.get_cell(i-1,j),self.grille.get_cell(i-1,j+1),self.grille.get_cell(i,j-1),self.grille.get_cell(i,j+1),self.grille.get_cell(0,j-1),self.grille.get_cell(0,j),self.grille.get_cell(0,j+1)]
                elif i-1<0:                           #haut
                    Voisin = [self.grille.get_cell(self.n-1,j-1),self.grille.get_cell(self.n-1,j),self.grille.get_cell(self.n-1,j+1),self.grille.get_cell(i,j-1),self.grille.get_cell(i,j+1),self.grille.get_cell(i+1,j-1),self.grille.get_cell(i+1,j),self.grille.get_cell(i+1,j+1)]
                else:
                    #cas classique
                    Voisin = [self.grille.get_cell(i-1,j-1),self.grille.get_cell(i-1,j),self.grille.get_cell(i-1,j+1),self.grille.get_cell(i,j-1),self.grille.get_cell(i,j+1),self.grille.get_cell(i+1,j-1),self.grille.get_cell(i+1,j),self.grille.get_cell(i+1,j+1)]
                compt = 0
                for k in range(len(Voisin)):
                    if str(Voisin[k]) == "V":
                        compt = compt+1
                if str(self.grille.get_cell(i,j)) == " " and compt==3:
                    Ngrille.get_cell(i,j).vivant()
                elif str(self.grille.get_cell(i,j)) == "V" and(compt==2 or compt==3):
                    Ngrille.get_cell(i,j).vivant()
                else:
                    Ngrille.get_cell(i,j).mort()
        self.grille = Ngrille

    def evolution(self,temps):
        """
        permet de mettre a jour toute les cellule k fois
        """
        k = 0
        while k < int(temps) :
            self.update()
            print(self.grille)
            time.sleep(0.1) # Permet de mettre en pause après la mise à jour des cellules, ici 0,1s
            os.system("clear") # Efface la console avant la prochaine mise à jour
            k = k+1
            
if __name__ == "__main__":

    a=Jeu(Grille(30))
    """
    # exemple 2
    a.grille.get_cell(5,5).vivant()
    a.grille.get_cell(6,5).vivant()
    a.grille.get_cell(6,6).vivant()
    a.grille.get_cell(6,7).vivant()
    a.grille.get_cell(7,5).vivant()
    a.grille.get_cell(7,6).vivant()
    a.grille.get_cell(8,5).vivant()
    """
    # exemple 1
    a.grille.get_cell(5,5).vivant()
    a.grille.get_cell(5,6).vivant()
    a.grille.get_cell(5,7).vivant()
    a.grille.get_cell(6,7).vivant()
    a.grille.get_cell(7,6).vivant()

    a.evolution(20)
    # Vous proposerez un test, issue des exemples 1 ou 2

# fin du fichier automate_a_completer
