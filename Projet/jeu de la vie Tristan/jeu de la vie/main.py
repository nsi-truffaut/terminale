#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : main.py
#  auteur : Tristan Roy-Salas
#    date : 13/02/2023

from automate_a_completer import*

def modele(Nmodele,Ngrille):
    """
    Permet de récupérer la configuration souhaité par le joueur et d'initialiser la grille avec cette derniere
    """
    if int(Nmodele) ==1:
        with open("./modele/modele1.csv","r") as fichier:
            liste = fichier.readline().strip().split(",")
            Lmodele = []
            while liste != [""]:
                liste = fichier.readline().strip().split(",")
                Lmodele.append(liste)

    elif int(Nmodele) == 2:
        with open("./modele/modele2.csv","r") as fichier:
            liste = fichier.readline().strip().split(",")
            Lmodele = []
            while liste != [""]:
                liste = fichier.readline().strip().split(",")
                Lmodele.append(liste)
        
    elif int(Nmodele) == 3:
        with open("./modele/modele3.csv","r") as fichier:
            liste = fichier.readline().strip().split(",")
            Lmodele = []
            while liste != [""]:
                liste = fichier.readline().strip().split(",")
                Lmodele.append(liste)
        
    else:
        raise Exception("veuillez choisir un programme entre 1 et 3")

    for i in range(len(Lmodele)-1):
        for j in range(len(Lmodele[0])):
            if int(Lmodele[i][j]) == 1:
                Ngrille.grille.get_cell(i,j).vivant()

    return 

def interface():
    """
    permet d'afficher une interface utilisateur simple pour lancer la simulation
    """
    #creation de la grille d'une taille >= a 35
    taille = input("Bonjour et bienvenue dans le jeu de la vie \npour commencer veuillez saisir la taille de la grille (minimum 35):\n")
    assert int(taille)>=35, "la grille doit faire au minimum 35"
    Ngrille = Jeu(Grille(int(taille)))

    #initialisation de la grille celon le modele choisi
    modele(input("Très bien, maintenant veuillez choisir un modele prédéfini: \n 1:le gosper glider gun \n 2:la galaxie de kok \n 3:un planeur \n "),Ngrille)

    #lance la simulation 
    Ngrille.evolution(input("Et pour finir veuillez choisir le temps de la simulation:\n"))

interface() #permet de lancer le jeu entierement 

# fin du fichier 
