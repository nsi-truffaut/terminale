import string
from module_chiffrement import chaine_vers_bin


class Text :

    def __init__(self,fichier):
        self.f = fichier
        self.__dico = { chr(i) : i for i in range(256)}
        #self.__dico = {"a" :0,"b":1,"c":2} 
##        self.__dico =  dict(zip(string.ascii_lowercase, [k for k in range(26)]))
##        self.__dico_u =  dict(zip(string.ascii_uppercase,[k for k in range(26,52)]))
##        self.__dico_n = dict(zip([str(k) for k in range(10)],[k for k in range(53,64)]))
##        self.__dico.update(self.__dico_u)
##        self.__dico.update({' ' : 52})
##        self.__dico.update(self.__dico_n)
        self.__dico_dec = { i[1] : i[0] for i in self.__dico.items()}

    def get_dic(self):
        return self.__dico_dec

    def get_dic2(self) :
        return self.__dico

    def vers_bin(self):
        with open(self.f,'r',encoding='utf-8') as fichier :
            with open(self.f+"_bin",'w',encoding='utf-8') as f :
                ligne = fichier.readline().strip()
                while ligne !="" :
                    l = list(map(str,chaine_vers_bin(ligne)))
                    f.write("".join(l)+'\n')
                    ligne = fichier.readline().strip()
        
        
    def compression_lzw_1(self,chaine):
        s = ''
        n = len(chaine)
        t = len(self.__dico)
        
        courant = ''
        for i in range(n) :
            
            fenetre = courant + chaine[i]
            
            if fenetre not in self.__dico.keys() :
                    self.__dico[fenetre] = t
                    t = t+1
                    s = s + str(self.__dico[courant]) + ' '
                    #print(fenetre,end='/')
                    courant = chaine[i]
            else :
                courant = fenetre
        #print(self.__dico)   
        return s + str(self.__dico[courant])

    def decompression_lzw_1(self,code):
        s=''
        
        liste_code = list(map(int,code.split()))
        n = len(liste_code)
        t = len(self.__dico_dec)
        car_lu ="" # Le caractère qu'on vient de décodé. Au début, on a rien encore
        
        for nombre in liste_code :
            
            if nombre not in self.__dico_dec : # On teste si le nombre est dans le dico
                self.__dico_dec[nombre] = car_lu + car_lu[0] # On ajoute le caractère qui vient d'être décode + l'encours
            car_decode = self.__dico_dec[nombre] # On décode de toute façon
            s = s + car_decode # On construit le message
            #print(nombre,car_decode,s,t,car_lu)
            if car_lu !="" and car_lu + car_decode[0] not in self.__dico_dec.values():
                    self.__dico_dec[t] = car_lu + car_decode[0] # Si on a décodé une chaine non vide, on ajoute les caractères au dico
                    t = t +1
            car_lu = car_decode
            #print(nombre,car_decode,s,t,car_decode)    
                
        print(self.__dico_dec)                 
            
        return s 

    
    def decompression_lzw_2(self,code):
        s = ''
        chaine = ""
        liste_code=code.split()
        t = len(self.__dico_dec)
        for nombre in liste_code :
            
            if int(nombre) not in self.__dico_dec :
                self.__dico_dec[int(nombre)] = chaine + chaine[0]
            car_decode = self.__dico_dec[int(nombre)]
            s = s + car_decode
            if len(chaine) !=0 :
                self.__dico_dec[t] = chaine + car_decode[0]
                t = t +1
            chaine = car_decode
        return s
           

    def compresser(self):
        with open(self.f,'r',encoding='utf-8') as file :
            with open(self.f+"_compresse",'w',encoding='utf-8') as nfile :
                chaine = file.readline().strip('\n')
                
                s = ""
                while chaine !='' :
                    c = self.compression_lzw_1(chaine)
                    nfile.write(c+ '\n')
                    chaine = file.readline().strip('\n')
                
                
               
                    

    def decompresser(self):
        with open(self.f,'r',encoding='utf-8') as file :
            with open(self.f+"_decompresse",'w',encoding='utf-8') as nfile :
                chaine = file.readline().strip('\n')
                print(chaine)
                s = ""
                while chaine !="" :
                    c = self.decompression_lzw_2(chaine)
                    nfile.write(c + '\n')
                    chaine = file.readline().strip('\n')
               
                
                
                    

if __name__=='__main__' :
       
    t = Text("fichier_mael_compresse.txt_dechiffre")
    t.decompresser()
    #tc = Text("texte_ref.txt_compresse")
    #tc.decompresser()

    #rint(t.compression_lzw_1("0100011101110110110001011110011010010011000110110001100001001000000"))
    #print(t.compression_lzw_1("abracadabra"))
    #a = Text("test.txt_compresse")
    #s = t.decompression_lzw_1('2 0 2 1 0 0 2 3')
    print(s)
    #print("0100011101110110110001011110011010010011000110110001100001001000000")
    #a.decompresser()
    import sys
    #print(sys.getsizeof("Les_Miserables.txt"))
    #print(sys.getsizeof("Les_Miserables.txt_compresse"))
    #print(t.get_dic())

