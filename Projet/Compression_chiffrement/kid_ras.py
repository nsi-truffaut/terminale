from random import *


def inverse_mod_n(d,n) :
    """
    Fonction qui permet d'inverser d modulo n
    L'objectif est de déterminer la clé privée à partir de la clé publique
    Faiblesse de kid_rsa
    """
    v = 1
    while (1-v*n)%d != 0 :
        v = v+1
    return (1-v*n)//d + n

class KidRSA :
    """
    Classe qui permet de générer 4 nombres aléatoires afin de déterminer les clés
    publiques et privées.
    """
    def __init__(self):
        self.__n_secret = [randint(0,10**9) for _ in range(4)]

    def get_secret(self):
        return self.__n_secret

    def public_key(self):
        a,b,c,d = self.__n_secret
        M = a*b-1
        e = M*c + a
        d1 = d*M + b
        n = (e*d1-1) // M
        
        return e,n

    def private_key(self):
        a,b,c,d = self.__n_secret
        M = a*b-1
        e = M*c + a
        d1 = d*M + b
        n = (e*d1-1) // M
        return d1,n
        

def code(nombre,cle_pub):
        e,n = cle_pub
        return nombre*e%n

def decode(nombre,cle_priv):
        d,n = cle_priv
        return nombre*d%n
    
# Fonction qui permet de chiffrer un texte ligne par ligne
# A condition que chaque caractère soit transformé en nombre décimal
# code ASCII
# ou codage d'un fichier compressé par LZW

def chiffrer(fichier,cle_pub):
    
    with open(fichier,'r',encoding='utf-8') as f :
        with open(fichier + "_chiffre",'w',encoding='utf-8') as nf :
            ligne =f.readline().strip().split(' ')
            while ligne != ['']:
                for nombre in ligne :
                    nf.write(str(code(int(nombre),cle_pub))+' ')
                nf.write('\n')
                ligne =f.readline().strip().split(' ')
    

def dechiffrer(fichier,cle_priv) :
    with open(fichier,'r',encoding='utf-8') as f :
        with open(fichier + "_dechiffre",'w',encoding='utf-8') as nf :
            ligne =f.readline().strip().split(' ')
            
            while ligne != ['']:
                
                for nombre in ligne :
                    print(nombre,str(decode(int(nombre),cle_priv)))
                    nf.write(str(decode(int(nombre),cle_priv))+' ')
                nf.write('\n')
                ligne =f.readline().strip().split(' ')
                    
                    
