#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : Crypatge clef de Cesar
#   auteur: Quentin LUCAS
#     date: 2021/09/15

# Programme

def clef_de_cesar(phrase,clef):
    """ permet de crypter une phrase en majuscule
    >>> clef_de_cesar("ABCD",1)
    BCDE
    >>> clef_de_cesar("WXY",2)
    ABC
    """
    lettre = ("A", "B","C", "D", "E","F", "G", "H","I","J", "K", "L", "M", "N", "O", "P", "Q","R","S","T", "U", "V", "W", "X", "Y", "Z", " ")
    phraseC = ""
    for i in range(len(phrase)-1):
        j = 0
        while phrase[i] != lettre[j] and j <= 26:
            j = j + 1
        if lettre[j] == " ":
            phraseC = phraseC + phrase[i]
        phraseC = phraseC + lettre[(j+clef) % 25]
    return phraseC
            
def decclef_de_cesar(phrase,clef):
    """permet de decrypter une phrase en majuscule
    >>> decclef_de_cesar("BCDE,1")
    ABCD
    >>> decclef_de_cesar("ABC,2")
    WXY
    """
    lettre = ("A", "C", "D", "E","F", "G", "H","I","J", "K", "L", "M", "N", "O", "P", "Q","R","S","T", "U", "V", "W", "X", "Y", "Z", " ")
    phraseDC = ""
    for i in range(len(phrase)-1):
        j = 0
        while phrase[i] != lettre[j] and j <= 26:
            j = j + 1
        if lettre[j] == " ":
            phraseDC = phraseDC + phrase[i]
        phraseDC = phraseDC + lettre[abs(j-clef) % 25]
            
