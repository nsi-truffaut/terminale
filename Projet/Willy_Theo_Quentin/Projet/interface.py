#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : interface.py
#   auteur: Willy PENAUD
#     date: 2021/12/04

#importation
from Crypatge_clef_de_Cesar import *
from test2 import *
from tkinter import *

#fct.get

##vigenère
def vencode(phrase,code):
    return ap(phrase,code)
def vdecode(phrase,code):
    return zp(phrase,code)
##César
def cencode(phrase,code):
    return clef_de_cesar(phrase,code)
def cdecode(phrase,code):
    return decclef_de_cesar(phrase,code)
    
#fonctions
def getTextInput():
    global phrase
    global code
    if variable.get() == 'César':
        phrase= Phrase.get("1.0","end")
        code = Code.get("1.0","end")
    else :
        phrase= Phrase.get("1.0","end")
        code= Code.get("1.0","end")
        
def encode():
    getTextInput()
    if variable.get() == 'César':
        text.set(cencode(phrase,code))
    else :
        text.set(vencode(phrase,code))
        
def decode():
    getTextInput()
    if variable.get() == 'César':
        text.set(cdecode(phrase,code))
    else :
        text.set(vdecode(phrase,code))

#tkinter
app = Tk()
app.geometry('1366x768')

##Variable
text = StringVar()
text.set("")

OptionList = [
"César",
"Vigenère"
]
t = StringVar()
t.set("Texte")
phrase = ""

c = StringVar()
c.set("Code")
code = 0
##menu déroulant
variable = StringVar(app)
variable.set(OptionList[0])

opt = OptionMenu(app, variable, *OptionList)
opt.config(width=90, font=('Helvetica', 12))
opt.pack()

##Texte

Phrase_ind = Label(textvariable=t)
Phrase = Text(app, height=10)
Code_ind = Label(textvariable=c)
Code = Text(app, height=1, width=20)

Resultat = Label(textvariable=text,padx = 100)

zone1 = Frame(app)
encoder = Button(zone1 , text="encoder", command=encode)
decoder = Button(zone1 , text="decoder", command=decode)

Phrase_ind.pack()
Phrase.pack()
Code_ind.pack()
Code.pack()

zone1.pack(fill=Y,padx = 10, pady = 10)
encoder.pack(side = LEFT, fill=X,ipady=30,padx=10,pady=10)
decoder.pack(side = RIGHT, fill=X,ipady=30,padx=10,pady=10)

Resultat.pack()

app.mainloop()
