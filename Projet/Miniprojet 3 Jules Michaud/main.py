#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : main.py
#  auteur : Jules MICHAUD
#    date : 2023-02-23

from automate import *

#def main():
taille = int(input("Quelle longueur de grille souhaitez-vous ? Il faut qu'elle soit supérieure à 5 :  "))
iteration = int(input("Combien d'évolution souhaitez-vous effectuer ?  "))
grille = Grille(taille)
grille =  int(input("Quelle configuration souhaitez-vous utiliser ? 1, 2 :  "))
if grille == 1:
	with open ("conf1.csv","r",encoding="utf-8") as fichier:
		grille = fichier.readline().strip().split(";")
elif grille == 2:
	with open ("conf2.casv","r",encoding="utf-8") as fichier:
		grille = fichier.readline().strip().split(";")
start = Jeu.evolution(grille,iteration)
