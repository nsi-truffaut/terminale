#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : automate.py
#  auteur : Gregory MAUPU & Jules MICHAUD
#    date : 2023-02-23


# Importation des modules

import os
import time

class Cellule :

    def __init__(self, etat):
        self.etat = 0

    def __repr__(self):
        if self.etat == 0:
            return self.mort()
        else:
            return self.vivant()
			        
    def vivant(self):
        return "&"

    def mort(self):
        return " "
    

        
class Grille :

    def __init__(self,taille):
        self.taille = taille
        self.grille = []
        for i in range(taille):
            self.grille.append([])
            for j in range(taille):
                self.grille[i].append(Cellule(0))

    def get_cell(self,i,j):
        assert i < self.taille and j < self.taille, "la demande n'entre pas dans la taille de la grille"
        return self.grille[i][j]

    def __repr__(self):
        return self.grille

    def __len__(self):
        return self.taille

    

class Jeu :

    def __init__(self,grille) :
        self.grille = grille
        self.n = len(grille)
        
    def update(self): # à chaque fois qu je veux faire "self.get_cell(i,j) = self.vivant()" cela me renvoyais une erreur j'ai du le modifier mais je ne sais pas si ça marche quand même
        for i in range(self.n):
            for j in range (self.n):
                if i == 0 and j == 0: # si la cellule est en haut à gauche de la grille
                    if self.etat.get_cell(i,j) == 0:
                        if self.etat.get_cell(self.__len__(),self.__len__()) + self.etat.get_cell(self.__len__(),j) + self.etat.get_cell(self.__len__(),j+1) + self.etat.get_cell(i,j+1) + self.etat.get_cell(i+1,j+1) + self.etat.get_cell(i+1,j) + self.etat.get_cell(i+1,self.__len__()) + self.etat.get_cell(i,self.__len__()) == 3 :
                            self.get_cell(i,j) == self.vivant()
                    else:
                        if self.etat.get_cell(self.__len__(),self.__len__()) + self.etat.get_cell(self.__len__(),j) + self.etat.get_cell(self.__len__(),j+1) + self.etat.get_cell(i,j+1) + self.etat.get_cell(i+1,j+1) + self.etat.get_cell(i+1,j) + self.etat.get_cell(i+1,self.__len__()) + self.etat.get_cell(i,self.__len__()) == 2 or 3:
                            self.get_cell(i,j) == self.mort()
                elif i == self.__len__() and j == 0: # si la cellule est en haut à droite de la grille
                    if self.etat.get_cell(i,j) == 0:
                        if self.etat.get_cell(i-1,self.__len__()) + self.etat.get_cell(i-1,j) + self.etat.get_cell(i-1,j+1) + self.etat.get_cell(i,j+1) + self.etat.get_cell(0,j+1) + self.etat.get_cell(0,j) + self.etat.get_cell(0,self.__len__()) + self.etat.get_cell(i,self.__len__()) == 3:
                            self.get_cell(i,j) == self.vivant()
                    else:
                        if self.etat.get_cell(i-1,self.__len__()) + self.etat.get_cell(i-1,j) + self.etat.get_cell(i-1,j+1) + self.etat.get_cell(i,j+1) + self.etat.get_cell(0,j+1) + self.etat.get_cell(0,j) + self.etat.get_cell(0,self.__len__()) + self.etat.get_cell(i,self.__len__()) == 2 or 3:
                            self.get_cell(i,j) == self.mort()
                elif i == 0 and j == self.__len__(): # si la cellule est en bas à gauche de la grille
                    if self.etat.get_cell(i,j) == 0:
                        if self.etat.get_cell(self.__len__(),j-1) + self.etat.get_cell(self.__len__(),j) + self.etat.get_cell(self.__len__(),0) + self.etat.get_cell(i,0) + self.etat.get_cell(i+1,0) + self.etat.get_cell(i+1,j) + self.etat.get_cell(i+1,j-1) + self.etat.get_cell(i,j-1) == 3:
                            self.get_cell(i,j) == self.vivant()
                    else:
                        if self.etat.get_cell(self.__len__(),j-1) + self.etat.get_cell(self.__len__(),j) + self.etat.get_cell(self.__len__(),0) + self.etat.get_cell(i,0) + self.etat.get_cell(i+1,0) + self.etat.get_cell(i+1,j) + self.etat.get_cell(i+1,j-1) + self.etat.get_cell(i,j-1) == 2 or 3:
                            self.get_cell(i,j) == self.mort()
                elif i == self.__len__() and j == self.__len__(): # si la cellule est en haut à droite de la grille
                    if self.etat.get_cell(i,j) == 0:
                        if self.etat.get_cell(i-1,j-1) + self.etat.get_cell(i-1,j) + self.etat.get_cell(i-1,0) + self.etat.get_cell(i,0) + self.etat.get_cell(0,0) + self.etat.get_cell(0,j) + self.etat.get_cell(0,j-1) + self.etat.get_cell(i,j-1) == 3:
                            self.get_cell(i,j) == self.vivant()
                    else:
                        if self.etat.get_cell(i-1,j-1) + self.etat.get_cell(i-1,j) + self.etat.get_cell(i-1,0) + self.etat.get_cell(i,0) + self.etat.get_cell(0,0) + self.etat.get_cell(0,j) + self.etat.get_cell(0,j-1) + self.etat.get_cell(i,j-1) == 2 or 3:
                            self.get_cell(i,j) == self.mort()
                elif i == 0: # si la cellule se trouve sur le bord gauche de la grille
                    if self.etat.get_cell(i,j) == 0:
                        if self.etat.get_cell(self.__len__(),j-1) + self.etat.get_cell(self.__len__(),j) + self.etat.get_cell(self.__len__(),j+1) + self.etat.get_cell(i,j+1) + self.etat.get_cell(i+1,j+1) + self.etat.get_cell(i+1,j) + self.etat.get_cell(i+1,j-1) + self.etat.get_cell(i,j-1) == 3:
                            self.get_cell(i,j) == self.vivant()
                    else:
                        if self.etat.get_cell(self.__len__(),j-1) + self.etat.get_cell(self.__len__(),j) + self.etat.get_cell(self.__len__(),j+1) + self.etat.get_cell(i,j+1) + self.etat.get_cell(i+1,j+1) + self.etat.get_cell(i+1,j) + self.etat.get_cell(i+1,j-1) + self.etat.get_cell(i,j-1) == 2 or 3:
                            self.get_cell(i,j) == self.mort()
                elif i == self.__len__(): # si la cellule se trouve sur le bord droit de la grille
                    if self.etat.get_cell(i,j) == 0:
                        if self.etat.get_cell(i-1,j-1) + self.etat.get_cell(i-1,j) + self.etat.get_cell(i-1,j+1) + self.etat.get_cell(i,j+1) + self.etat.get_cell(0,j+1) + self.etat.get_cell(0,j) + self.etat.get_cell(0,j-1) + self.etat.get_cell(i,j-1) == 3:
                            self.get_cell(i,j) == self.vivant()
                    else:
                        if self.etat.get_cell(i-1,j-1) + self.etat.get_cell(i-1,j) + self.etat.get_cell(i-1,j+1) + self.etat.get_cell(i,j+1) + self.etat.get_cell(0,j+1) + self.etat.get_cell(0,j) + self.etat.get_cell(0,j-1) + self.etat.get_cell(i,j-1) == 2 or 3:
                            self.get_cell(i,j) == self.mort()
                elif j == 0: # si la cellule se trouve sur le bord haut de la grille
                    if self.etat.get_cell(i,j) == 0:
                        if self.etat.get_cell(i-1,self.__len__()) + self.etat.get_cell(i-1,j) + self.etat.get_cell(i-1,j+1) + self.etat.get_cell(i,j+1) + self.etat.get_cell(i+1,j+1) + self.etat.get_cell(i+1,j) + self.etat.get_cell(i+1,self.__len__()) + self.etat.get_cell(i,self.__len__()) == 3:
                            self.get_cell(i,j) == self.vivant()
                    else:
                        if self.etat.get_cell(i-1,self.__len__()) + self.etat.get_cell(i-1,j) + self.etat.get_cell(i-1,j+1) + self.etat.get_cell(i,j+1) + self.etat.get_cell(i+1,j+1) + self.etat.get_cell(i+1,j) + self.etat.get_cell(i+1,self.__len__()) + self.etat.get_cell(i,self.__len__()) == 2 or 3:
                            self.get_cell(i,j) == self.mort()
                elif j == self.__len__(): # si la cellule se trouve sur le bord bas de la grille
                    if self.etat.get_cell(i,j) == 0:
                        if self.etat.get_cell(i-1,j-1) + self.etat.get_cell(i-1,j) + self.etat.get_cell(i-1,0) + self.etat.get_cell(i,0) + self.etat.get_cell(i+1,0) + self.etat.get_cell(i+1,j) + self.etat.get_cell(i+1,j-1) + self.etat.get_cell(i,j-1) == 3:
                            self.get_cell(i,j) == self.vivant()
                    else:
                        if self.etat.get_cell(i-1,j-1) + self.etat.get_cell(i-1,j) + self.etat.get_cell(i-1,0) + self.etat.get_cell(i,0) + self.etat.get_cell(i+1,0) + self.etat.get_cell(i+1,j) + self.etat.get_cell(i+1,j-1) + self.etat.get_cell(i,j-1) == 2 or 3:
                            self.get_cell(i,j) == self.mort()
                else :
                    if self.etat.get_cell(i,j) == 0: # cas "normal" quand une cellule se trouve au milieu de la grille
                        if self.etat.get_cell(i-1,j-1) + self.etat.get_cell(i-1,j) + self.etat.get_cell(i-1,j+1) + self.etat.get_cell(i,j+1) + self.etat.get_cell(i+1,j+1) + self.etat.get_cell(i+1,j) + self.etat.get_cell(i+1,j-1) + self.etat.get_cell(i,j-1) == 3:
                            self.get_cell(i,j) == self.vivant()
                    else:
                        if self.etat.get_cell(i-1,j-1) + self.etat.get_cell(i-1,j) + self.etat.get_cell(i-1,j+1) + self.etat.get_cell(i,j+1) + self.etat.get_cell(i+1,j+1) + self.etat.get_cell(i+1,j) + self.etat.get_cell(i+1,j-1) + self.etat.get_cell(i,j-1) == 2 or 3:
                            self.get_cell(i,j) == self.mort()
        for i in range(self.n): # on change les etats après avoir modifié toute la grille pour ne pas influer sur les autres
            for j in range (self.n):
                if self.get_cell == self.vivant():
                    self.etat.get_cell(i,j) == 1
                else:
                    self.etat.get_cell(i,j) == 0

    def evolution(self,temps):
        k = 0
        while k < temps :
            self.update() #je ne trouve pas l'erreur ici
            time.sleep(0.1) # Permet de mettre en pause après la mise à jour des cellules, ici 0,1s
            os.system("clear") # Efface la console avant la prochaine mise à jour
            k = k+1

if __name__=="__main__" :
    g = Grille(10)

    # Vous proposerez un test, issue des exemples 1 ou 2

# fin du fichier automate
