#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : automate_a_completer.py
#  auteur : Gregory MAUPU & Noa CHUPIN
#    date : 2023/02/09


# Importation des modules

import os
import time


class Cell :

    def __init__(self):
        self.e = 0
    
        
    def vivant(self):
        return self.e == 1

    def mort(self):
        return self.e == 0

    def __repr__(self):
        if self.e == 1:
            return "*"
        else:
            return ''
       
        
class Grille :

    def __init__(self,taille):
        self.n = taille
        self.g = []
        for k in range(self.n+1):
            self.g.append([])
        for a in range(self.n+1):
            for b in range(self.n+1):
                self.g[a].append([Cell()])
        

    def get_cell(self,i,j):
        a = self.g[i][j][0]
         
        return a

    def __len__(self):
        return self.n

    def affichage(self):
        for c in range(self.n):
            print(self.g[c])
        
    
    def __repr__(self):
        return "{}".format(self.affichage())
        
                

        
class Jeu :

    def __init__(self,g) :
        self.n = g.__len__()
        self.g = Grille(self.n)
        
    def allentour(self,g,i,j):
        compteur = 0
        if g.get_cell(i-1,j-1).e == 1:
            compteur += 1
        if g.get_cell(i,j-1).e == 1 :
            compteur += 1    
        if g.get_cell(i+1,j-1).e == 1 :
            compteur += 1
        if g.get_cell(i-1,j).e == 1 :
            compteur += 1
        if g.get_cell(i+1,j).e == 1 :
            compteur += 1
        if g.get_cell(i-1,j+1).e == 1 :
            compteur += 1
        if g.get_cell(i,j+1).e == 1 :
            compteur += 1
        if g.get_cell(i+1,j+1).e == 1 :
            compteur += 1
        return compteur
        
    def update(self,g):
        
        for i in range(self.n):
            for j in range(self.n):
                if g.get_cell(i,j).e == 0 and Jeu(g).allentour(g,i,j) == 3:
                    g.get_cell(i,j).e = 1
                if Jeu(g).allentour(g,i,j) < 2 or Jeu(g).allentour(g,i,j) > 3:
                    g.get_cell(i,j).e = 0
                
                    
        return g

def evolution(self,temps):
        
        k = 0
        while k < temps :
            Jeu(self).update(self)
            print(self)
            time.sleep(1) # Permet de mettre en pause après la mise à jour des cellules, ici 0,1s
            os.system("clear") # Efface la console avant la prochaine mise à jour
            k = k+1                
                        
                    
                    
    
        

if __name__ == "__main__":

    g = Grille(5)
    g.get_cell(2,3).e = 1
    #print(g)
    #Jeu(g).update(g)
    #print(g)
    #evolution(g,5)
    
    
    
    
    

    # Vous proposerez un test, issue des exemples 1 ou 2

# fin du fichier automate_a_completer
