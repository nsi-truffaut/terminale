#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : automate_a_completer.py
#  auteur : Gregory MAUPU & Jules HERBIN
#    date : 18/02/2023


# Importation des modules

import os
import time
import copy

class Cell :

    def __init__(self):
        self.etat = 0

    def __repr__(self):
        if self.etat == 0:
            return(".")
        else:
            return("x")
        
    def vivant(self):
        self.etat = 1

    def mort(self):
        self.etat = 0
        
class Grille :

    def __init__(self,taille):
        self.taille = taille
        self.grille = []
        for i in range(taille):
            self.grille.append([Cell() for y in range(taille)])


    def get_cell(self,i,j):
        return self.grille[i][j]

    def __repr__(self):
        s = ""
        for i in range(self.taille):
            for j in range(self.taille):
                s = s + str(self.get_cell(i,j)) + " "
            s = s + "\n"
        return s

    def __len__(self):
        return self.taille

class Jeu :

    def __init__(self,grille) :
        self.grille = grille
        self.n = grille.__len__()


    def update(self):
        grille_updt = copy.deepcopy(self.grille)
        for i in range(self.n):
            for j in range(self.n):
                voisins = []
                for x in [i-1, i, i+1]:
                    if x >= 0 and x < self.n:
                        for y in [j-1, j, j+1]:
                            if y >= 0 and y < self.n and (x,y) != (i,j):
                                voisins = voisins + [self.grille.get_cell(x,y).etat]

                vivants = 0
                for m in range(len(voisins)):
                    if voisins[m] == 1:
                        vivants = vivants + 1
                if self.grille.get_cell(i,j).etat == 0 and vivants == 3:
                    grille_updt.get_cell(i,j).vivant()
                elif self.grille.get_cell(i,j).etat == 1 and (vivants < 2 or vivants > 3):
                    grille_updt.get_cell(i,j).mort()

        self.grille = copy.deepcopy(grille_updt)
        print(self.grille)


    def evolution(self,temps):
        k = 0
        while k < temps :
            self.update()
            time.sleep(0.1) # Permet de mettre en pause après la mise à jour des cellules, ici 0,1s
            os.system("clear") # Efface la console avant la prochaine mise à jour
            k = k+1

if __name__=="__main__" :
    g = Grille(10)
    g.get_cell(5,0).vivant()
    g.get_cell(5,1).vivant()
    g.get_cell(5,2).vivant()
    g.get_cell(6,2).vivant()
    g.get_cell(7,1).vivant()
    print(g)
    j = Jeu(g)
    j.evolution(2)
    print(j.grille)

# fin du fichier automate_a_completer
