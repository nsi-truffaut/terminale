#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier :  main.py
# auteur  : Jules Herbin
# date    : 18/02/2023

import automate_a_completer

def main():
    taille = int(input("Entrez la taille de la grille : "))
    grille = automate_a_completer.Grille(taille)

    print("Configurations possibles : \n-Planeur\n-Mathusalem")
    configuration = input("choisissez une configuration : ")
    assert configuration == "Planeur" or configuration == "Mathusalem", "Cela ne figure pas dans les configurations possibles"
    with open(configuration+".csv","r",encoding="utf-8") as fichier:
        liste = fichier.readline().strip().split(";")
        i = 0
        while liste != [""]:
            assert taille >= len(liste), "Il faut une taille au moins égale à " + str(len(liste))
            for y in range(len(liste)):
                if liste[y] == '1':
                    grille.get_cell(i + taille//3 - 1,y + taille//3 - 1).vivant()
            liste = fichier.readline().strip().split(";")
            i = i + 1

    iteration = int(input("Entrez le nombre d'itérations : "))
    jeu = automate_a_completer.Jeu(grille)
    jeu.evolution(iteration)
    print(jeu.grille)

if __name__=="__main__":
    main()

# fin du fichier main.py
