#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : pendu.py
#  auteurs : HERMIER Maxime, BAUDOUR Malcom, NAUD Ilona
#    date : 05/12/2021

from random import*

#Fonction qui permet d'importer la liste de mots et d'afficher le nombre de tirets correctement avec espaces.

def fonction1():
    listemots=[] #Liste vide dans laquelle on ajoutera tout les mots de notre dictionnaire.
    with open('dictionnaire.txt','r') as f :
        for ligne in f :
            ligne = ligne.replace("\n","") #Retirer les retours a la ligne.
            listemots.append(ligne) #Ajouter tout les mots du dictionnaire dans la liste (listemots).
    mot=list(choice(listemots))
    tirets=len(mot)
    mot_cache = tirets * ["_"]
    for i in mot_cache:
        print(i,"",end="")
    return mot,mot_cache


#dessin pendu

pendu_dessin=[
    '''
        ____________________
        |                  \\|
        |                   |
        |                   |
        |
        |
        |
        |
        |
        |
        |
        |_____________________
    ''',
    '''
        ____________________
        |                  \\|
        |                   |
        |                   |
        |                   O
        |
        |
        |
        |
        |
        |
        |_____________________
    ''',
    '''
        ____________________
        |                  \\|
        |                   |
        |                   |
        |                   O
        |                   |
        |                   |
        |
        |
        |
        |
        |_____________________
    ''',
    '''
        ____________________
        |                  \\|
        |                   |
        |                   |
        |                   O
        |                  /|
        |                   |
        |
        |
        |
        |
        |_____________________
    ''',
    '''
        ____________________
        |                  \\|
        |                   |
        |                   |
        |                   O
        |                  /|\
        |                   |
        |
        |
        |
        |
        |_____________________
    ''',
    '''
        ____________________
        |                  \\|
        |                   |
        |                   |
        |                   O
        |                  /|\
        |                   |
        |                  /
        |
        |
        |
        |_____________________

    ''',
    '''
        ____________________
        |                  \\|
        |                   |
        |                   |
        |                   O
        |                  /|\
        |                   |
        |                  / \
        |
        |
        |
        |_____________________
    '''
]

#Programme de jeu principal

def pendu(): 
    print("Bienvenue dans le jeu du pendu, vous avez 6 essais ! Bonne chance !")
    print(pendu_dessin[0])
    mot,mot_cache=fonction1()
    vie=0
    j=0
    Deja_joue=[]
    lettre=input("Entrez une lettre : ")
    while mot!=mot_cache and vie!=6:
        if lettre in mot:
            if lettre in Deja_joue:
                print (pendu_dessin[j])
                print("Lettre déjà jouée !")
                print("Lettres déjà jouées : ")
                for i in (Deja_joue):
                    print(i,"",end="")
                print()
                lettre=input("Entrez une lettre : ")
            else:
                for i in range(len(mot_cache)):
                    if lettre==mot[i]:
                        mot_cache[i]=lettre
                print (pendu_dessin[j])
                for i in mot_cache:
                    print(i,"",end="")
                Deja_joue=Deja_joue+list(lettre)
                print()
                print("Lettres déjà jouées : ")
                for i in (Deja_joue):
                    print(i,"",end="")
                print()
                if mot!=mot_cache and vie!=6:
                    lettre=input("Entrez une lettre : ")
        else:
            if lettre in Deja_joue:
                print(pendu_dessin[j])
                print("Lettre déjà jouée !")
                for i in mot_cache:
                    print(i,"",end="")
                print()
                print("Lettres déjà jouées : ")
                for i in (Deja_joue):
                    print(i,"",end="")
                print()
                lettre=input("Entrez une lettre : ")
            else:
                vie=vie+1
                j=j+1
                print(pendu_dessin[j])
                print("Recommence!")
                for i in (mot_cache):
                    print(i,"",end="")
                print()
                Deja_joue=Deja_joue+list(lettre)
                print("Lettres déjà jouées : ")
                for i in (Deja_joue):
                    print(i,"",end="")
                print()
                if mot!=mot_cache and vie!=6:
                    lettre=input("Entrez une lettre : ")
    if mot==mot_cache:
        print(pendu_dessin[j])
        print("Bien joué ! ")
    else:
        print(pendu_dessin[6])
        print("Perdu ! ")
        print("le mot était : ")
        for i in mot:
            print(i,"",end="")

# fin du fichier pendu.py