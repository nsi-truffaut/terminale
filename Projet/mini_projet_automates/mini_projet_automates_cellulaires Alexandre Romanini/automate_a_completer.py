#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : automate_a_completer.py
#  auteur : Gregory MAUPU & Alexandre ROMANINI
#    date : 16/02/2023


# Importation des modules

import os
import time

class Cell :

    def __init__(self):
        self.e = 0

    def __repr__(self):
        """
        Cette fonction permet d'afficher le charactère "O" lorsque la cellule et vivante et un vide " " lorsqu'elle est morte
        """
        cellule = ""
        if self.e == 0:
            cellule = " "
        else:
            cellule = "O"
        return cellule
            
    def vivant(self):
        self.e = 1

    def mort(self):
        self.e = 0
        
class Grille :

    def __init__(self,taille):
        self.t = taille
        self.grille = [[Cell() for i in range(taille)] for j in range(taille)]              # On crée un liste de sous-listes de cellules mortes pour former la grille

    def get_cell(self,i,j):
        return self.grille[i][j]

    def __repr__(self):
        """
        Cette fonction permet d'afficher la grille,
        le charactère "|" petmet d'encadrer et de délimiter la taille de celle-ci.
        """
        affichage = ""
        for i in range(self.t):
            affichage = affichage + '|'
            for j in range(self.t):
                if self.get_cell(i, j).e == 0:
                    affichage = affichage + ' '
                else:
                    affichage = affichage + 'O'
                if j != self.t - 1:
                    affichage = affichage + ' '
            affichage = affichage + '|\n'
        return affichage

    def __len__(self):
        return self.t
    

class Jeu :

    def __init__(self, grille) :
        self.grille = grille
        self.n = len(grille)

    def update(self):
        """
        Cette fonction permet de faire évoluer la grille une fois.
        """
        new_grille = Grille(self.n)

        for ligne in range(self.n):
            for colonne in range(self.n):
                voisin_vivant = 0                                           # cette variable va permettre de compter les voisins vivants de la cellule en question

                if ligne == 0 and colonne == 0:                             # Test pour côin haut/gauche
                    if self.grille.get_cell(ligne + 1, colonne).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne + 1, colonne + 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne, colonne + 1).e == 1:
                        voisin_vivant = voisin_vivant + 1

                elif ligne == 0 and colonne == self.n - 1:                  # Test pour côin haut/droit
                    if self.grille.get_cell(ligne + 1, colonne).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne + 1, colonne - 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne, colonne - 1).e == 1:
                        voisin_vivant = voisin_vivant + 1

                elif ligne == self.n - 1 and colonne == 0:                  # Test pour côin bas/gauche
                    if self.grille.get_cell(ligne - 1, colonne).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne, colonne + 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne - 1, colonne + 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
            
                elif ligne == self.n - 1 and colonne == self.n - 1:         # Test pour côin bas/droit
                    if self.grille.get_cell(ligne, colonne - 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne - 1, colonne).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne - 1, colonne - 1).e == 1:
                        voisin_vivant = voisin_vivant + 1

                elif colonne - 1 < 0:                                       # Test pour côté gauche
                    if self.grille.get_cell(ligne - 1, colonne).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne - 1, colonne + 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne, colonne + 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne + 1, colonne + 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne + 1, colonne).e == 1:
                        voisin_vivant = voisin_vivant + 1

                elif colonne + 1 > self.n - 1:                              # Test pour côté droit
                    if self.grille.get_cell(ligne - 1, colonne).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne - 1, colonne - 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne, colonne - 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne + 1, colonne - 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne + 1, colonne).e == 1:
                        voisin_vivant = voisin_vivant + 1

                elif ligne + 1 > self.n - 1:                                # Test pour côté inferieur (en bas)
                    if self.grille.get_cell(ligne, colonne + 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne - 1, colonne + 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne - 1, colonne).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne - 1, colonne - 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne, colonne - 1).e == 1:
                        voisin_vivant = voisin_vivant + 1

                elif ligne - 1 < 0:                                         # Test pour côté supérieur (en haut)
                    if self.grille.get_cell(ligne, colonne + 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne + 1, colonne + 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne + 1, colonne).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne + 1, colonne - 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne, colonne - 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                else:                                                       # Test pour le cas général (ni sur un des côtés, ni dans un des coins)
                    if self.grille.get_cell(ligne, colonne + 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne + 1, colonne + 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne + 1, colonne).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne + 1, colonne - 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne, colonne - 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne - 1, colonne - 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne - 1, colonne + 1).e == 1:
                        voisin_vivant = voisin_vivant + 1
                    if self.grille.get_cell(ligne - 1, colonne).e == 1:
                        voisin_vivant = voisin_vivant + 1
          
                if self.grille.get_cell(ligne ,colonne).e == 1 and (voisin_vivant < 2 or voisin_vivant > 3):        # Teste si la cellule est vivante, et si elle a moins de 2 ou plus de 3 voisins vivants, si c'est le cas, la cellule meurt
                        new_grille.get_cell(ligne, colonne).mort() 
                    
                elif self.grille.get_cell(ligne,colonne).e == 0 and voisin_vivant == 3:                             # Teste si la cellule est morte et si elle a exactement 3 voisines vivantes, si c'est le cas, elle devient vivante
                        new_grille.get_cell(ligne, colonne).vivant()

                elif self.grille.get_cell(ligne ,colonne).e == 1 and (voisin_vivant == 2 or voisin_vivant == 3):    # Teste si la cellule est vivante et si elle a exactement 2 ou 3 voisines vivantes, si c'est le cas, elle reste en vie
                        new_grille.get_cell(ligne, colonne).vivant()
                    

        self.grille = new_grille
        
                                  
    def evolution(self,temps):
        """
        Cette fonction repète "temps" fois la fonction update et permet de la faire évoluer en temps réel
        """
        k = 0
        while k < temps :
            self.update()
            print(self.grille)
            time.sleep(0.1) # Permet de mettre en pause après la mise à jour des cellules, ici 0,1s
            os.system("clear") # Efface la console avant la prochaine mise à jour
            k = k+1

if __name__ == "__main__" :

    # exemple 1 + test
    
    g = Grille(30)                  # On crée une grille de taille 30
    g.get_cell(10, 10).vivant()     # On rend vivante les cellules qu'on veut
    g.get_cell(10, 11).vivant()
    g.get_cell(10, 12).vivant()
    g.get_cell(11, 12).vivant()
    g.get_cell(12, 11).vivant()
    print(g)                        # On affiche l'état initial de la grille avec les cellules vivantes
    J = Jeu(g)                      # On crée le jeu avec la grille créé précédemment
    J.evolution(10)                 # On lance le jeu de la vie avec 10 évolutions
    

    # exemple 2 (attention agrandir la console d'execution car sinon l'affichage est buggé à cause de la grande taille de la grille)
    """
    g2 = Grille(43)
    g2.get_cell(15, 15).vivant()
    g2.get_cell(16, 15).vivant()
    g2.get_cell(17, 15).vivant()
    g2.get_cell(18, 15).vivant()
    g2.get_cell(16, 16).vivant()
    g2.get_cell(16, 17).vivant()
    g2.get_cell(17, 16).vivant()
    print(g2)
    J2 = Jeu(g2)
    J2.evolution(20)
    """

# fin du fichier automate_a_completer
