#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : main.py
#  auteur : Alexandre ROMANINI
#    date : 16/02/2023


# Importation des modules

from automate_a_completer import *
import csv


def trouver_coordonnees_de_1(liste):
    """
    Cette fonction permet de trouver les coordonnées des '1' dans une liste de listes de '0' et de '1' et nous sert dans la fonction main().
    """
    coordonnees = []
    for i in range(len(liste)):
        for j in range(len(liste[i])):
            if liste[i][j] == 1:
                coordonnees.append((i, j))
    return coordonnees

    
def main():

    Choix = input("Bonjour ! Voulez-vous personnaliser votre propre automate cellulaire ou bien choisir entre plusieurs configurations de départ ? \n Entrez 1 : pour personnaliser \n Entrez 2 : pour choisir une des configurations par défaut \n")

    if Choix == "1":
        taille = input("Donnez la taille de la grille que vous souhaitez : ")
        assert int(taille) >= 5, "La grille ne peut pas être de taille inferieur à 5"
        liste_init = []
        coors = []
        print("Ne rentrez pas de coordonnées de valeurs superieures ou égales à la taille choisit précédemment")
        while True:
            entree = input("Entrez les coordonnées x,y d'un point (ou 'fin' pour terminer) : ")
            
            if entree == "fin":
                break
            x, y = entree.split(",")
            coor = (int(x), int(y))
            coors.append(coor)
            
        iteration = input("Pour finir, donnez le nombre d'étapes que vous souhaitez que votre automate cellulaire exécute : ")
        g = Grille(int(taille))
        
        for i in range(len(coors)):
            g.get_cell(coors[i][0], coors[i][1]).vivant()
            
        print(g)
        J = Jeu(g)
        J.evolution(int(iteration))

        
    
    elif Choix == "2":
        
        nom_config = ""
        config = input("Quelle configuration par défaut voulez vous ? \n Planeur : 1 \n Grenouille : 2 \n Canon : 3 \n")

        if config == "1":
            nom_config = './configuration_defaut/Glider.csv'

        elif config == "2":
            nom_config = './configuration_defaut/Grenouille.csv'

        elif config == "3":
            nom_config = './configuration_defaut/Canon.csv'

        else:
            raise ValueError("Choix non valide. Veuillez entrer 1, 2 ou 3.")
            

        iteration = input("Pour finir, donnez le nombre d'étapes que vous souhaitez que votre automate cellulaire exécute : ")

        with open(nom_config, newline='') as fichier:   # Les '1' dans les fichiers csv représentent les cellules vivantes des configurations par défaut
            reader = csv.reader(fichier)
            grille = []
            for ligne in reader:                        # Ici, on ajoute les lignes du csv dans des sous-listes, chaque sous-liste représente une ligne de la grille
                nouvelle_ligne = []
                for element in ligne:
                    nouvelle_ligne.append(int(element))
                grille.append(nouvelle_ligne)

            coor = trouver_coordonnees_de_1(grille)     # On trouve donc les coordonnées des '1' dans les sous-listes, qui vont nous permettre d'initialiser la grille avec les cellules vivantes
            g = Grille(30)
            for i in range(len(coor)):
                g.get_cell(coor[i][0], coor[i][1]).vivant()
            print(g)
            J = Jeu(g)
            J.evolution(int(iteration))
    
    else:
        raise ValueError("Choix non valide. Veuillez entrer 1 ou 2.")
                
                
if __name__ == "__main__" :
    main()
    
    
    
