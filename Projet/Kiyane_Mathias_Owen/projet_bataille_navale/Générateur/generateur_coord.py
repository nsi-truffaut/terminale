from test_bateau import*
#ceci est une ébauche possible de générateur de coordonnées de bateaux

def bateau(joueur):
    for i in range(len(joueur)):
        sens=input("le sens du bateau  Verticale=v    Horizontale=h")
        if sens=="h":
            premier=input("la coordonnee la plus à gauche")
            creationH(joueur[i],i+1,premier)
        else:
            premier=input("la coordonnee la plus en haut")
            creationV(joueur[i],i+1,premier)

def creationH(bateau,i,premier):
    if i==1:
        bateau.ajout(premier)
        bateau.ajout(premier[0] + chr(ord(premier[1])+1))
    if i==2:
        bateau.ajout(premier)
        bateau.ajout(premier[0] + chr(ord(premier[1])+1))
        bateau.ajout(premier[0] + chr(ord(premier[1])+2))
    if i==3:
        bateau.ajout(premier)
        bateau.ajout(premier[0] + chr(ord(premier[1])+1))
        bateau.ajout(premier[0] + chr(ord(premier[1])+2))
    if i==4:
        bateau.ajout(premier)
        bateau.ajout(premier[0] + chr(ord(premier[1])+1))
        bateau.ajout(premier[0] + chr(ord(premier[1])+2))
        bateau.ajout(premier[0] + chr(ord(premier[1])+4))
    if i==5:
        bateau.ajout(premier)
        bateau.ajout(premier[0] + chr(ord(premier[1])+1))
        bateau.ajout(premier[0] + chr(ord(premier[1])+2))
        bateau.ajout(premier[0] + chr(ord(premier[1])+3))
        bateau.ajout(premier[0] + chr(ord(premier[1])+4))


def creationV(bateau,i,premier):
    if i==1:
        bateau.ajout(premier)
        bateau.ajout(chr(ord(premier[0])+1) + premier[1])
    if i==2:
        bateau.ajout(premier)
        bateau.ajout(chr(ord(premier[0])+1) + premier[1])
        bateau.ajout(chr(ord(premier[0])+2) + premier[1])
    if i==3:
        bateau.ajout(premier)
        bateau.ajout(chr(ord(premier[0])+1) + premier[1])
        bateau.ajout(chr(ord(premier[0])+2) + premier[1])
    if i==4:
        bateau.ajout(premier)
        bateau.ajout(chr(ord(premier[0])+1) + premier[1])
        bateau.ajout(chr(ord(premier[0])+2) + premier[1])
        bateau.ajout(chr(ord(premier[0])+3) + premier[1])
    if i==5:
        bateau.ajout(premier)
        bateau.ajout(chr(ord(premier[0])+1) + premier[1])
        bateau.ajout(chr(ord(premier[0])+2) + premier[1])
        bateau.ajout(chr(ord(premier[0])+3) + premier[1])
        bateau.ajout(chr(ord(premier[0])+4) + premier[1])

bateau(joueur1)
