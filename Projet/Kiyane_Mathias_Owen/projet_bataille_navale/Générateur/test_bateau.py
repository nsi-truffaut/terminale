#/usr/bin/env python3
#-*- coding: utf-8 -*-

#fichier: test_bateau
#auteur: Kiyane HAMMOUMI
#date: 02/12/2021
#
from fonction_touche import *

torpilleur1, sous_marin1, contre_torpilleur1, croiseur1, porte_avions1 = Bateau(),Bateau(),Bateau(),Bateau(),Bateau()
torpilleur2, sous_marin2, contre_torpilleur2, croiseur2, porte_avions2 = Bateau(),Bateau(),Bateau(),Bateau(),Bateau()
joueur1 = [torpilleur1, sous_marin1, contre_torpilleur1, croiseur1, porte_avions1]
joueur2 = [torpilleur2, sous_marin2, contre_torpilleur2, croiseur2, porte_avions2]

if __name__=='__main__':
    torpilleur1.ajout("B4")
    torpilleur1.ajout("B5")

    sous_marin1.ajout("H10")
    sous_marin1.ajout("G10")
    sous_marin1.ajout("F10")

    contre_torpilleur1.ajout("J1")
    contre_torpilleur1.ajout("J2")
    contre_torpilleur1.ajout("J3")

    croiseur1.ajout("B6")
    croiseur1.ajout("C6")
    croiseur1.ajout("D6")
    croiseur1.ajout("E6")

    porte_avions1.ajout("A10")
    porte_avions1.ajout("A9")
    porte_avions1.ajout("A8")
    porte_avions1.ajout("A7")
    porte_avions1.ajout("A6")
    

    torpilleur2.ajout("D5")
    torpilleur2.ajout("E5")

    sous_marin2.ajout("A1")
    sous_marin2.ajout("B1")
    sous_marin2.ajout("C1")

    contre_torpilleur2.ajout("H8")
    contre_torpilleur2.ajout("H7")
    contre_torpilleur2.ajout("H9")

    croiseur2.ajout("I6")
    croiseur2.ajout("I5")
    croiseur2.ajout("I4")
    croiseur2.ajout("I3")

    porte_avions2.ajout("F10")
    porte_avions2.ajout("J10")
    porte_avions2.ajout("I10")
    porte_avions2.ajout("G10")
    porte_avions2.ajout("H10")

    jeu(joueur1,joueur2)
    
