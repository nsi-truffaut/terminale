# !/usr/bin/env python3
#-*- coding: utf-8 -*-

#
# Fichier : bataille_navale.py
#  Auteur : HAMMOUMI Kiyane, PAULARD Owen, THIBAUD Matthias
#    Date : 03/12/2021
#


# Début du fichier bataille_navale.py

import os

def touche(coordonnees,attaque):
    """ Fonction qui renvoie True si la coordonnée donnée est dans la liste d'un bateau,
        coordonees symbolise le bateau et attaque la coordonnées saisie par l'utilisateur """
    if attaque in coordonnees:
        coordonnees.remove(attaque)
        return True
    else:
        return False
        
def coule(coordonnees):
    """ Fonction qui renvoie True si la liste de coordonnées d'un bateau est vide."""
    return coordonnees == []
            
# bateaux joueur 1

torpilleur1 = input("Joueur 1 : saisissez les coordonnées du torpilleur (2 cases) : ").split(",")
sous_marin1 = input("Joueur 1 : saisissez les coordonnées du sous-marin (3 cases) : ").split(",")
contre_torpilleur1 = input("Joueur 1 : saisissez les coordonnées du contre-torpilleur (3 cases) : ").split(",")
croiseur1 = input("Joueur 1 : saisissez les coordonnées du croiseur (4 cases) : ").split(",")
porte_avions1 = input("Joueur 1 : saisissez les coordonnées du porte-avions (5 cases) : ").split(",")

joueur1 = [torpilleur1,sous_marin1,contre_torpilleur1,croiseur1,porte_avions1]

clearConsole = lambda: os.system('cls' if os.name in ('nt', 'dos') else 'clear')

clearConsole()
# bateaux joueur 2

torpilleur2 = input("Joueur 2 : saisissez les coordonnées du torpilleur (2 cases) : ").split(",")
sous_marin2 = input("Joueur 2 : saisissez les coordonnées du sous-marin (3 cases) : ").split(",")
contre_torpilleur2 = input("Joueur 2 : saisissez les coordonnées du contre-torpilleur (3 cases) : ").split(",")
croiseur2 = input("Joueur 2 : saisissez les coordonnées du croiseur (4 cases) : ").split(",")
porte_avions2 = input("Joueur 2 : saisissez les coordonnées du porte-avions (5 cases) : ").split(",")

joueur2 = [torpilleur2,sous_marin2,contre_torpilleur2,croiseur2,porte_avions2]

clearConsole()
#TEST !
# pour tester décommentez les lignes 54 à 68 et commentez les lignes 29 à 49
##
##torpilleur1 = ["B1","B2"]
##sous_marin1 = ["B3","B4","B5"]
##contre_torpilleur1 = ["C1","C2","C3"]
##croiseur1 = ["D1","D2","D3","D4"]
##porte_avions1 = ["D5","D6","D7","D8","D9"]
##
##joueur1 = [torpilleur1,sous_marin1,contre_torpilleur1,croiseur1,porte_avions1]
##
##torpilleur2 = ["H1","H2"]
##sous_marin2 = ["E4","E5","E6"]
##contre_torpilleur2 = ["E7","E8","E9"]
##croiseur2 = ["F1","F2","F3","F4"]
##porte_avions2 = ["F5","F6","F7","F8","F9"]
##
##joueur2 = [torpilleur2,sous_marin2,contre_torpilleur2,croiseur2,porte_avions2]

# programme jeu

while joueur1 != [[],[],[],[],[]] and joueur2 != [[],[],[],[],[]]:
    print("C'est au joueur 1 de jouer.")
    entree = input("Saisissez les coordonnées pour attaquer : ")
    etat = "raté"
    for bateau in joueur2: 
        if touche(bateau,entree) == True :
            etat= "touché"
            if coule(bateau):
                etat = "touché, coulé !"
                
    print(etat)
    
    print("C'est au joueur 2 de jouer.")
    entree = input("Saisissez les coordonnées pour attaquer : ")
    etat = "raté"
    for bateau in joueur1:  
        if touche(bateau,entree) == True:
            etat= "touché"
            if coule(bateau):
                etat= "touché, coulé !"

    print(etat)

def victoire(joueur1, joueur2) : 
    """Fonction qui renvoie "Victoire du Joueur 1" si tout les bateaux du joueur 2 sont coulés, et vice-versa.
    """
    if joueur1 == [[],[],[],[],[]] : 
        return "victoire du joueur 2" 
    elif joueur2 == [[],[],[],[],[]] : 
        return "victoire du joueur 1" 

print(victoire(joueur1, joueur2))

    


# Fin du fichier bataille_navale.py
