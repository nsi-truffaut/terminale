#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : automate.py
#  auteur : Gregory MAUPU & DORE Maël
#    date : 16/02/2023


# Importation des modules

import os
import time
import copy

class Cell :

    def __init__(self):
        self.etat = 0

    def __repr__(self):
        if self.etat == 1:
            return '*'
        else:
            return '-'
        
    def vivant(self):
        self.etat = 1

    def mort(self):
        self.etat = 0
        
class Grille :

    def __init__(self,taille):
        self.taille = taille
        self.grille = [[Cell() for i in range(self.taille)] for j in range(self.taille)]

    def get_cell(self,i,j):
        return self.grille[i][j]

    def __repr__(self):
        chaine = ""
        for i in range(self.taille):
            for j in range(self.taille):
                chaine = chaine + str(self.grille[i][j])
            chaine = chaine + "\n"
        return chaine

    def __len__(self):
        return len(self.grille)

class Jeu :

    def __init__(self,grille) :
        self.grille_jeu = grille
        self.taille_jeu = Grille.__len__(self.grille_jeu)
        
        
    def update(self):

        copied_grille = copy.deepcopy(self.grille_jeu)

        for i in range(self.taille_jeu):
            for j in range(self.taille_jeu):

                cellule = self.grille_jeu.get_cell(i, j)
                coordo_voisins = [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)]
                vivantes = 0
                for l, h in coordo_voisins:
                    voisin_i, voisin_j = (i+l) % self.taille_jeu, (j+h) % self.taille_jeu
                    voisin = copied_grille.get_cell(voisin_i, voisin_j)
                    vivantes += voisin.etat

                if cellule.etat == 0 and vivantes == 3: # une cellule morte devient vivante
                    cellule.vivant()
                elif cellule.etat == 1 and vivantes == 2: # une cellule vivante reste vivante
                    cellule.vivant()
                else: # une cellule vivante devient morte
                    cellule.mort()

        return self.grille_jeu
            

    def evolution(self,temps):
        k = 0
        while k < temps :
            self.update()
            print(self.grille_jeu)
            time.sleep(0.1) # Permet de mettre en pause après la mise à jour des cellules, ici 0,1s
            os.system("clear") # Efface la console avant la prochaine mise à jour
            k = k+1

#((temp[0].etat == a[0].etat == b[0].etat)^(temp[0].etat == a[0].etat == c[0].etat)^(temp[0].etat == a[0].etat == d[0].etat)^(temp[0].etat == a[0].etat == e[0].etat)^(temp[0].etat == a[0].etat == f[0].etat)^(temp[0].etat == a[0].etat == g[0].etat)^(temp[0].etat == a[0].etat == h[0].etat)^(temp[0].etat == b[0].etat == c[0].etat)^(temp[0].etat == b[0].etat == d[0].etat)^(temp[0].etat == b[0].etat == e[0].etat)^(temp[0].etat == b[0].etat == f[0].etat)^(temp[0].etat == b[0].etat == g[0].etat)^(temp[0].etat == b[0].etat == h[0].etat)^(temp[0].etat == c[0].etat == d[0].etat)^(temp[0].etat == c[0].etat == e[0].etat)^(temp[0].etat == c[0].etat == f[0].etat)^(temp[0].etat == c[0].etat == g[0].etat)^(temp[0].etat == c[0].etat == h[0].etat)^(temp[0].etat == d[0].etat == e[0].etat)^(temp[0].etat == d[0].etat == f[0].etat)^(temp[0].etat == d[0].etat == g[0].etat)^(temp[0].etat == d[0].etat == h[0].etat)^(temp[0].etat == e[0].etat == f[0].etat)^(temp[0].etat == e[0].etat == g[0].etat)^(temp[0].etat == e[0].etat == h[0].etat)^(temp[0].etat == f[0].etat == g[0].etat)^(temp[0].etat == f[0].etat == h[0].etat)^(temp[0].etat == g[0].etat == h[0].etat)) == False


if __name__=="__main__" :
    print("Exemple n°1")
    a = Grille(5)
    b = Jeu(a)
    c = a.get_cell(1,1)
    c.vivant()
    c = a.get_cell(1,2)
    c.vivant()
    c = a.get_cell(1,3)
    c.vivant()
    c = a.get_cell(2,3)
    c.vivant()
    c = a.get_cell(3,2)
    c.vivant()
    print(a)
    b.evolution(3)

    print("Exemple n°2")
    x = Grille(6)
    y = Jeu(x)
    z = x.get_cell(1,2)
    z.vivant()
    z = x.get_cell(2,2)
    z.vivant()
    z = x.get_cell(2,3)
    z.vivant()
    z = x.get_cell(2,4)
    z.vivant()
    z = x.get_cell(3,2)
    z.vivant()
    z = x.get_cell(3,3)
    z.vivant()
    z = x.get_cell(4,2)
    z.vivant()
    y.evolution(5)
    

    # Vous proposerez un test, issue des exemples 1 ou 2

# fin du fichier automate
