#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#fichier ..: main.py
#auteur ...: DORE Maël
#date .....: 16/02/2023

from automate import*

def main():
    fichier =""
    print("Bonjour et bienvenue dans le jeu de la vie")
    print("Voici les différentes configurations initiales possibles: \n1) exemple1 \n2) exemple2 \n3) Clignotant")
    configuration = input("Quelle configuration initiale souhaitez-vous ? \n -> ")
    taille = int(input("Quelle taille de grille souhaitez-vous ? \nNote, si vous sélectionnez une taille trop petite par rapport au jeu défini, une taille par défaut sera affectée \n-> "))
    if configuration == "exemple1" or configuration == '1':
        fichier = 'exemple1.csv'
        if taille < 5:
            taille = 5
    elif configuration == "exemple2" or configuration == '2':
        fichier = 'exemple2.csv'
        if taille < 5:
            taille = 5
    elif configuration == 'clignotant' or configuration == 'blinker' or configuration == '3':
        fichier = 'blinker.csv'
        if taille < 18:
            taille = 18
    
    
    grille = Grille(taille)
    jeu = Jeu(grille)
    iterations = int(input("Combien de cycles d'évolution souhaitez-vous ? \n -> "))

    liste_csv = []
    with open(fichier, 'r') as f:
        compteur = 0
        ligne_lue = f.readline().strip().split(',')
        while ligne_lue != [""]:
            liste_csv.append(ligne_lue)
            ligne_lue = f.readline().strip().split(',')
    
    debut = (taille-len(liste_csv))//2
    for k in range(debut, debut + len(liste_csv)):
        for l in range(len(liste_csv[k - debut])):
            if liste_csv[k - debut][l] == '*':
                cell = grille.get_cell(k,l)
                cell.vivant()
    print(grille)
    jeu.evolution(iterations)

if __name__ == "__main__":
    main()

#fin du fichier main.py
