from chiffrement import cle

# conversion d'une chaîne ce caractère en binaire

def dec_to_bin(d):
    
    if d == 0:
        return [0]
    else :
        return dec_to_bin(d//2) + [d%2] 

def bin_to_dec(l):
    d = 0
    for i in range(len(l)):
        d = d + l[i]*2**(len(l)-i-1)
    return d
    
def chaine_vers_bin(chaine):
    u=[]
    for k in range(len(chaine)):
         n = dec_to_bin(ord(chaine[k]))
         for i in range(len(n)) :
             u.append(n[i])
    return u

def bin_vers_chaine(l):
    s = ""
    for i in range(len(l)//8) :
        m = []
        for j in range(i*8,(i+1)*8) :
            m.append(l[j])
        d = bin_to_dec(m)%127
        if d < 33 :
            d = d +33
        elif d > 126 :
            d = d -126
        s = s + chr(d)
    return s

def xor(a,b):
    if a == 0 :
        p = True
    else :
        p = False
    if b == 0 :
        q = True
    else :
        q = False
    etat = 0
    if (p and not q) or (not p and q) :
        etat = 1
    return etat

def chiffrement(chaine,cle):
    l = chaine_vers_bin(chaine)
    m =len(cle)
    code = []
    n = len(l)
    for k in range(n):
        code.append(xor(l[k],cle[k%m]))
    return code

def affichage_chiffrement(message):
    return bin_vers_chaine(message)

def dechiffrement(message,cle):
    taille_cle = len(cle)
    
    u = []
    for k in range(len(message)):
        u.append(xor(message[k],cle[k%taille_cle]))
      
        
    return bin_vers_chaine(u)

if __name__=="__main__" :
    chaine = "IncroyablementsuperGenial"
    print(chaine_vers_bin(chaine))
    n = len(chaine)
    cle = cle(n)
    print(cle)
    mes = chiffrement(chaine,cle)
    print("mes",mes)
    aff = affichage_chiffrement(mes)
    print("aff",aff)
    c = chaine_vers_bin(aff)
    print(c==mes)
    print(dechiffrement(mes,cle))
    
    
        
