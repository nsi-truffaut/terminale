# Version 1

def recherche(elt,tab):
    """
    >>> recherche(1, [2, 3, 4])
    -1
    >>> recherche(1, [10, 12, 1, 56])
    2
    >>> recherche(50, [1, 50, 1])
    1
    >>> recherche(15, [8, 9, 10, 15])
    3
    >>> recherche(3,[])
    -1
    >>> recherche(4,[4,4,4,1])
    0
    """
    k = 0
    while k < len(tab) and tab[k] != elt:
        k = k+1
    if k == len(tab) :
        return -1
    else :
        return k

# Version 2

def recherche2(elt,tab):
    """
    >>> recherche2(1, [2, 3, 4])
    -1
    >>> recherche2(1, [10, 12, 1, 56])
    2
    >>> recherche2(50, [1, 50, 1])
    1
    >>> recherche2(15, [8, 9, 10, 15])
    3
    >>> recherche2(3,[])
    -1
    >>> recherche2(4,[4,4,4,1])
    0
    """
    k = 0 
    indice = -1
    while k < len(tab) and tab[k] != elt:
          k = k+1
    if k < len(tab) :
       indice = k
    return indice
            
# Version 3

def recherche3(elt,tab):
    """
    >>> recherche3(1, [2, 3, 4])
    -1
    >>> recherche3(1, [10, 12, 1, 56])
    2
    >>> recherche3(50, [1, 50, 1])
    1
    >>> recherche3(15, [8, 9, 10, 15])
    3
    >>> recherche3(3,[])
    -1
    >>> recherche3(4,[4,4,4,1])
    0
    """
    indice = -1
    for k in range(len(tab)-1,-1,-1):
        if tab[k] == elt :
            indice = k
    
    return indice

def insere(a, tab):
    """
    >>> insere(3,[1,2,4,5])
    [1, 2, 3, 4, 5]
    >>> insere(10,[1,2,7,12,14,25])
    [1, 2, 7, 10, 12, 14, 25]
    >>> insere(1,[2,3,4])
    [1, 2, 3, 4]
    >>> insere(8,[2,3,4])
    [2, 3, 4, 8]
    >>> insere(5,[])
    [5]
    """ 
    assert tab == sorted(tab), "le tableau n'est pas triée"
    l = list(tab) #l contient les mêmes éléments que tab
    l.append(a)
    i = len(l)-2
    while a < l[i] and i >= 0: 
      l[i+1] = l[i]
      l[i] = a
      i = i-1
    return l

if __name__ == '__main__' :
    

    # Doctest
    import doctest
    doctest.testmod(verbose = True)
