#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : automate_a_completer.py
#  auteur : Gregory MAUPU & ....
#    date : .....


# Importation des modules

import os
import time

class Cell :

    def __init__(self):
        # a compléter

    def __repr__(self):
        # à compléter
        
    def vivant(self):
        # à compléter

    def mort(self):
        # à compléter
        
class Grille :

    def __init__(self,taille):
        # à compléter

    def get_cell(self,i,j):
        # à compléter

    def __repr__(self):
        # à compléter

    def __len__(self):
        # à compléter

class Jeu :

    def __init__(self,grille) :
        # à compléter
        
        
    def update(self,N):
        # à compléter
            

    def evolution(self,temps):
        k = 0
        while k < temps :
            # à compléter
            time.sleep(0.1) # Permet de mettre en pause après la mise à jour des cellules, ici 0,1s
            os.system("clear") # Efface la console avant la prochaine mise à jour
            k = k+1

if __name__=="__main__" :

    # Vous proposerez un test, issue des exemples 1 ou 2

# fin du fichier automate_a_completer
