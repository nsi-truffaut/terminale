#Jeu de la vie

from turtle import *
from random import *



class Cell :

    def __init__(self,etat,pos):
        self.e = etat
        self.t = Turtle()
        self.t.up()
        self.t.ht()
        self.t.goto(pos)
        self.t.shape("square")
        self.t.resizemode("user")
        self.t.shapesize(0.5,0.5,1)
        self.t.speed(0)
        

    def couleur(self):
        if self.e == 1 :
            self.t.color("black")
        elif self.e == 0 :
            self.t.color("white")

    def mort(self):
        self.e = 0
        self.t.st()

    def vivant(self):
        self.e = 1
        self.t.st()

class Grille :

    def __init__(self,n) :
        
        self.g = []
        for k in range(n) :
            t = []
            for j in range(n) :
                t.append(Cell(0,(j*10,k*10)))
                t[j].couleur()
            self.g.append(t)

    def quadrillage(self):
        for k in range(8):
            down()
            forward(160)
            up()
            goto(0,k*20)
            
            

    def get_cell(self,i,j):
        return self.g[i][j]

    def __len__(self):
        return len(self.g)

   
        
class Jeu :

    def __init__(self,grille) :
        self.g = grille
        self.n = len(grille)
        
        
    def update(self,N):
        s = dict()
        for i in range(N) :
            for j in range(N):
                s[(i,j)] = self.g.get_cell(i,(j-1)%N).e + self.g.get_cell(i,(j+1)%N).e + self.g.get_cell((i-1)%N,j).e + self.g.get_cell((i+1)%N,j).e + self.g.get_cell((i-1)%N,(j-1)%N).e + self.g.get_cell((i-1)%N,(j+1)%N).e + self.g.get_cell((i+1)%N,(j-1)%N).e + self.g.get_cell((i+1)%N,(j+1)%N).e
        
        for cle in s.keys() :
            i,j = cle[0],cle[1]
            if self.g.get_cell(i,j).e == 1 :
                    if s[cle] < 2 or s[cle] > 3 :
                       self.g.get_cell(i,j).mort()
                       
            else :
                    if s[cle] == 3 :
                        self.g.get_cell(i,j).vivant()
            self.g.get_cell(i,j).couleur()

    def evolution(self,temps):
        k = 0
        while k < temps :
            self.update(self.n)
            k = k+1
            
            
    def vaisseau(self,i,j) :
        self.g.get_cell(i,j).vivant()
        self.g.get_cell(i,j+1).vivant()
        self.g.get_cell(i,j+2).vivant()
        self.g.get_cell(i-1,j+2).vivant()
        self.g.get_cell(i-2,j+1).vivant()
        self.g.get_cell(i,j).couleur()
        self.g.get_cell(i,j+1).couleur()
        self.g.get_cell(i-1,j+2).couleur()
        self.g.get_cell(i,j+2).couleur()
        self.g.get_cell(i-2,j+1).couleur()
        

    def barre(self,i,j):
       self.g.get_cell(i,j).vivant()
       self.g.get_cell(i+1,j).vivant()
       self.g.get_cell(i+2,j).vivant()
       self.g.get_cell(i,j).couleur()
       self.g.get_cell(i+1,j).couleur()
       self.g.get_cell(i+2,j).couleur()
    


if __name__=="__main__" :
    ht()
    g = Grille(10)
    j = Jeu(g)
    j.barre(3,1)
    j.evolution(10)


    
                    
