#Jeu de la vie en console
import os
import time

class Cell :

    def __init__(self):
        self.e = 0

    def __repr__(self):
        if self.e == 1 :
            return "*"
        else :
            return " "
        
    def vivant(self):
        self.e = 1

    def mort(self):
        self.e = 0
        
class Grille :

    def __init__(self,longueur,hauteur):
        self.l = longueur
        self.h = hauteur
        self.g = [[Cell() for k in range(self.l)] for j in range(self.h)]

    def get_cell(self,i,j):
        return self.g[i][j]

    def __repr__(self):
        s = ""
        for k in range(self.h):
            for j in range(self.l) :
                s = s + str(self.g[k][j]) + ''
            s = s + '\n'
        return s

    def __len__(self):
        return len(self.g)

    def get_l(self):
        return self.l

    def get_h(self):
        return self.h

class Jeu :

    def __init__(self,grille) :
        self.g = grille
        self.l = self.g.get_l()
        self.h = self.g.get_h()
        
        
    def update(self):
        s = dict()
        for i in range(self.h) :
            for j in range(self.l):
                s[(i,j)] = self.g.get_cell(i,(j-1)%self.l).e + self.g.get_cell(i,(j+1)%self.l).e + self.g.get_cell((i-1)%self.h,j).e + self.g.get_cell((i+1)%self.h,j).e + self.g.get_cell((i-1)%self.h,(j-1)%self.l).e + self.g.get_cell((i-1)%self.h,(j+1)%self.l).e + self.g.get_cell((i+1)%self.h,(j-1)%self.l).e + self.g.get_cell((i+1)%self.h,(j+1)%self.l).e
        
        for cle in s.keys() :
            i,j = cle[0],cle[1]
            if self.g.get_cell(i,j).e == 1 :
                    if s[cle] < 2 or s[cle] > 3 :
                       self.g.get_cell(i,j).mort()
                       
            else :
                    if s[cle] == 3 :
                        self.g.get_cell(i,j).vivant()
            

    def evolution(self,temps):
        k = 0
        
        while k < temps :
            print(self.g)
            self.update()
            time.sleep(0.1)
            os.system("clear")
            k = k+1
        print(self.g)

if __name__=="__main__" :
    g = Grille(45,45)
##    with open("planeur.csv","r") as fichier :
##        l = fichier.readline().strip().split(";")
##        print(l)
##        i = 0
##        while l!=[""] :
##            for k in range(len(l)):
##                if int(l[k]) == 1 :
##                    g.get_cell(i,k).vivant()
##            l = fichier.readline().strip().split(";")
##            i = i+1

    g.get_cell(10,10).vivant()
    g.get_cell(11,10).vivant()
    g.get_cell(12,10).vivant()
    g.get_cell(13,10).vivant()
    
    g.get_cell(11,11).vivant()
    g.get_cell(12,11).vivant()
   
    g.get_cell(11,12).vivant()
    
    j = Jeu(g)
    j.evolution(200)
    print(g)
