from jeu_console import *

import argparse

configuration = """Les configurations de départ sont les suivantes : 
                1) Planeur 
                2) Glitter
                3) Aléatoire
                Choisissez le numéro de la configuration """
                
def main() :
    
    b = int(input("Quelle taille de grille souhaitez-vous ?"))
    g = Grille(b)
    a = int(input(configuration))
    if a == 1 :
        f = "planeur.csv"
    elif a == 2 :
        f = "glitter.csv"
    elif a == 3:
        f = "alea.csv"
    with open(f,"r") as fichier :
        l = fichier.readline().strip().split(";")
        i = 0
        while l!=[""] :
            for k in range(len(l)):
                if int(l[k]) == 1 :
                    g.get_cell(i,k).vivant()
            l = fichier.readline().strip().split(";")
            i = i+1
    
    j = Jeu(g)
    c = int(input("Combien d'itération souhaitez-vous voir ?"))
    j.evolution(c)

def main2() :
    # Pour ajouter des options à la lignes de commande
    argParser = argparse.ArgumentParser() # Création de l'objet

    # Ajout des options
    
    argParser.add_argument("-l","--longueur",type=int, help="longueur de la grille")
    argParser.add_argument("-ha","--hauteur",type=int, help="hauteur de la grille")
    argParser.add_argument("-c","--configuration", help="Choisissez une configuration de départ \n planeur \n glitter \n alea")
    argParser.add_argument("-i","--iteration",type=int, help="nombre d'itération")

    # Lecture des options
    
    args =argParser.parse_args()

    # Programme
    
    g=Grille(args.longueur,args.hauteur)
    c = "%s.csv" % args.configuration
    with open(c,"r") as fichier :
        l = fichier.readline().strip().split(";")
        i = 0
        while l!=[""] :
            for k in range(len(l)):
                if int(l[k]) == 1 :
                    g.get_cell(i,k).vivant()
            l = fichier.readline().strip().split(";")
            i = i+1
    j = Jeu(g)
    j.evolution(args.iteration)
    
if __name__=="__main__" :
    main2()
