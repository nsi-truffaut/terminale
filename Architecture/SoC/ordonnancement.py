from file import File

import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
fig = plt.figure()
ax = fig.add_subplot(111)
plt.xlim([0,20])
plt.ylim([0,10])

def rr(liste_proc,q) :
    f = File()
    coord_rect = {}
    for k in range(len(liste_proc)):
        f.enfiler(liste_proc[k])
    hauteur = 1
    absi = 0
    while not f.est_vide():
        proc = f.defiler()
        print("le processus",proc[0]," s'exécutent pendant",min(q,proc[2]),"ms")
        if proc[0] not in coord_rect.keys() :
            coord_rect[proc[0]]=[((absi,hauteur),min(q,proc[2]),1)]
            hauteur = hauteur +2
        else :
            hauteur = coord_rect[proc[0]][0][0][1]
            
            coord_rect[proc[0]].append(((absi,hauteur),min(q,proc[2]),1)) ###
        absi = absi+min(q,proc[2])
        proc[2] = max(proc[2]-q,0)
        if proc[2] != 0 :
            f.enfiler(proc)
        
    return coord_rect

liste_proc = [["A",0,3],["B",1,6],["C",3,4],["D",6,5],["E",8,2]]

c = rr(liste_proc,2)
for cle in c :
    for i in range(len(c[cle])):
        a,b,e=c[cle][i]
        ax.add_patch(Rectangle(a,b,e,color="blue"))
plt.show()
        
    
