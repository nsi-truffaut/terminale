import time
import math
from pylab import *
from matplotlib import pyplot as plt


def fibo(n):
    if n==0 :
        return 0
    if n == 1 :
        return 1
    else :
        return fibo(n-1) + fibo(n-2)

def fibod(n) :
    l = [0 for _ in range(n+1)]
    #l[0] = 0
    l[1] = 1
    for i in range(2,n+1) :
        l[i] = l[i-1] + l[i-2]
    return l

def rend(pieces : list,somme_a_rendre : int):
    """ Fonction qui renvoie le nombre de pièces
        minimal pour la somme à rendre ainsi que la valeur des pièces
    """
    if somme_a_rendre ==0 :
        return 0,[] # nombre = 0 et pas de valeurs
    mini = somme_a_rendre # Nombre maximal de pièces possibles (que des 1)
    type_pieces = []
    for j in range(len(pieces)) :
        if pieces [j] <=somme_a_rendre :
            a = rend(pieces, somme_a_rendre-pieces[j]) # On appelle rend sur chaque sous-somme possibles
            k = 1 + a[0] # On incrémente le nombre de pièce de 1 
            if k < mini :
                mini = k
                type_pieces = a[1] + [pieces[j]]
            
    return mini,type_pieces

def rend_dyna(pieces,n):
    # On initialise avec le nombre max de pièces
    nb = [k for k in range(n+1)]
    #Autre solution
    # On initialise avec que des 0
    for i in range(1,n+1):
        # dans ce cas on ajoute nb[i]=i, pour avoir le plus grand nombre de pièces
        for p in pieces :
            if p <= n :
                nb[i] = min(nb[i],1+nb[i-p])
    return nb[n]

def rend_dyna_sol(pieces,n):
    nb = [0 for k in range(n+1)]
    sol = [[0 for k in range(len(pieces)+1)] for j in range(n+1)]
    for k in range(1,n+1):
        nb[k] = k
        for j in range(1,len(pieces)+1) :
            
            #if pieces[j-1] <=k and 1+ nb[k-pieces[j-1]] <= nb[k] :
            if pieces[j-1] <= k :
                nb[k] = min(nb[k],1+ nb[k-pieces[j-1]]) # On peut aussi faire comme çà
                #nb[k] = 1+ nb[k-pieces[j-1]]
                sol[k] = sol[k-pieces[j-1]].copy()
                sol[k][j] = sol[k][j]+1
                  
    return nb[n],sol

def affichage(pieces,n) :
    sol = rend_dyna_sol(pieces,n)
    print("0",end =" | ")
    for i in range(len(pieces)):
        print(pieces[i],end=" | ")
    print("\n")
    for k in range(len(sol)):
        print(k,end = "|")
        for j in range(len(sol[k])):
            print(sol[k][j], end = " | ")
        print("\n")



 
#pieces = [1,2,5,10,20,50,100,200]

def SK(objet,poids_max):
    if poids_max == 0 :
        return 0,[]
    nb = len(objet)
    sol = []
    for j in range(len(objet)) :
        if objet[j][1] <= poids_max :
            a = SK(objet[j+1:len(objet)], poids_max-objet[j][1])
            k = 1 + a[0]
            if k < nb :
                nb = k
                sol = a[1] + [objet[j]]
            
    return nb,sol

##V = [(700,13),(500,12),(200,8),(300,10),(600,14),(800,18)]
##U = [(114,4.57,"A"), (32,0.63,"B"),(20,1.65,"C"),(4,0.085,"D"),(18,2.15,"E"),(80,2.71,"F"),(5,0.32,"G")]
##
##fig, ax = subplots()
##xdata,ydata = [], []
##line, = plot([],[])
##xlim(0,45)
##ylim(0,60)
##
##xdata = [k for k in range(41)]
##def temps(n) :
##    l = []
##    for k in range(n):
##        t = time.time()
##        fibo(k)
##        l.append(time.time()-t)
##    return l
##
##ydata = temps(41)
##plt.plot(xdata,ydata)
##plt.show()
if __name__ =='__main__' :
    
    pieces = [1,3,6]
##    # Test n°1
##    t = time.time()
##    print(rend(pieces,13))
##    print(rend_dyna_sol(pieces,23))
##    t2 = time.time()-t
##    print(t2)
##
##    # Test n°2
##    t = time.time()
##    print(rend_dyna(pieces,48))
##    t2 = time.time()-t
##    print(t2)
##    t = time.time()
##    print(fibo(40))
##    
##    t2 = time.time()-t
    print('a')
