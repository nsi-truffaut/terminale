def fct_1(x) :
    if x > 5 :
        return True
    else :
        return False

y = 6
def fct_2():
    if y > 5 :
        return True
    else :
        return False

y = 6
def fct_3(x) :
    global y
    if x > 5 :
        y = y +x
    else :
        y = y -x
    return y

def fct_3bis(x,y):
    return y+x if x >5 else y-x

tab = [0, 1, 2]
def fct_4(tab) :
    tab.append(3)
    return tab

tab=[0, 1, 2]
def fct_5(tab) :
    return tab + [3]
    
def somme(tab,debut,fin):
    if debut == fin :
        return tab[debut]
    else :
        return tab[debut] + somme(tab,debut+1,fin)

def somme2(tab,debut,fin):
    return tab[debut] if debut == fin else tab[debut]  + somme(tab,debut+1,fin)


def compare_KS(x,y):
    return x[0]/x[1] > y[0]/y[1]

#tri_selection(V,compare_KS)
#tri_selection(V, lambda x,y : x[0]/x[1] > y[0]/y[1])


def compare_Seq(x,y):
    return x[1] < y[1]

#tri_selection(S,compare_Seq)
#tri_selection(V, lambda x,y : x[1] < y[1])

def compare_Knn(x,y,distance,e):
    return distance(e,x[0]) < distance(e,y[0])

#tri_selection(classe,e,distance,compare_Knn)

def carre_i(tab) :
    for k in range(len(tab)):
        tab[k] = tab[k]**2

def carre_f(tab) :
    return [a**2 for a in tab]

list(map(lambda x : x**2,tab))

def positif(tab):
    for a in tab :
        if a < 0 :
            del a

def positif_f(tab):
    return [a for a in tab if a > 0]

list(filter(lambda x : x > 0, tab))

def produit(tab):
    p = 1    
    for a in tab :
        p = p *a
    return p

def produit_f(tab,debut,fin):
    return 0 if tab == [] else tab[debut] if debut==fin else tab[debut]*produit_f(tab,debut+1,fin)

import functools

functools.reduce(lambda x,y : x*y, tab)
    
def listevide() :
    return []

def premier(l):
    return l[0]

def reste(l):
    return l[1:]

def dernier(l):
    return premier(l) if reste(l) == listevide() else dernier(reste(l))
