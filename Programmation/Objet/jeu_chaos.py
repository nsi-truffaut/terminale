# jeu du chaos

from turtle import *
from random import randint
from math import sqrt

class Point :

    def __init__(self,x,y):
        self.abs = x
        self.ord = y

    def get_abscisse(self):
        return self.abs

    def get_ordonnee(self):
        return self.ord

def milieu(A,B):
    xM = (A.get_abscisse()+ B.get_abscisse())/2
    yM = (A.get_ordonnee()+ B.get_ordonnee())/2
    return Point(xM,yM)

def jeu(nb_iteration):
    up()
    A = Point(-100,0)
    goto(A.get_abscisse(),A.get_ordonnee())
    dot(2)
    B = Point(100,0)
    goto(B.get_abscisse(),B.get_ordonnee())
    dot(2)
    C = Point(0, 200*sqrt(3)/2)
    goto(C.get_abscisse(),C.get_ordonnee())
    dot(2)
    n = randint(-100,100)
    m= randint(-100,100)
    D = Point(n,m)
    goto(D.get_abscisse(),D.get_ordonnee())
    dot(2)
    for i in range(nb_iteration) :
        de = randint(1,6)
        if de == 1 or de == 2 :
            D = milieu(D,A)
        if de == 3 or de == 4 :
            D = milieu(D,B)
        if de == 5 or de == 6 :
            D = milieu(D,C)
        goto(D.get_abscisse(),D.get_ordonnee())
        dot(2)

exitonclick()
    
