class Identite :
    def __init__(self, nom, prenom, an) :
        self.nom = nom
        self.prenom = prenom
        self.annee = an

    def age(self,a):
        return a - self.annee

    def get_nom(self):
        return self.nom

    def get_prenom(self) :
        return self.prenom

    def get_an(self):
        return self.annee
    
# On tape dans la console
# hugo = Identite('Hugo', 'Dupont', 1999)

# a) Qu'affiche hugo.age(2020) ?
# b) Qu'affiche hugo.annee ?
# c) Qu'affiche hugo.get_an ?
# d) Combien d'attributs a cette classe ? de méthodes ?
# d1) Comment s'appelle la variable hugo en POO ?

# e) L'encapsulation c'est :
#       1) le fait d'écrire une classe
#       2) le fait d'utiliser une classe
#       3) le fait que la façon de définir les attributs et les méthodes
#           ne sont pas visibles pour l'utilisateur
#       4) la programmation objet

# f) Comment s'appelle la fonction __init__ ?
# g) La surcharge d'opérateur c'est :
#       1) le fait d'écrire une méthode
#       2) le fait que les attributs soit privés ou publics
#       3) la redéfinition d'une méthode privée existante
#       4) le fait d'écrire la méthode __repr__





hugo = identite('Hugo', 'Dupont', 1999)
print(hugo.age(2020))
print(hugo.annee)
print(hugo.get_an())
print(hugo)
