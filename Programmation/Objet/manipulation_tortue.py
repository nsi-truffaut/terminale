from turtle import *


class Voiture :

    def __init__(self,couleur,position) :
        self.c = couleur
        self.t = Turtle()
        self.p = position
        self.t.color(couleur)

    def rouler(self, destination):
        self.t.up()
        self.t.goto(self.p)
        direction = self.t.towards(destination)
        self.t.setheading(direction)
        self.t.down()
        self.t.goto(destination)

if __name__=="__main__" :

    # Initialisation de variables
    couleur = ["black","blue","red","green"]
    position = [(-150,150),(300,-200),(-150,-150),(100,100)]

    
    for k in range(4):
        v = Voiture(couleur[k],position[k])
        v.rouler((0,0))


