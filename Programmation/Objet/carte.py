# Cours sur les modules
# 3 façons différentes de les importer

from random import randint

class Carte :

    def __init__(self,couleur,valeur):
        self.c = couleur
        self.v = valeur

    def __repr__(self):
        mot =""
        if self.v == 1 :
            mot = "As"
        elif self.v == 11 :
            mot = "Valet"
        elif self.v == 12 :
            mot = "Dame"
        elif self.v == 13 :
            mot = "Roi"
        if mot == "" :
            return "{} de {}".format(self.v,self.c)
        else :
            return "{} de {}".format(mot,self.c)

class Paquet :

    def __init__(self):
        self.p = [[Carte(couleur,valeur) for valeur in range(1,14)]
                  for couleur in ["Pique","Coeur","Carreau","Trèfle"]]


    def tirer(self):
        a = randint(0,3)
        b = randint(0,12)
        print(a,b)
        return self.p[a][b]


if __name__=='__main__' :

    jeu = Paquet()
    print(jeu.p)
    pallas = Carte("Pique", 12)
    print(pallas)
    for k in range(10):
        print(jeu.tirer())



    
