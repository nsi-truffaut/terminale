from turtle import *
from random import *

class Avion :

    def __init__(self,code) :
        self.c = code
        self.t = Turtle()
        self.t.ht()
        self.t.color(255//randint(1,6),255//randint(1,6),255//randint(1,6))
        self.p = (0,0)
        self.d = 0
        self.v = 0
        self.planvol = []

    def decoller(self):
        self.v = 1

    def atterrir(self) :
        self.v = 0

    def voler(self,plan_vol,balises):
        for i in range(len(plan_vol)-1) :
                self.p = balises[plan_vol[i]]
                self.t.up()
                self.t.goto(self.p)
                self.t.down()
                self.t.st()
                self.d = self.t.towards(balises[plan_vol[i+1]])
                self.t.setheading(self.d)
                self.t.speed(self.v)
                self.t.goto(balises[plan_vol[i+1]])

class Simulation :

    def __init__(self,balises,vols):
        self.b = balises
        self.v = vols

    def demarrer(self) :
        t = Turtle()
        t.ht()
        t.up()
        for cle in self.b.keys():
            t.goto(self.b[cle])
            t.dot(10,"blue")
            t.write(cle)
        for code_avion in self.v.keys():
            a = Avion(code_avion)
            a.decoller()
            a.voler(plans[self.v[code_avion]],self.b)
            a.atterrir()

if __name__=="__main__":
    
        balises = {
                    'CDG': (0, 170),
                    'BDX': (-140, -100),
                    'BYN': (-180, -180),
                    'TLS': (-40, -175),
                    'NAT': (-180, 60),
                    'BST': (-310, 140),
                    'REN': (-260, 120),
                    'LIL': (40, 300),
                    'STG': (240, 150),
                    'LMG': (-55, -35),
                    'LYN': (130, -40),
                    'MRS': (130, -180),
                    'GRN': (160, -80),
                    'NCE': (230, -170),
                    'MPL': (70, -175),
                    }
        plans = {
            ('Bordeaux', 'Paris')       : ['BDX', 'LMG', 'CDG'],
            ('Nantes',   'Nice')        : ['NAT', 'LMG', 'MRS', 'NCE'],
            ('Lille',    'Montpellier') : ['LIL', 'CDG', 'LMG', 'TLS', 'MPL'],
            ('Brest',    'Grenoble')    : ['BST', 'NAT', 'LMG', 'LYN', 'GRN'],
            ('Bayonne',  'Strasbourg')  : ['BYN', 'TLS', 'LYN', 'STG'],
            ('Rennes',  'Marseille')    : ['REN', 'LMG', 'MPL', 'MRS'],
                }

        vols = {
            'AF2267': ('Bordeaux', 'Paris'),
            'YZ1432': ('Nantes', 'Nice'),
            'IT2256': ('Lille', 'Montpellier'),
            'BA5983': ('Brest', 'Grenoble'),
            'IB4521': ('Bayonne', 'Strasbourg'),
            'DT7856': ('Rennes', 'Marseille'),
                }
        colormode(255)
        s = Simulation(balises,vols)
        s.demarrer()
                
                
            
            
            
        



        
