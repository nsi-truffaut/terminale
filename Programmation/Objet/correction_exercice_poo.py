# Exercice 1 - Solution

from math import gcd,sqrt

class Rectangle :

    def __init__(self,x,y):
        self.longueur = x
        self.largeur = y

    def get_longueur(self):
        return self.longueur

    def get_largeur(self):
        return self.largeur

    def perimetre(self):
        """Renvoie le périmètre"""
        return 2*(self.get_largeur() + self.get_longueur())

    def aire(self) :
        return self.get_largeur()*self.get_longueur()

    def est_carre(self) :
        return self.get_largeur()== self.get_longueur()












# Exercice 2 - Solution 1

class Point :

    def __init__(self,x,y):
        self.abs = x
        self.ord = y

    def get_abscisse(self):
        return self.abs

    def get_ordonnee(self):
        return self.ord

class Point_2 :

    def __init__(self,x,y):
        self.coord = (x,y)
        

    def get_abscisse(self):
        return self.coord[0]

    def get_ordonnee(self):
        return self.coord[1]

def distance(A,B):
    return sqrt( (B.get_abscisse()- A.get_abscisse())**2 + (B.get_ordonnee()- A.get_ordonnee())**2)

def milieu(A,B):
    xM = (A.get_abscisse()+ B.get_abscisse())/2
    yM = (A.get_ordonnee()+ B.get_ordonnee())/2
    return Point(xM,yM)

#Exercice 3

class Eleve :

    def __init__(self,nom,prenom):
        self.nom = nom
        self.prenom = prenom
        self.notes= []

    def get_nom(self):
        return self.nom

    def get_prenom(self):
        return self.prenom

    def get_notes(self):
        return self.notes

    def __repr__(self) :
        notes = self.get_notes()
        l = " {} {} :".format(self.prenom,self.nom)
        for note in notes :
            l = l + "{} - ".format(note)
        return  l

    def modifie_nom(self,nouveaunom):
          self.nom = nouveaunom

    def modifie_prenom(self,nouveauprenom) :
        self.prenom = nouveauprenom

    def ajout_note(self,note):
        self.notes.append(note)

    def nb_notes(self):
        return len(self.notes)

    def moyenne(self):
        s = 0
        n = self.nb_notes()
        for i in range(n) :
            s = s + self.notes[i]
        return s/n
    
#Exercice4

class Fraction :

    def __init__(self,numerateur,denominateur) :
        self.num = numerateur
        self.den = denominateur

    def get_num(self):
        return self.num

    def get_den(self) :
        return self.den
    
    def __mul__(self,other):
        
        n = self.get_num()*other.get_num()
        d = self.get_den()*other.get_den()
        
        return Fraction(n,d)

    def __repr__(self):
        return " {}/{}".format(self.get_num(),self.get_den())

    def __add__(self,other):
        n = self.get_num()*other.get_den() + other.get_num()*self.get_den()
        d = self.get_den()*other.get_den()
        return Fraction(n,d)

    def __neg__(self):
        return Fraction(-self.get_num(),self.get_den())

    def __sub__(self,other):
        return Fraction(self.get_num(),self.get_den())+ (-Fraction(other.get_num(),other.get_den()))

    def __truediv__(self,other) :
        return Fraction(self.get_num(),self.get_den())*(Fraction(other.get_den(),other.get_num()))

    def simplifie(self):
        d = gcd(self.get_num(),self.get_den())
        return Fraction(self.get_num()//d,self.get_den()//d)

    def __eq__(self,other):
        return self.get_num()*other.get_den()-self.get_den()*other.get_num() == 0

    def __gt__(self,other):
        return self.get_num()*other.get_den()-self.get_den()*other.get_num() > 0

    def __lt__(self,other):
        return self.get_num()*other.get_den()-self.get_den()*other.get_num() < 0
        
        
    
    

    

    
        

    
    
    
