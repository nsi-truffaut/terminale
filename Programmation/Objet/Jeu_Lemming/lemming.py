class Jeu :

    def __init__(self,carte):

        with open(carte,'r') as fichier :
            ligne = fichier.readline()
            cartes=[]
            
            while ligne !='' :
                cartess = []
                for k in range(len(ligne)) :
                    cartess.append(Case(ligne[k]))
                ligne = fichier.readline()
                cartes.append(cartess)

        self.terrain=cartes
        self.lemmings = []

    def affiche(self):
        for ligne in self.terrain:
            for colonne in ligne:
                print(str(colonne),end='')
            
                                    
            
    def tour(self) :
        self.affiche() 
        for lem in self.lemmings :
            lem.action()
          

    def jouer(self):
        l = Lemming(0,1,1,self)
        self.lemmings.append(l)
        self.terrain[0][1].lemming = l
        k = 0
        while k < 12 :
            self.tour()
            print('\n')
            print(k)
            k+=1

class Lemming() :

    def __init__(self,ligne,colonne,direction,jeu):
        self.l = ligne
        self.c = colonne
        self.d = direction
        self.jeu = jeu
        

    def action(self) :
        
        if self.jeu.terrain[self.l+1][self.c].estLibre() :
            self.jeu.terrain[self.l][self.c].depart()
            self.l = self.l + 1
            self.jeu.terrain[self.l][self.c].arrivee(self)
            print(self.l,self.c)
        else :
            if self.d == 1 :
                if self.jeu.terrain[self.l][self.c+1].estLibre() :
                    
                    self.jeu.terrain[self.l][self.c].depart()  
                    self.c = self.c + 1
                    self.jeu.terrain[self.l][self.c].arrivee(self)
                    print(self.l,self.c)
                elif self.jeu.terrain[self.l][self.c+1].a == '#' :
                   
                    self.d = -self.d
                    
            else :
                if self.jeu.terrain[self.l][self.c-1].estLibre() :
                    
                    self.jeu.terrain[self.l][self.c].depart()
                    self.c = self.c - 1
                    self.jeu.terrain[self.l][self.c].arrivee(self)
                    
                elif self.jeu.terrain[self.l][self.c-1].a == '#' :
                    
                    self.d = -self.d
                    
       
             
        
                    
    def sort(self) :
        
        self.jeu.lemmings.remove(self)
        self.jeu.terrain[self.l][self.c].depart() 
        

    def __str__(self) :
        if self.d == 1 :
            return ">"
        else :
            return "<"


class Case :

    def __init__(self,aspect):
        self.a = aspect
        self.lemming = None
        

    def estLibre(self) :
        return self.a ==" " and self.lemming == None or self.a == "S"

    def depart(self):
        self.lemming = None
        
    def arrivee(self,lem):
        self.lemming = lem
        if self.a =="S" :
           self.lemming.sort()
           
       
            
        
    def __str__(self) :
        if self.lemming is None :
            return self.a
        else :
            return str(self.lemming)
            

        

            
            


if __name__=='__main__' :
    J = Jeu("carte.txt")
    #J.affiche()
    l = Lemming(0,1,1,J)
    J.lemmings.append(l)
    J.terrain[0][1].lemming = l
    J.jouer()
        

    
    
