#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : traitement_fichier_dictionnaire.py
#  auteur : Gregory MAUPU
#    date : 2021/09/07

#Importation des modules

import re

# Programme

def traitement():
    with open('dictionnaire.txt','r',encoding='utf-8') as fichier :
        with open('dico.txt','w',encoding='utf-8') as new :
            l = fichier.readline()
            
            while l !="" :
                l = re.sub('[éêëè]','e',l)
                l = re.sub('[iïî]','i',l)
                l = re.sub('[oôö]','o',l)
                l = re.sub('[àâä]','a',l)
                l = re.sub('[ûüù]','u',l)
                l = re.sub('[-]','',l)
                l = re.sub('[ç]','c',l)
                new.write(l)
                l = fichier.readline()

def traitement_bis():
    traitement()
    with open('dico.txt','r',encoding='utf-8') as fichier :
        ligne = fichier.readlines()
       
        newligne= sorted(list(set(ligne)))
        
        with open('dico2.txt','w',encoding='utf-8') as new :
            for l in newligne :
                new.write(l.lower())

traitement_bis()

# fin du fichier traitement_fichier_dictionnaire.py
