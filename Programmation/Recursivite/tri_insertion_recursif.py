def inserer(t,liste):
    
    j = 0
    n = len(liste)
    while j < n and t > liste[j]:
            j = j+1
    return liste[0:j] + [t] + liste[j:n]

def inserer2(v,t):
    t.append(v)
    x = v
    j = len(t)-1
    while j > 0 and t[j -1] > v :
        t[j] = t[j-1]
        j = j-1
        
    t[j] = x
    return t
    
print(inserer2(4,[3,7,8,9,10.2,11]))

##def tri_ins_rec(t):
##    if len(t) == 0 :
##        return []
##    else :
##        j = 0
##        u = tri_ins_rec(t[1:len(t)])
##        while j < len(u) and t[0] > u[j]:
##            j = j+1
##        return u[0:j] + [t[0]] + u[j:len(t)]

def tri_ins_rec(t):
    if len(t) == 0 :
        return []
    else :
        u = tri_ins_rec(t[0:len(t)-1])
        return inserer(t[len(t)-1],u)

def tri_ins_rec2(t,debut,fin):
    if debut > fin:
        return [t[debut]]
    else :
        u = tri_ins_rec2(t,debut,fin-1)
        return inserer(t[fin],u)
    

print(tri_ins_rec([9,5,7]))

def minimum(liste):
    if len(liste) == 1 :
        return liste[0]
    else :
        mini = liste[0]
        autre_mini = minimum(liste[1:len(liste)])
        if mini < autre_mini :
            return mini
        else :
            return autre_mini

def tri_selection_rec(l):
    if len(l) == 1 :
        return l
    else :
        return [minimum(l)] + tri_selection_rec(l[1:len(l)])

print(tri_selection_rec([9,5,7]))
print(minimum([2,6,1,8,-4]))
