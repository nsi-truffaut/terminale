from random import randint

def maxi_rec(tab,debut,fin):
    if debut == fin :
        return tab[debut]
    else :
        maxi = maxi_rec(tab,debut+1,fin)
        if tab[debut] < maxi :
            return maxi
        else :
            return tab[debut]

def max_rec(tab):
    return maxi_rec(tab,0,len(tab)-1)

tab = [randint(-50,50) for k in range(10)]
print(tab)
print(max_rec(tab))

def romain(s) :
    if len(s) == 1 :
        return symb[s]
    else :
        if symb[s[0]] >= symb[s[1]] :
            return symb[s[0]] + romain(s[1:len(s)])
        else :
            return symb[s[0]] - romain(s[1:len(s)])
symb={'M':1000, 'D':500,'C':100,'L':50,'X':10,'V':5,'I':1}
print(romain("MMCCCXCVIII"))
