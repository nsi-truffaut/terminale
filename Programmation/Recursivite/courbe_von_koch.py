#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fichier : courbe_von_koch.py
#  auteur : Gregory MAUPU
#    date : 2021/09/16

# Importation des modules

from turtle import *
from math import *
# Programme

def von_koch(n_etape,longueur,angle) :
    if n_etape == 1 :
        forward(longueur)
    else :
        longueur = longueur/3
        von_koch(n_etape-1,longueur,angle)
        left(angle)
        von_koch(n_etape-1,longueur,angle)
        right(2*angle)
        von_koch(n_etape-1,longueur,angle)
        left(angle)
        von_koch(n_etape-1,longueur,angle)

def von_koch_q(n_etape,longueur) :
    if n_etape == 1 :
        forward(longueur)
    else :
        longueur = longueur/3
        von_koch_q(n_etape-1,longueur)
        left(90)
        von_koch_q(n_etape-1,longueur)
        right(90)
        von_koch_q(n_etape-1,longueur)
        right(90)
        von_koch_q(n_etape-1,longueur)        
        left(90)
        von_koch_q(n_etape-1,longueur)
        
def flocon(n_etape,longueur,angle):
    for i in range(3):
        von_koch(n_etape,longueur,angle)
        right(120)

def levy(n_tape,longueur):
    if n_tape == 1 :
        forward(longueur)
    else :
        longueur = longueur/sqrt(2)
        right(45)
        levy(n_tape-1,longueur)
        left(90)
        levy(n_tape-1,longueur)
        right(45)
# test
speed(0)
up()
goto(200,0)
down()
#von_koch_q(5,300)
levy(15,500)
# fin du fichier courbe_von_koch.py
