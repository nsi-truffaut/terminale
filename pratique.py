# Sujet 1

def convertir(T):
    """
    T est un tableau d'entiers, compris entre 0 et 7.
    Il représente un entier écrit en octal.
    Renvoie l'écriture décimale de l'entier positif dont la représentation
    octale est donnée par le tableau T.
    """
    for k in range(len(T)):
        assert T[k] < 8 and T[k] > 0, "les nombres ne sont pas dans la bonne plage"
    n = len(T)
    s = 0
    for k in range(n) :
        s = s + T[k]*8**(n-1-k)
    return s
    

    

# Sujet 2

def moyenne(T):
    s = 0
    for i in range(len(T)):
        s = s + T[i]
    return s/len(T)

def egaliser(T):
    s = sum(T)
    m = moyenne(T)
    return [round(m-T[i],2) for i in range(len(T))]

print(egaliser([3,4,2,1,8]))

# Sujet3

def achat(p,N):
    n = len(p)
    s = 0
    for i in range(n):
        s = s + p[i]*N[i]
    return s

#Sujet 4

def effacer(s):
    d = s
    k = 0
    while k < len(d)-2 :
        if d[k] != d[k+1] :
            k = k+1
        else :
            d = d[0:k]+d[k+2:len(s)]
            k = 0
    return d

# Sujet 5

def lisser_une_fois(T):
    n = len(T)
    u = T.copy()
    for i in range(1,n-1):
        u[i] = (T[i-1]+T[i+1])/2
    return u

def lisser(T,diff_max):
    
    n=len(T)
    ok = False
    k = 0
    while not ok :
        k = k+1
        j = 0
        while j < n-1 and abs(T[j+1]-T[j]) < diff_max :
            j = j+1
        if j == n-1 :
            ok = True
        else :
            T = lisser_une_fois(T)
    return T
            
# Sujet 6

def proche_factoriel(n):
    k = 1
    u = 1
    while u < n :
         k = k+1
         u = k*u
         
    return k-1

def factoriel(p):
    u = 1
    k = 1
    while k < p :
        k = k+1
        u = u*k
        
    return u

def decomposition(n):
   
    l = []
    p = proche_factoriel(n)
    while p > 0:
        print("n=",n,"p=",p)
        l.append(n//factoriel(p))
        n = n%factoriel(p)
        print(n)
        p = p-1
       
    return l 

# Sujet 7

def nombre_doublon(s):
    dico = {}
    n = len(s)
    for k in range(n):
        if s[k] not in dico.keys():
            dico[s[k]] = 0
        else :
            dico[s[k]] = (dico[s[k]]+1)//2+1
    return dico

       

def zero_p(n):
    if n == 1 :
        return "({}+{})".format(0,0)
    else :
        return "({}+{})".format(zero_p(n-1),zero_p(n-1))

def zero(n):
    
    return "0={}".format(zero_p(n))
        

# Sujet 8

def hauteur_matrice(L,n,m):
    maxi = 0
   
    for j in range(m):
        i = n-1
        mm = 0
        while i >-1 and L[i][j] == 1:
            mm +=1
            i = i-1
        
        if mm > maxi :
            maxi = mm
    return maxi

def retourne(s,n):
    if n == 0:
        return ''
    else :
        return s[n-1] + retourne(s,n-1)
            
            
# Sujet 10

def seul(T) :
    d = {}
    for k in range(len(T)):
        if T[k] not in d.keys():
            d[T[k]] = 1
        else :
            d[T[k]] += 1
    seul = T[0]
    for cle in d.keys() :
        if d[cle] % 2 ==1 :
            seul = cle
    return seul
        
# Sujet 12

def lexico(s):
    n = len(s)
    k = 0
    while k < n-1 and s[k+1] > s[k] :
          k = k+1
          
    if k == n-1 :
        return ""
    else :
        return s[k+1]
    

print(lexico("arbre"))
print(lexico("mort"))
print(lexico("abcdefa"))
print(seul([3,7,1,8,3,7,8]))
