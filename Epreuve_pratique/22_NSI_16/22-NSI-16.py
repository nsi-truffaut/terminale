def maxi(tab):
    """
    >>> maxi([1,5,6,9,1,2,3,7,9,8])
    (9, 3)
    """
    indice_max = len(tab)-1
    maxi = tab[indice_max]
    for k in range(len(tab)-1,-1,-1):
        if tab[k] >= maxi :
            indice_max = k
            maxi = tab[k]
    return maxi,indice_max

def maxi2(tab) :
    """
    >>> maxi2([1,5,6,9,1,2,3,7,9,8])
    (9, 3)
    >>> maxi2([])
    AssertionError
    """
    assert tab !=[]
    solution = []
    indice_max = 0
    maxi = tab[0]
    for k in range(1,len(tab)):
        if tab[k] > maxi :
            indice_max = k
            maxi = tab[k]
    return maxi,indice_max    

def positif(T):
    """
    >>> positif([-1,0,5,-3,4,-6,10,9,-8 ])
    T =  [-1, 0, 5, -3, 4, -6, 10, 9, -8]
    [0, 5, 4, 10, 9]
    >>> positif([-2,-3,-4,-8])
    T =  [-2, -3, -4, -8]
    []
    """
    T2 = list(T)
    T3 = []
    while T2 != []:
        x = T2.pop()
        if x >= 0:
            T3.append(x)
    #T2 = []
    while T3 != []:
        x = T3.pop()
        T2.append(x)
    print('T = ',T)
    return T2

if __name__=='__main__':
    import doctest
    doctest.testmod(verbose = True)
