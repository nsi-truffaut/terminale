def occurence_lettres(phrase):
    """
    >>> occurence_lettres('Hello world !')
    {'H': 1, 'e': 1, 'l': 3, 'o': 2, ' ': 2, 'w': 1, 'r': 1, 'd': 1, '!': 1}
    >>> occurence_lettres('Abracadabra')
    {'A': 1, 'b': 2, 'r': 2, 'a': 4, 'c': 1, 'd': 1}
    >>> occurence_lettres("")
    {}
    """
    dic_result = {}
    for lettre in phrase :
        if lettre not in dic_result.keys():
            dic_result[lettre] = 1
        else :
            dic_result[lettre] += 1
    return dic_result

def occurence_lettres_rec(phrase,debut,fin):
    dico_rec = {}
    if debut == fin :
        return {phrase[debut] : 1}
    else :
        dico_rec = occurence_lettres_rec(phrase,debut+1,fin)
        if phrase[debut] in dico_rec.keys() :
            dico_rec.update({phrase[debut]: dico_rec[phrase[debut]]+1})
        else :
            dico_rec.update({phrase[debut]: 1})
    return dico_rec

def occ(phrase):
    return occurence_lettres_rec(phrase,0,len(phrase)-1)

def fusion(L1,L2):
    """
    >>> fusion([1,6,10],[0,7,8,9])
    [0, 1, 6, 7, 8, 9, 10]
    >>> fusion([3,7,15],[0,1,1,3])
    [0, 1, 1, 3, 3, 7, 15]
    """
    n1 = len(L1)
    n2 = len(L2)
    L12 = [0]*(n1+n2)
    i1 = 0
    i2 = 0
    i = 0
    while i1 < n1 and i2 < n2 :
        if L1[i1] < L2[i2]:
            L12[i] = L1[i1]
            i1 = i1+1
        else:
            L12[i] = L2[i2]
            i2 = i2+1
        i += 1
    while i1 < n1:
    	L12[i] = L1[i1]
    	i1 = i1 + 1
    	i = i+1
    while i2 < n2:
    	L12[i] = L2[i2]
    	i2 = i2 + 1
    	i = i+1
    return L12

if __name__=="__main__" :
    import doctest
    doctest.testmod(verbose=True)
