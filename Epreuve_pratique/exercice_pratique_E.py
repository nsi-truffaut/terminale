def complement_a_2(n : str) -> str :
    s = ""
    for k in range(len(n)):
        s = s + str(1-int(n[k])) 
    return s

def bin_to_dec(s : str) -> int :
    n = 0
    t = len(s)
    for k in range(t):
        n = n + int(s[k])*2**(t-k-1)
    return n

def deux_taux(T : list) -> tuple :
    d = {}
    for k in range(len(T[0])):
        n0 = 0
        n1 = 0
        for j in range(len(T)):
            if T[j][k] == "0" :
                n0 +=1
            else :
                n1 +=1
        d[k] = (n0,n1)
    s = ""
    for cle in d.keys() :
        if max(d[cle]) == d[cle][0] :
            s = s + "0"
        else :
            s = s + "1"
       
    return bin_to_dec(s)*bin_to_dec(complement_a_2(s))

T = ["00100","11110","10110","10111","10101","01111","00111","11100","10000","11001","00010","01010"]


        
