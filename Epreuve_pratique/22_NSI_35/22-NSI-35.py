def moyenne(tab)  :
    somme = 0
    for k in range(len(tab)):
        somme +=tab[k]
    return somme/len(tab)

assert moyenne([1]) == 1
assert moyenne([1,2,3,4,5,6,7]) == 4
assert moyenne([1,2]) == 1.5
assert moyenne([1.5,2.3,6.2,18.5])==7.125


def dichotomie(tab, x):
    """
        tab : tableau trie dans l'ordre croissant
        x : nombre entier
        La fonction renvoie True si tab contient x et False sinon

    >>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33],28)
    True
    >>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33],27)
    (False, 3)
    >>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33],1)
    (False, 2)
    >>> dichotomie([],28)
    (False, 1)
    """
    # cas du tableau vide
    if tab == []:
        return False,1

    # cas ou x n'est pas compris entre les valeurs extremes
    if (x < tab[0]) or x > tab[len(tab)-1] :
        return False,2
    
    debut = 0
    fin = len(tab) - 1
    while debut <= fin:
        m = (debut+fin)//2
        if x == tab[m]:
            return True
        if x > tab[m]:
            debut = m + 1
        else:
            fin = m-1			
    return False,3

if __name__=='__main__':
    import doctest
    doctest.testmod(verbose=True)
