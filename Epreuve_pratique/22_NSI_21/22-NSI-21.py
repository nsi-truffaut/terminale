def multiplication(n,m):
    """
    >>> multiplication(3,5)
    15
    >>> multiplication(-4,-8)
    32
    >>> multiplication(-2,6)
    -12
    >>> multiplication(-2,0)
    0
    >>> multiplication(6,-7)
    -42
    """
    r = 0
    for k in range(abs(m)):
        if m > 0 :
            r = r + n
        else :
            r = r - n
    return r

def multiplication_r(n,m):
    if m == 0 :
        return 0
    else :
        if m > 0 :
            return n + multiplication_r(n,m-1)
        else :
            return -n + multiplication_r(n,m+1)
        
def dichotomie(tab, x):
    """
        tab : tableau d’entiers trié dans l’ordre croissant
        x : nombre entier
        La fonction renvoie True si tab contient x et False sinon

    >>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33],28)
    True
    >>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33],27)
    False
    """

    debut = 0 
    fin = len(tab) - 1
    while debut <= fin:
        m = (debut+fin)//2
        if x == tab[m]:
            return True
        if x > tab[m]:
            debut = m + 1
        else:
             fin = m-1		
    return False

if __name__=='__main__':
    import doctest
    doctest.testmod(verbose = True)
    
