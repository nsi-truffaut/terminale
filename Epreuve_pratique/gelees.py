def gelees(t):
    n = len(t)
    max_t_seq = 0
    k = 0
    while k < n :  # On va faire des sauts (les sous-séquences)
        if t[k] >= 0 : # si c'est + on avance
            k = k+1
        else :  # le cas négatif
            j = k
            t_seq = 0 # on réinitialise la taille à chaque nouvelle sseq
            while j < n and t[j] < 0  : # on parcourt jusqu'à la fin eventuellement
                j = j +1
                t_seq +=1
            
            if t_seq > max_t_seq : # on est sorti, on compare au max courant
                max_t_seq = t_seq
            k = j # on remet k au dernier indice 
            
    return max_t_seq

print(gelees([2,-3,-2,0,1,-1]))
print(gelees([-3,-2,-2]))
print(gelees([7,-4,-1,-5,1,2,-10,-10,-8,-7,6]))
