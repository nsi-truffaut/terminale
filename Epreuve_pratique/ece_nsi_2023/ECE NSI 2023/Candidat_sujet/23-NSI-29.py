class Arbre:
    def __init__(self, etiquette):
        self.v = etiquette
        self.fg = None
        self.fd = None


def taille(a) :
    if a is None :
        return 0
    else :
        return 1+ taille(a.fg) + taille(a.fd)

a = Arbre(1)
a.fg = Arbre(4)
a.fd = Arbre(0)
a.fd.fg = Arbre(7)

b = Arbre(0)
b.fg = Arbre(1)
b.fg.fg = Arbre(3)
b.fd = Arbre(2)
b.fd.fg = Arbre(4)
b.fd.fd = Arbre(5)
b.fd.fg.fd = Arbre(6)

print(taille(b))

def hauteur(a):
    if a is None :
        return 0
    else :
        return 1+ max(hauteur(a.fg),hauteur(a.fd))

print(hauteur(b))

def ajoute(indice, element, liste):
    nbre_elts = len(liste)
    L = [0 for i in range(nbre_elts + 1)]
    if indice < nbre_elts:
        for i in range(indice):
            L[i] = liste[i]
        L[indice] = element
        for i in range(indice + 1, nbre_elts + 1):
            L[i] = liste[i-1]
    else:
        for i in range(nbre_elts):
            L[i] = liste[i]
        L[nbre_elts] = element
    return L

def ajoute2(indice, element, liste) :
    n = len(liste)
    liste.append(element)
    if indice < n :
        j = n
        while j > indice :
            liste[j] = liste[j - 1]
            j = j-1
        liste[j] = element
    return liste
        
