def enumere(L) :
    d = {}
    for i in range(len(L)) :
        if L[i] not in d.keys() :
            d[L[i]] = [i]
        else :
            d[L[i]].append(i)
    return d

print(enumere([1,1,2,3,2,1]))

class Arbre:
    def __init__(self, etiquette):
        self.v = etiquette
        self.fg = None
        self.fd = None

def parcours(arbre, liste):
    if arbre != None:
        parcours(arbre.fg, liste)
        liste.append(arbre.v)
        parcours(arbre.fd, liste)
    return liste

def insere(arbre, cle):
    """ arbre est une instance de la classe Arbre qui implémente
        un arbre binaire de recherche.
    """
    if cle < arbre.v:
        if arbre.fg != None :
            insere(arbre.fg, cle)
        else:
            arbre.fg = Arbre(cle)
    else:
        if arbre.fd != None:
            insere(arbre.fd, cle)
        else:
            arbre.fd = Arbre(cle)

a = Arbre(5)
insere(a,2)
insere(a,3)
insere(a,7)
print(parcours(a,[]))
