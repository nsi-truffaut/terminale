class AdresseIP:

    def __init__(self, adresse):
        self.adresse = adresse
   
    def liste_octet(self):
        """renvoie une liste de nombres entiers,
           la liste des octets de l'adresse IP"""
        return [int(i) for i in self.adresse.split(".")] 
        
    def est_reservee(self):
        """renvoie True si l'adresse IP est une adresse
           reservee, False sinon"""
        return self.liste_octet()[3] == '0' or self.liste_octet()[3] == '255'
             
    def adresse_suivante(self):
        """renvoie un objet de AdresseIP avec l'adresse 
           IP qui suit l'adresse self
           si elle existe et False sinon"""
        if int(self.liste_octet()[3]) < 254:
            octet_nouveau = int(self.liste_octet()[3]) + 1
            return AdresseIP('192.168.0.' + str(octet_nouveau))
        else:
            return False

a1 = AdresseIP('192.168.0.1')
a2 = AdresseIP('192.168.0.2')
a3 = AdresseIP('192.168.0.0')
a4 = AdresseIP('192.168.0.254')

print(a1.est_reservee())
print(a3.est_reservee())
print(a2.adresse_suivante().adresse)
print(a4.adresse_suivante())
