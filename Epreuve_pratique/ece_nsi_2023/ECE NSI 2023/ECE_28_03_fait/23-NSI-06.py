from math import sqrt   # import de la fonction racine carree

def recherche(tab,n):
    """
    >>> recherche([5, 3], 1)
    2
    >>> recherche([2, 4], 2)
    0
    >>> recherche([2, 3, 5, 2, 4], 2)
    3
    """
    assert tab !=[],"Le tableau est vide"
    indice = len(tab)
    for i in range(len(tab)):
        if tab[i] == n :
            indice = i
    return indice

  

        
def distance(point1, point2):
    """ Calcule et renvoie la distance entre deux points.
    >>> distance((1, 0), (5, 3))
    5.0
    >>> distance((1, 0), (0, 1))
    1.4142135623730951

    """
    return sqrt((point2[0]-point1[0])**2 + (point2[1]-point1[1])**2)

def plus_courte_distance(tab, depart):
    """ Renvoie le point du tableau tab se trouvant a la plus
    courte distance du point depart.
    >>> plus_courte_distance([(7, 9), (2, 5), (5, 2)], (0, 0))
    (2, 5)
    >>> plus_courte_distance([(7, 9), (2, 5), (5, 2)], (5, 2))
    (5, 2)
    """
    point = tab[0]
    min_dist = distance(point,depart)
    for i in range (1, len(tab)):
       if distance(tab[i], depart) < min_dist :
            point = tab[i] 
            min_dist = distance(point,depart)
    return point

if __name__=='__main__' :
    import doctest
    doctest.testmod(verbose = True)
