def recherche_min(tab) :
    """
    >>> recherche_min([5])
    0
    >>> recherche_min([2, 4, 1])
    2
    >>> recherche_min([5, 3, 2, 2, 4])
    2
    """
    assert tab !=[],"Le tableau est vide"
    mini = tab[0]
    indice = 0
    for k in range(len(tab)):
        if tab[k] < mini :
            mini = tab[k]
            indice = k
        
    return indice

def recherche_min_rec(tab,debut,fin):
    """
    >>> recherche_min_rec([5],0,0)
    0
    >>> recherche_min_rec([2, 4, 1],0,2)
    2
    >>> recherche_min_rec([5, 3, 2, 2, 4],0,4)
    2
    """
    assert tab !=[],"Le tableau est vide"
    if debut == fin :
        return debut
    else :
        ind_mini = recherche_min_rec(tab,debut+1,fin)
        if tab[debut] <= tab[ind_mini] :
            return debut
        else :
            return ind_mini


def separe(tab):
    """
    >>> separe([1, 0, 1, 0, 1, 0, 1, 0])
    [0, 0, 0, 0, 1, 1, 1, 1]
    >>> separe([1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0])
    [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    """
    gauche = 0
    droite = len(tab)-1
    while gauche < droite :
        if tab[gauche] == 0 :
            gauche = gauche+1
        else :
            tab[gauche], tab[droite] = tab[droite],tab[gauche] # demander fct aux
            droite = droite - 1
    return tab

if __name__=='__main__' :
    import doctest
    doctest.testmod()
