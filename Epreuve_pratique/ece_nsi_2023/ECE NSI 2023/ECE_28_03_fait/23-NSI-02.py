def indices_maxi(tab):
    """
    >>> indices_maxi([1, 5, 6, 9, 1, 2, 3, 7, 9, 8])
    (9, [3, 8])
    >>> indices_maxi([7])
    (7, [0])
    """
    assert tab !=[], "La liste ne doit pas être vide"
    maxi = tab[0]
    for i in range(len(tab)):
        if tab[i] > maxi :
            maxi = tab[i]
    liste_indices = []
    for i in range(len(tab)) :
        if tab[i] == maxi :
            liste_indices.append(i)
    return (maxi,liste_indices)

def indices_maxi_2(tab):
    """
    >>> indices_maxi_2([1, 5, 6, 9, 1, 2, 3, 7, 9, 8])
    (9, [3, 8])
    >>> indices_maxi_2([7])
    (7, [0])
    """
    assert tab !=[], "La liste ne doit pas être vide"
    maxi = tab[0]
    liste_indices = []
    for i in range(len(tab)):
        if tab[i] > maxi :
            maxi = tab[i]
            liste_indices = []
            liste_indices.append(i)
        elif tab[i] == maxi :
            liste_indices.append(i)
    return (maxi,liste_indices)

def positif(pile): # On peut demander de tout passer en objet si temps ok
    """
    >>> positif([-1, 0, 5, -3, 4, -6, 10, 9, -8])
    [0, 5, 4, 10, 9]
    >>> positif([-2])
    []
    """
    pile_1 = list(pile)
    pile_2 = []
    while pile_1 != []:
        x = pile_1.pop()
        if x >= 0:
            pile_2.append(x)
    while pile_2 != []:
        x = pile_2.pop()
        pile_1.append(x)
    return pile_1

if __name__=='__main__' :
    import doctest
    doctest.testmod(verbose = True)
