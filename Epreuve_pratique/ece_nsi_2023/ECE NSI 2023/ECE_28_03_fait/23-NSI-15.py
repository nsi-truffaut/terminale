def mini(t_moy,annee):
    assert len(t_moy)==len(annee),"Les 2 listes n'ont pas la même longueur"
    t_mini=t_moy[0]
    k = 0
    for i in range(len(t_moy)) :
        if t_moy[i] < t_mini :
            t_mini = t_moy[i]
            k = i
    return t_mini,annee[k]

def inverse_chaine(chaine):
    result = ""
    for caractere in chaine:
       result = caractere + result 
    return result

def est_palindrome(chaine):
    inverse = inverse_chaine(chaine)
    return chaine == inverse

# Version itérative sans recours à inverse_chaine

def palin(chaine):
    k = 0
    ok = False
    n = len(chaine)
    while k < n//2 and chaine[k] == chaine[n-k-1]:
        k = k+1
    if k == n//2 :
        ok = True
    return ok

# Version récursive

def palin_rec(chaine,debut,fin):
    if debut == fin :
        return True
    else :
        return chaine[debut] == chaine[fin] and palin_rec(chaine,debut+1,fin-1)


def est_nbre_palindrome(nbre):
    chaine = str(nbre)
    return est_palindrome(chaine)


if __name__=='__main__' :
    t_moy = [14.9, 13.3, 13.1, 12.5, 13.0, 13.6, 13.7]
    annees = [2013, 2014, 2015, 2016, 2017, 2018, 2019]
    assert(mini(t_moy,annees))==(12.5,2016)

    assert(inverse_chaine('bac'))=='cab'
    assert(est_palindrome('NSI')) == False
    assert(est_palindrome('ISN-NSI'))==True
    assert(est_nbre_palindrome(214312))==False
    assert(est_nbre_palindrome(213312))==True
