def recherche(a,tab):
    """
    >>> recherche(5, [])
    0
    >>> recherche(5, [-2, 3, 4, 8])
    0
    >>> recherche(5, [-2, 3, 1, 5, 3, 7, 4])
    1
    >>> recherche(5, [-2, 5, 3, 5, 4, 5])
    3
    """
    nb_occ = 0
    for elt in tab :
        if elt == a :
            nb_occ = nb_occ +1
    return nb_occ

def recherche_rec(a,tab):
    """
    >>> recherche_rec(5, [])
    0
    >>> recherche_rec(5, [-2, 3, 4, 8])
    0
    >>> recherche_rec(5, [-2, 3, 1, 5, 3, 7, 4])
    1
    >>> recherche_rec(5, [-2, 5, 3, 5, 4, 5])
    3
    """
    if tab == [] :
        return 0
    else :
        if tab[0] == a :
            return 1 + recherche_rec(a, tab[1:len(tab)])
        else :
            return recherche_rec(a, tab[1:len(tab)])




pieces = [1, 2, 5, 10, 20, 50, 100, 200]

def rendu_monnaie(somme_due, somme_versee):
    """
    >>> rendu_monnaie(700, 700)
    []
    >>> rendu_monnaie(102, 500)
    [200, 100, 50, 20, 20, 5, 2, 1]
    """
    rendu = []
    a_rendre = somme_versee - somme_due
    i = len(pieces) - 1
    while a_rendre > 0 :
        if pieces[i] <= a_rendre :
            rendu.append(pieces[i])
            a_rendre = a_rendre - pieces[i]
        else :
            i = i - 1
    return rendu

if __name__ == '__main__' :
    import doctest
    doctest.testmod(verbose= True)
