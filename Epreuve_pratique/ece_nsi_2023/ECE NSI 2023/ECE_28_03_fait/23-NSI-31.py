def nb_repetitions(elt,tab):
    occ = 0
    for a in tab :
        if a == elt :
            occ +=1
    return occ



def binaire(a):
    bin_a = str(a%2)
    a = a // 2
    while a !=0 :
        bin_a = str(a%2) + bin_a
        a = a//2
    return bin_a

def bin_rec(a):
    if a < 2 :
        return str(a%2)
    else :
        return  bin_rec(a//2) + str(a%2)    
