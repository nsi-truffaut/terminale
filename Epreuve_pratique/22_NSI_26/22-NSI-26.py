def RechercheMin(tab):
    """
    >>> RechercheMin([5])
    0
    >>> RechercheMin([2, 4, 1])
    2
    >>> RechercheMin([5, 3, 2, 2, 4])
    2
    """
    indice_mini = 0
    for k in range(1,len(tab)):
        if tab[k] < tab[indice_mini] :
            indice_mini = k
    return indice_mini

        

def separe(tab):
    """
    >>> separe([1, 0, 1, 0, 1, 0, 1, 0])
    [0, 0, 0, 0, 1, 1, 1, 1]
    >>> separe([1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0])
    [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    """
    i = 0
    j = len(tab)-1 # on parcourt le tableau en rétrécissant de chaque côté
    while i < j :
        if tab[i] == 0 :
            i = i+1 # on avance à gauche
        else :
            tab[i], tab[j] = tab[j],tab[i] # on rencontre un 1, on échange
            j = j - 1 # A droite, on vient de placer un 1 donc on recule d'un cran
    return tab

if __name__=='__main__':
    import doctest
    doctest.testmod(verbose = True)
