def ajoute_espaces(mot, longueur):
    """ Prolonge la chaîne de caratères mot par des espaces jusqu'à
        la longueur passée en paramètres
    """
    return ... + " " * (...)


def barres(categories, valeurs):
    """ Construit le diagramme en barres de taille valeur associée
        à chaque élément de catégories
    """
    longueur_max = ...
    for mot in ...:
        if len(mot) > ...:
            longueur_max = ...

    diagramme = ""
    for i in range(len(categories)):
        diagramme = ...  # la catégorie
        diagramme = ... + " : "
        diagramme = ...  # la barre
        diagramme = ... + "\n"  # retour à la ligne

    return ...

if __name__=='__main__' :
    
    # Tests
    
    # Fonction ajoute_espaces
    
    
    # Fonction barres
    
    categories = ["Anne", "Luc", "Patrick", "Sam"]
    valeurs = [10, 8, 5, 15]
    


