def rouler(terrain, i, j, deplacements_possibles):
    """ Fonction qui renvoie la position finale de la balle sur le terrain
        depuis une position initiale (i,j)
    """

    (delta_i, delta_j) = (-1, -1)
    # Boucle des déplacements
    while (delta_i, delta_j) != ... :
        # Initialisation du déplacment
        (delta_i, delta_j) = (0, 0)
        variation_maxi = ... 
        # Recherche du déplacement "maximal"
        for depl_i, depl_j in deplacements_possibles:
            if terrain[i][j] - terrain[...][...] > variation_maxi:
                delta_i = ....
                delta_j = ....
                variation_maxi = ....
        # Application du déplacement "maximal"
        i = ....
        j = ....

    return (i, j)

if __name__ == '__main__' :

    # Déclaration des variables
    
    terrain = [
    [100, 100, 100, 100, 100, 100, 100, 100, 100, 100],
    [100,  39,  17,  16,  30,  39,  56,  62,  51, 100],
    [100,  39,  18,   4,  10,  20,  43,  62,  53, 100],
    [100,  27,  22,  12,  18,  34,  43,  46,  42, 100],
    [100,  15,  23,  19,  30,  49,  45,  38,  30, 100],
    [100,  39,  43,  25,  27,  39,  36,  40,  40, 100],
    [100,  64,  59,  38,  36,  43,  39,  31,  31, 100],
    [100,  60,  49,  38,  42,  47,  55,  40,  22, 100],
    [100,  51,  38,  33,  45,  43,  49,  49,  28, 100],
    [100, 100, 100, 100, 100, 100, 100, 100, 100, 100]]

    deplacements_possibles = [(-1, -1),
                          ( 0, -1),
                          (+1, -1),
                          (-1,  0),
                          (+1,  0),
                          (-1, +1),
                          ( 0, +1),
                          (+1, +1)]

    # Tests à compléter
    
    

