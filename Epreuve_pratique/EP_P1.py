def histo(liste) :
    resultat = [ 0 for i in range(10)]
    for k in range(len(liste)) :
        resultat[liste[k]] +=1
    return resultat

print(histo([1 , 1, 1, 3, 2]))

def somme_pairs(liste):
    somme_indice = 0
    somme = 0
    for k in range(len(liste)):
        if k%2 == 0 :
            somme_indice += liste[k]
        if liste[k]%2 == 0:
            somme += liste[k]
    return (somme_indice, somme)

#print(somme_pairs([17, 6,3,2,4]))
