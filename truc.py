class Chat :

    def __init__(self,annee_n,race,nom):
        self.annee = annee_n
        self.race = race
        self.__nom = nom
        

    def age(self):
        return 2021 - self.annee

class Personnage :

    def __init__(self,genre,pouvoir,nb_vie):
        self.__genre = genre
        self.pouvoir = pouvoir
        self.nbvie = nb_vie

    def gagner_vie(self):
        self.nbvie = self.nbvie+1

    def __repr__(self):
        return "C'est un  {} dont le pouvoir est de type {} de niveau {}".format(self.__genre,self.pouvoir,self.nbvie)
        
